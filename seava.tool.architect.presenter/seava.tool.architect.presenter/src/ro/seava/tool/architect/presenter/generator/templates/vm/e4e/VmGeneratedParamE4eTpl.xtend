package ro.seava.tool.architect.presenter.generator.templates.vm.e4e

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.presenter.scoping.PresenterScopeUtils.*

import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.presenter.VmParam
import ro.seava.tool.architect.presenter.presenter.VmMemberValuesUsage
import ro.seava.tool.architect.presenter.presenter.VmMemberValues

class VmGeneratedParamE4eTpl extends AbstractVmE4e {

  def doExtjsClass(Vm vm) {

    if (vm.hasGeneratedParam) {

      // var initData = vm.memberValueList.findFirst(t|t.usage == VmMemberValuesUsage::INIT_PARAM_CLIENT);
      var VmMemberValues initData = null;
      if (vm.defaultValues != null) {
        initData = vm.defaultValues.memberValues.findFirst(t|t.usage == VmMemberValuesUsage::INIT_PARAM_CLIENT);
      }

      '''        
        «copyright»
        
        Ext.define("«vm.canonicalNameParamUiExtjs»", {
          extend: 'Ext.data.Model',
          «IF initData != null»
            
            initParam: function() {
              «FOR f : initData.values»
                this.set("«f.member.name»", «f.value._init_val_tpl(f.member)»);
              «ENDFOR»
            },
          «ENDIF»
          
          fields: [
            «FOR f : vm.collect_members_as_type(VmParam).sortBy(t|t.name) SEPARATOR ","»
              {name:"«f.name»", type:"«f.dataType.extjsType»"«IF !f.config.noFilter», forFilter:true«ENDIF»«f.
          modelDataFormat»«IF f.dataType.isNumber», useNull:true«ENDIF»}
            «ENDFOR»
          ]
        });
      '''
    }
  }

}
