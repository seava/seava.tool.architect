package ro.seava.tool.architect.presenter.extensions

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.presenter.presenter.Asgn
import ro.seava.tool.architect.presenter.presenter.P_Package
import ro.seava.tool.architect.project.project.Module
import ro.seava.tool.architect.project.project.Unit
import ro.seava.tool.architect.presenter.presenter.AsgnParam

class Asgn_Cfg {

  /* ====================== ASGN OWNERS ====================== */
  /**
   * Get containing package
   */
  def static P_Package getPackage(Asgn asgn) {
    return asgn.eContainer as P_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(Asgn asgn) {
    return asgn.package.module;
  }

  /**
   * Get containing unit
   */
  def static Unit getUnit(Asgn asgn) {
    return asgn.package.unit;
  }

  /* ====================== SIMPLE NAMES ====================== */
  def static String simpleNameModel(Asgn asgn) {
    if (asgn instanceof Asgn) {
      return (asgn as Asgn).name + "_Asgn";
    } else {
      return ""
    }
  }

  def static String simpleNameParam(Asgn asgn) {
    return asgn.simpleNameModel + "Param";
  }

  def static String simpleNameGeneratedService(Asgn asgn) {
    return asgn.simpleNameModel + "Service";
  }

  def static String simpleNameCustomService(Asgn asgn) {
    return asgn.simpleNameModel + "Service";
  }

  def static String simpleNameGeneratedQueryBuilder(Asgn asgn) {
    return asgn.simpleNameModel + "Qb";
  }

  def static String simpleNameCustomQueryBuilder(Asgn asgn) {
    return asgn.simpleNameModel + "Qb";
  }

  // extjs
  def static String simpleNameModelUiExtjs(Asgn asgn) {
    return asgn.name + "_Asgn$Model";
  }

  def static String simpleNameFilterUiExtjs(Asgn asgn) {
    return asgn.name + "_Asgn$Filter";
  }

  def static String simpleNameParamUiExtjs(Asgn asgn) {
    return asgn.name + "_Asgn$Param";
  }

  def static String simpleNameListUiExtjs(Asgn asgn) {
    return asgn.name + "_Asgn$List";
  }

  def static String simpleNameWindowUiExtjs(Asgn asgn) {
    return asgn.name + "_Asgn$Window";
  }

  /* ====================== ROOT PACKAGES ====================== */
  def static String getRootPackagePresenter(Asgn asgn) {
    asgn.module.config.presenter.rootPackage + ".presenter"
  }

  def static String getRootPackageUiExtjs(Asgn asgn) {
    asgn.module.config.ui.rootPackage + ".ui.extjs"
  }

  def static String getRootPackageTrl(Asgn asgn) {
    asgn.module.config.trl.rootPackage + ".i18n.extjs"
  }

  /* ====================== PACKAGES ====================== */
  def static String getPackageGeneratedModel(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackagePresenter + ".generated." + unit.name + ".model";
    } else {
      asgn.rootPackagePresenter + ".generated.model";
    }
  }

  def static String getPackageGeneratedFilter(Asgn asgn) {
    return asgn.packageGeneratedModel;
  }

  def static String getPackageGeneratedParam(Asgn asgn) {
    return asgn.packageGeneratedModel;
  }

  def static String getPackageCustomModel(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackagePresenter + ".extensions." + unit.name + ".model";
    } else {
      asgn.rootPackagePresenter + ".extensions.model";
    }
  }

  def static String getPackageCustomFilter(Asgn asgn) {
    return asgn.packageCustomModel;
  }

  def static String getPackageCustomParam(Asgn asgn) {
    return asgn.packageCustomModel;
  }

  def static String getPackageGeneratedModelUiExtjs(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackageUiExtjs + ".generated." + unit.name + ".asgn";
    } else {
      asgn.rootPackageUiExtjs + ".generated.asgn";
    }
  }

  def static String getPackageGeneratedFilterUiExtjs(Asgn asgn) {
    return asgn.packageGeneratedModelUiExtjs;
  }

  def static String getPackageGeneratedParamUiExtjs(Asgn asgn) {
    return asgn.packageGeneratedModelUiExtjs;
  }

  def static String getPackageCustomModelUiExtjs(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackageUiExtjs + ".extensions." + unit.name + ".asgn";
    } else {
      asgn.rootPackageUiExtjs + ".extensions.asgn";
    }
  }

  def static String getPackageCustomFilterUiExtjs(Asgn asgn) {
    return asgn.packageCustomModelUiExtjs;
  }

  def static String getPackageCustomParamUiExtjs(Asgn asgn) {
    return asgn.packageCustomModelUiExtjs;
  }

  def static String getPackageGeneratedService(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackagePresenter + ".generated." + unit.name + ".service";
    } else {
      asgn.rootPackagePresenter + ".generated.service"
    }
  }

  def static String getPackageCustomService(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackagePresenter + ".extensions." + unit.name + ".service";
    } else {
      asgn.rootPackagePresenter + ".extensions.service";
    }
  }

  def static String getPackageGeneratedQueryBuilder(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackagePresenter + ".generated." + unit.name + ".qb";
    } else {
      asgn.rootPackagePresenter + ".generated.qb"
    }
  }

  def static String getPackageCustomQueryBuilder(Asgn asgn) {
    var unit = asgn.unit;
    if (unit != null) {
      asgn.rootPackagePresenter + ".extensions." + unit.name + ".qb";
    } else {
      asgn.rootPackagePresenter + ".extensions.qb";
    }
  }

  /* ====================== CANONICAL NAMES ====================== */
  def static String alias(Asgn asgn) {
    return asgn.module.config.aliasPrefixPresenter + asgn.name + "_Asgn"
  }

  def static String canonicalNameGeneratedModel(Asgn asgn) {
    return asgn.packageGeneratedModel + '.' + asgn.simpleNameModel;
  }

  def static String canonicalNameCustomModel(Asgn asgn) {
    return asgn.packageCustomModel + '.' + asgn.simpleNameModel;
  }

  def static String canonicalNameModel(Asgn asgn) {
    if (asgn.hasCustomModel) {
      return asgn.canonicalNameCustomModel
    } else {
      return asgn.canonicalNameGeneratedModel
    }
  }

  def static String canonicalNameGeneratedModelUiExtjs(Asgn asgn) {
    return asgn.packageGeneratedModelUiExtjs + '.' + asgn.simpleNameModelUiExtjs;
  }

  def static String canonicalNameCustomModelUiExtjs(Asgn asgn) {
    return asgn.packageCustomModelUiExtjs + '.' + asgn.simpleNameModelUiExtjs;
  }

  def static String canonicalNameModelUiExtjs(Asgn asgn) {
    if (asgn.hasCustomModel) {
      return asgn.canonicalNameCustomModelUiExtjs
    } else {
      return asgn.canonicalNameGeneratedModelUiExtjs
    }
  }

  def static String canonicalNameGeneratedParam(Asgn asgn) {
    return asgn.packageGeneratedParam + '.' + asgn.simpleNameParam;
  }

  def static String canonicalNameCustomParam(Asgn asgn) {
    return asgn.packageCustomParam + '.' + asgn.simpleNameParam;
  }

  def static String canonicalNameParam(Asgn asgn) {
    if (asgn.hasCustomParam) {
      return asgn.canonicalNameCustomParam
    } else {
      return asgn.canonicalNameGeneratedParam
    }
  }

  def static String canonicalNameGeneratedParamUiExtjs(Asgn asgn) {
    return asgn.packageGeneratedParamUiExtjs + '.' + asgn.simpleNameParamUiExtjs;
  }

  def static String canonicalNameCustomParamUiExtjs(Asgn asgn) {
    return asgn.packageCustomParamUiExtjs + '.' + asgn.simpleNameParamUiExtjs;
  }

  def static String canonicalNameParamUiExtjs(Asgn asgn) {
    if (asgn.hasCustomParam) {
      return asgn.canonicalNameCustomParamUiExtjs
    } else {
      return asgn.canonicalNameGeneratedParamUiExtjs
    }
  }

  def static String canonicalNameListUiExtjs(Asgn asgn) {
    return asgn.packageGeneratedModelUiExtjs + "." + asgn.simpleNameListUiExtjs;
  }

  def static String canonicalNameWindowUiExtjs(Asgn asgn) {
    return asgn.packageGeneratedModelUiExtjs + "." + asgn.simpleNameWindowUiExtjs;
  }

  def static String canonicalNameGeneratedService(Asgn asgn) {
    return asgn.packageGeneratedService + '.' + asgn.simpleNameGeneratedService;
  }

  def static String canonicalNameCustomService(Asgn asgn) {
    return asgn.packageCustomService + '.' + asgn.simpleNameCustomService;
  }

  def static String canonicalNameService(Asgn asgn) {
    if (asgn.hasCustomService) {
      return asgn.canonicalNameCustomService
    } else {
      return asgn.canonicalNameGeneratedService
    }
  }

  def static String canonicalNameGeneratedQueryBuilder(Asgn asgn) {
    return asgn.packageGeneratedQueryBuilder + '.' + asgn.simpleNameGeneratedQueryBuilder;
  }

  def static String canonicalNameCustomQueryBuilder(Asgn asgn) {
    return asgn.packageCustomQueryBuilder + '.' + asgn.simpleNameCustomQueryBuilder;
  }

  def static String canonicalNameQueryBuilder(Asgn asgn) {
    if (asgn.hasCustomQueryBuilder) {
      return asgn.canonicalNameCustomQueryBuilder
    } else {
      return asgn.canonicalNameGeneratedQueryBuilder
    }
  }

  /* ====================== GENERATED CONTENT ====================== */
  def static boolean hasGeneratedModel(Asgn asgn) {
    return true
  }

  def static boolean hasCustomModel(Asgn asgn) {
    return false
  }

  def static boolean hasModel(Asgn asgn) {
    return asgn.hasGeneratedModel || asgn.hasCustomModel
  }

  def static boolean hasGeneratedFilter(Asgn asgn) {
    return false
  }

  def static boolean hasCustomFilter(Asgn asgn) {
    return false
  }

  def static boolean hasFilter(Asgn asgn) {
    return false
  }

  def static boolean hasGeneratedParam(Asgn asgn) {
    return (asgn.members.findFirst[t|t instanceof AsgnParam] != null )
  }

  def static boolean hasCustomParam(Asgn asgn) {
    return false
  }

  def static boolean hasParam(Asgn asgn) {
    return asgn.hasGeneratedParam || asgn.hasCustomParam
  }

  def static boolean hasGeneratedService(Asgn asgn) {
    return asgn.withCustomService
  }

  def static boolean hasCustomService(Asgn asgn) {
    return asgn.withCustomService
  }

  def static boolean hasService(Asgn asgn) {
    return asgn.hasGeneratedService || asgn.hasCustomService
  }

  def static boolean hasGeneratedQueryBuilder(Asgn asgn) {
    return false
  }

  def static boolean hasCustomQueryBuilder(Asgn asgn) {
    return false
  }

  def static boolean hasQueryBuilder(Asgn asgn) {
    return asgn.hasGeneratedQueryBuilder || asgn.hasCustomQueryBuilder
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  def static String fileLocationGeneratedModel(Asgn asgn) {
    return asgn.module.config.presenter.path + "/src/main/java/" + asgn.canonicalNameModel.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedParam(Asgn asgn) {
    return asgn.module.config.presenter.path + "/src/main/java/" + asgn.canonicalNameParam.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedService(Asgn asgn) {
    return asgn.module.config.presenter.path + "/src/main/java/" + asgn.canonicalNameGeneratedService.pathFromFQN +
      ".java"
  }

  def static String fileLocationCustomService(Asgn asgn) {
    return asgn.module.config.presenter.path + "/src/main/java/" + asgn.canonicalNameCustomService.pathFromFQN +
      ".java"
  }

  def static String fileLocationGeneratedQueryBuilder(Asgn asgn) {
    return asgn.module.config.presenter.path + "/src/main/java/" +
      asgn.canonicalNameGeneratedQueryBuilder.pathFromFQN + ".java"
  }

  def static String fileLocationCustomQueryBuilder(Asgn asgn) {
    return asgn.module.config.presenter.path + "/src/main/java/" + asgn.canonicalNameCustomQueryBuilder.pathFromFQN +
      ".java"
  }

  def static String fileLocationGeneratedModelUiExtjs(Asgn asgn) {
    return asgn.module.config.ui.path + "/src/main/resources/public/" + asgn.canonicalNameModelUiExtjs.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedParamUiExtjs(Asgn asgn) {
    return asgn.module.config.ui.path + "/src/main/resources/public/" + asgn.canonicalNameParamUiExtjs.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedListUiExtjs(Asgn asgn) {
    return asgn.module.config.ui.path + "/src/main/resources/public/" + asgn.canonicalNameListUiExtjs.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedWindowUiExtjs(Asgn asgn) {
    return asgn.module.config.ui.path + "/src/main/resources/public/" + asgn.canonicalNameWindowUiExtjs.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedWindowUiExtjsDpd(Asgn asgn) {
    return asgn.module.config.ui.path + "/src/main/resources/public/" + asgn.canonicalNameWindowUiExtjs.pathFromFQN +
      ".jsdp"
  }
}
