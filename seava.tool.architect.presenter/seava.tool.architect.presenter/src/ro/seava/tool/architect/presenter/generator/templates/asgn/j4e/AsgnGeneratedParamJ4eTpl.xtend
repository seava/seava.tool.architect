package ro.seava.tool.architect.presenter.generator.templates.asgn.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Asgn
import ro.seava.tool.architect.presenter.presenter.AsgnParam

class AsgnGeneratedParamJ4eTpl extends AbstractPresenterTemplate {

  def doJavaClass(Asgn asgn) {
    var CharSequence body = asgn.body_tpl;
    '''
      «copyright»
      package «asgn.packageGeneratedParam»;
      
      «imports»
      «body»
    '''
  }

  def private body_tpl(Asgn asgn) {
    '''   
      public class «asgn.simpleNameParam()» {
        «asgn.field_names_tpl»
        «asgn.fields_tpl»
        «asgn.accessors_tpl»
      }
    '''
  }

  /**
   * Static field names
   */
  def protected field_names_tpl(Asgn asgn) {
    '''
      
      «FOR f : asgn.members.filter(typeof(AsgnParam))»
        public static final String f_«f.name» = "«f.name»";
      «ENDFOR»
    '''
  }

  /**
   * Fields template
   */
  def protected fields_tpl(Asgn asgn) {
    '''
      «FOR f : asgn.members.filter(typeof(AsgnParam))»
        
        private «f.dataDomain.dataType.javaType.simpleNameFromFQN» «f.name»;
      «ENDFOR»
    '''
  }

  /**
   * Getters setters 
   */
  def protected accessors_tpl(Asgn asgn) {
    '''
      «FOR f : asgn.members.filter(typeof(AsgnParam))»
        «var type = f.dataDomain.dataType.javaType.simpleNameFromFQN»
        «var name = f.name»
        «var nameToFirstUpper = f.name.toFirstUpper»
        
        public «type» get«nameToFirstUpper»() {
          return this.«name»;
        }
        
        public void set«nameToFirstUpper»(«type» «name») {
          this.«name» = «name»;
        }
      «ENDFOR»
    '''
  }
}
