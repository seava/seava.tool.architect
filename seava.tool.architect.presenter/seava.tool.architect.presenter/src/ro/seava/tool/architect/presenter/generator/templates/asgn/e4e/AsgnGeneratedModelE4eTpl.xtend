package ro.seava.tool.architect.presenter.generator.templates.asgn.e4e

import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Asgn
import ro.seava.tool.architect.presenter.presenter.AsgnField

class AsgnGeneratedModelE4eTpl extends AbstractPresenterTemplate {

  def doJsClass(Asgn asgn) {
    '''
      «copyright»
      Ext.define("«asgn.canonicalNameGeneratedModelUiExtjs»", {
        extend: 'Ext.data.Model',
        statics: {
          ALIAS: "«asgn.alias»"
        },
        fields:  [
          «FOR f : asgn.members.filter(typeof(AsgnField)) SEPARATOR ','»
            {name:"«f.name»", type:"«f.source.dataDomain.dataType.extjsType»"}
          «ENDFOR»
        ]
      });
    '''
  }
}
