package ro.seava.tool.architect.presenter.extensions

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.presenter.presenter.P_Package
import ro.seava.tool.architect.presenter.presenter.VmDelegate
import ro.seava.tool.architect.project.project.Module
import ro.seava.tool.architect.project.project.Unit

class VmDelegate_Cfg {

  /* ====================== VM OWNERS ====================== */
  /**
   * Get containing package
   */
  def static P_Package getPackage(VmDelegate d) {
    return d.eContainer as P_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(VmDelegate d) {
    return d.package.module;
  }

  /**
   * Get containing unit
   */
  def static Unit getUnit(VmDelegate d) {
    return d.package.unit;
  }

  /* ====================== SIMPLE NAMES ====================== */
  // java
  def static String simpleName(VmDelegate d) {
    return d.name + "_Pd";
  }

  /* ====================== ROOT PACKAGES ====================== */
  def static String getRootPackagePresenter(VmDelegate d) {
    d.module.config.presenter.rootPackage + ".presenter"
  }

  /* ====================== PACKAGES ====================== */
  def static String getPackageDelegate(VmDelegate d) {
    var unit = d.unit;
    if (unit != null) {
      d.rootPackagePresenter + ".extensions." + unit.name + ".delegate";
    } else {
      d.rootPackagePresenter + ".extensions.delegate";
    }
  }

  /* ====================== CANONICAL NAMES ====================== */
  def static String canonicalName(VmDelegate d) {
    return d.packageDelegate + '.' + d.simpleName;
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  def static String fileLocation(VmDelegate d) {
    return d.module.config.presenter.path + "/src/main/java/" + d.canonicalName.pathFromFQN + ".java"
  }

}
