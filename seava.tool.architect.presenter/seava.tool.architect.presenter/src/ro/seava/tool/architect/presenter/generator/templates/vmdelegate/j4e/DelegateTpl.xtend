package ro.seava.tool.architect.presenter.generator.templates.vmdelegate.j4e

import static extension ro.seava.tool.architect.presenter.extensions.VmDelegate_Cfg.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.VmDelegate

class DelegateTpl extends AbstractPresenterTemplate {

  def doJavaClass(VmDelegate d) {
    var CharSequence body = d.doBody;
    '''
      «copyright»
      package «d.packageDelegate»;
      
      «imports»
      
      «body»
    '''
  }

  def private doBody(VmDelegate d) {
    '''
      public class «d.simpleName» extends «"seava.lib.j4e.presenter.service.AbstractPresenterDelegate".simpleNameFromFQN» {
        //TODO: implement me ...
      }
    '''
  }
}
