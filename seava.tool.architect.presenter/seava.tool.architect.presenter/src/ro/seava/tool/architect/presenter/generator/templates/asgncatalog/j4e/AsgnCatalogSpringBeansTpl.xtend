package ro.seava.tool.architect.presenter.generator.templates.asgncatalog.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*
import static extension ro.seava.tool.architect.business.extensions.Bm_Ext.*
import static extension ro.seava.tool.architect.business.extensions.BmMember_Ext.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.AsgnCatalog
import ro.seava.tool.architect.presenter.presenter.Asgn
import ro.seava.tool.architect.business.business.BmRefField

class AsgnCatalogSpringBeansTpl extends AbstractPresenterTemplate {

  def doBeansXml(AsgnCatalog c) {
    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
        xsi:schemaLocation="
            http://www.springframework.org/schema/beans
              http://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context 
              http://www.springframework.org/schema/context/spring-context.xsd">
        «FOR asgn : c.items»
          «asgn.bean_declaration_tpl»
        «ENDFOR»
      </beans>
    '''
  }

  /**
   * 
   */
  def private bean_declaration_tpl(Asgn asgn) {
    if (asgn.hasCustomService) {
      return asgn.custom_bean_tpl
    } else if (asgn.hasGeneratedService) {
      return asgn.own_bean_tpl
    } else {
      return asgn.default_bean_tpl
    }
  }

  /**
   * 
   */
  def private default_bean_tpl(Asgn asgn) {
    '''
      
      <bean id="«asgn.alias»" scope="singleton" lazy-init="true"
        class="seava.lib.j4e.presenter.service.asgn.DefaultAsgnService">
        «asgn.properties_tpl»
      </bean>
    '''
  }

  /**
   * 
   */
  def private own_bean_tpl(Asgn asgn) {
    '''
      
      <bean id="«asgn.alias»" scope="singleton" lazy-init="true"
        class="«asgn.canonicalNameService»">
        «asgn.properties_tpl»
      </bean>
    '''
  }

  /**
   * 
   */
  def private custom_bean_tpl(Asgn asgn) {
    '''
      
      <bean id="«asgn.alias»" scope="singleton" lazy-init="true"
        class="«asgn.canonicalNameCustomService»">
        «asgn.properties_tpl»
      </bean>
    '''
  }

  /**
   * 
   */
  def private properties_tpl(Asgn asgn) {
    '''
      <property name="modelClass" value="«asgn.canonicalNameModel»"/>
      <property name="filterClass" value="«asgn.canonicalNameModel»"/>
      «IF asgn.hasParam»
        <property name="paramClass" value="«asgn.canonicalNameParam»"/>
      «ENDIF»
      <property name="entityClass" value="«asgn.sourceBm.canonicalNameModel»"/>
      «IF asgn.hasQueryBuilder»
        <property name="queryBuilderClass" value="«asgn.canonicalNameQueryBuilder»" />
      «ENDIF»
      <property name="ctx">
        <bean class="seava.lib.j4e.commons.descriptor.AsgnContext">
          <property name="leftTable" value="«asgn.sourceBm.tableName»"/>
        <property name="rightTable" value="«IF asgn.contextField.joinSource != null»«asgn.contextField.joinSource»«ELSE»«(asgn.
        mappedByField as BmRefField).joinSource»«ENDIF»"/>
        <property name="rightItemIdField" value="«asgn.contextField.columnName»"/>
        <property name="rightObjectIdField" value="«asgn.mappedByField.columnName»"/>
        </bean>
        </property>
    '''
  }

}
