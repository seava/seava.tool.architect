package ro.seava.tool.architect.presenter.extensions

import static extension ro.seava.tool.architect.presenter.scoping.PresenterScopeUtils.*

import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.presenter.presenter.VmSource
import ro.seava.tool.architect.presenter.presenter.IVmField
import ro.seava.tool.architect.presenter.presenter.Vm
import java.util.List

class Vm_Ext extends Vm_Cfg {

  /**
   * 
   */
  def static Bm resolveBm(VmSource vmSource) {
    return vmSource.root.source;
  }

  /**
   * 
   */
  def static IVmField getIdField(Vm vm) {
    return vm.getFieldByName("id")
  }

  /**
   * 
   */
  def static IVmField getClientIdField(Vm vm) {
    return vm.getFieldByName("clientId")
  }

  def static IVmField getFieldByName(Vm vm, String fieldName) {
    var List<IVmField> _r = vm.collect_members_as_type(IVmField).filter(e|e.name.matches(fieldName)).toList;
    if (_r.size > 0) {
      return _r.get(0);
    }
    return null;
  }

  /**
   * 
   */
  def static List<IVmField> getRangedFilterFields(Vm vm) {
    vm.collect_members_as_type(IVmField).filter(e|e.config.filterRange == true).toList;
  }

}
