package ro.seava.tool.architect.presenter.generator.templates.vm.e4e

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.presenter.scoping.PresenterScopeUtils.*

import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.presenter.IVmField
import ro.seava.tool.architect.presenter.presenter.VmMemberValuesUsage
import ro.seava.tool.architect.presenter.presenter.VmMemberValidationUsage
import ro.seava.tool.architect.presenter.presenter.VmMemberValues

class VmGeneratedModelE4eTpl extends AbstractVmE4e {

  def doExtjsClass(Vm vm) {

    var VmMemberValues initData = null;
    if (vm.defaultValues != null) {
      initData = vm.defaultValues.memberValues.findFirst(t|t.usage == VmMemberValuesUsage::INIT_RECORD_CLIENT);
    }

    '''
      «copyright»
      
      Ext.define("«vm.canonicalNameModelUiExtjs»", {
        extend: 'Ext.data.Model',
        
        statics: {
          ALIAS: "«vm.alias»"
        },
        «IF !vm.readOnly && vm.validations != null &&
        vm.validations.rules.exists[t|t.usage == VmMemberValidationUsage.CLIENT]»
          
          validators: {
            «FOR rule : vm.validations.rules.findFirst[t|t.usage == VmMemberValidationUsage.CLIENT].rules SEPARATOR ','» 
              «rule.member.name»: [«IF rule.isRequired»{ type: 'presence'}«ENDIF»]
            «ENDFOR»
          },
        «ELSEIF vm.superType != null && vm.superType.validations != null &&
        vm.superType.validations.rules.exists[t|t.usage == VmMemberValidationUsage.CLIENT]»
          
          validators: {
              «FOR rule : vm.superType.validations.rules.findFirst[t|t.usage == VmMemberValidationUsage.CLIENT].rules  SEPARATOR ','» 
                «rule.member.name»: [«IF rule.isRequired»{type: 'presence'}«ENDIF»]
              «ENDFOR»
          },
        «ENDIF»     
        «IF initData != null»
          
          initRecord: function() {
            «FOR f : initData.values»
              this.set("«f.member.name»", «f.value._init_val_tpl(f.member)»);
            «ENDFOR»
          },
        «ENDIF»
        
        fields: [
          «FOR f : vm.collect_members_as_type(IVmField) SEPARATOR ","»
            {name:"«f.name»", type:"«f.dataType.extjsType»"«f.modelDataFormat»«IF f.dataType.isNumber()», useNull:true«ENDIF»}
          «ENDFOR»
        ]
      });
    '''
  }

}
