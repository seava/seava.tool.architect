package ro.seava.tool.architect.presenter.generator.templates.vm.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*

import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate

class VmGeneratedFilterJ4eTpl extends AbstractPresenterTemplate {

  def doJavaClass(Vm vm) {
    var CharSequence body = vm.doBody;
    '''
      «copyright»
      package «vm.packageGeneratedFilter»;
      «imports»
      «body»
    '''
  }

  def private doBody(Vm vm) {
    '''
      
      /**
       * Helper filter object to run query by example with range values.
       *
       */
      public class «vm.simpleNameFilter» extends «vm.canonicalNameModel.simpleNameFromFQN» {
       «vm.fields_tpl»
        «vm.accessors_tpl»
      }
    '''
  }

  /**
   * 
   */
  def private fields_tpl(Vm vm) {
    '''
      «FOR f : vm.members.filter(t|t.config.filterRange)»
        «var type = f.dataType.javaType.simpleNameFromFQN»
        «var name = f.name»
        private «type» «name»_From;
        private «type» «name»_To;
      «ENDFOR»  
      «FOR f : vm.members.filter(t|t.config.getterFn != null)»
        private «f.dataType.javaType.simpleNameFromFQN()» «f.name»;
      «ENDFOR»
    '''
  }

  /**
   * 
   */
  def private accessors_tpl(Vm vm) {
    var list = vm.members.filter(t|t.config.filterRange).toList;
    list.addAll(vm.members.filter(t|t.config.getterFn != null))
    '''
      «FOR f : list»
        «var simpleJavaType = f.dataType.javaType.simpleNameFromFQN»
        «var name = f.name»
        «var nameToFirstUpper = f.name.toFirstUpper»
        
          public «simpleJavaType» get«nameToFirstUpper»_From() {
            return this.«name»_From;
          }
          public «simpleJavaType» get«nameToFirstUpper»_To() {
            return this.«name»_To;
          }
      «ENDFOR»
    '''
  }
}
