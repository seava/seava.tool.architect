package ro.seava.tool.architect.presenter.extensions

import ro.seava.tool.architect.presenter.presenter.IVmMember
import ro.seava.tool.architect.base.base.DataType
import ro.seava.tool.architect.base.base.DataDomain
import ro.seava.tool.architect.presenter.presenter.VmParam
import ro.seava.tool.architect.presenter.presenter.VmAttrField
import ro.seava.tool.architect.presenter.presenter.IVmFromItem
import ro.seava.tool.architect.presenter.presenter.VmFromBm
import ro.seava.tool.architect.presenter.presenter.VmFromAlias
import ro.seava.tool.architect.presenter.presenter.VmFromJoin
import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.presenter.presenter.VmRefField
import ro.seava.tool.architect.business.business.BmAttrField
import ro.seava.tool.architect.presenter.presenter.IVmField

class VmMember_Ext {

  def static String getFieldNameStaticConstantName(IVmMember m) {
    return "f_" + m.name;
  }

  /**
   * 
   */
  def static DataType dataType(IVmMember m) {
    if (m instanceof VmParam) {
      return (m as VmParam).dataDomain.dataType;
    } else if (m instanceof VmAttrField) {
      return (m as VmAttrField).dataDomain.dataType;
    } else if (m instanceof VmRefField) {
      return (m as VmRefField).sourceField.dataDomain.dataType;
    }
  }

  def static DataDomain dataDomain(IVmMember m) {
    if (m instanceof VmParam) {
      return (m as VmParam).dataDomain;
    } else if (m instanceof VmAttrField) {
      return (m as VmAttrField).dataDomain;
    } else if (m instanceof VmRefField) {
      return (m as VmRefField).sourceField.dataDomain;
    }
  }

  /**
   * 
   */
  def static Bm resolveBm(IVmFromItem fromItem) {
    if (fromItem instanceof VmFromBm) {
      return (fromItem as VmFromBm).source;
    } else if (fromItem instanceof VmFromAlias) {
      return (fromItem as VmFromAlias).sourceRefField.bm;
    } else if (fromItem instanceof VmFromJoin) {
      return (fromItem as VmFromJoin).source;
    }
    return null;
  }

  def static boolean isParam(IVmMember m) {
    return (  m instanceof VmParam )
  }

  def static boolean isRefField(IVmMember m) {
    return (  m instanceof VmRefField )
  }

  def static boolean isAttrField(IVmMember m) {
    return (  m instanceof VmAttrField  )
  }

  def static boolean isDeep(IVmField f) {
    if (f instanceof VmRefField) {
      var _f = (f as VmRefField)
      if (_f.vmFromItem != null && _f.vmFromItem instanceof VmFromAlias) {
        return true
      }
    }
    return false
  }

  /**
   * 
   */
  def static String getRefPath(IVmField f) {
    if (f instanceof VmRefField) {
      var _f = f as VmRefField;
      if (_f.vmFromItem instanceof VmFromBm) {
        return _f.sourceField.name;
      } else {
        var IVmFromItem fromItem = _f.vmFromItem;
        var StringBuffer sb = new StringBuffer(_f.sourceField.name);
        while (fromItem instanceof VmFromAlias) {
          var VmFromAlias _i = (fromItem as VmFromAlias);
          sb.insert(0, ".");
          sb.insert(0, _i.sourceRefField.name);
          fromItem = _i.vmFromItem
        }
        return sb.toString;
      }
    }
  }

  /**
   * 
   */
  def static String vmFieldAnnotationAttributes(IVmField field) {

    var StringBuffer sb = new StringBuffer();
    var BmAttrField sourceField;

    if (field instanceof VmRefField) {
      sourceField = (field as VmRefField).sourceField;
    }

    if (field.config.isDisabledInsert || field.config.isDisabled) {
      sb.append("noInsert=true");
    }
    if (field.config.isDisabledUpdate || field.config.isDisabled) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append("noUpdate=true");
    }
    if (field.deep) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append("join=\"left\"");
    }

    //    if (sourceField != null && sourceField.getOrderByFields().size() > 0) {
    //      if (sb.length() > 0) {
    //        sb.append(", ");
    //      }
    //      sb.append("orderBy=\"");
    //      var List<BmAttrField> sortfields = sourceField.getOrderByFields();
    //      var int x = 0;
    //      for (BmAttrField sortfield : sortfields) {
    //        if (x > 0) {
    //          sb.append(",");
    //        }
    //        sb.append(sortfield.getName());
    //        x = x + 1;
    //      }
    //      sb.append("\"");
    //    }
    if (sourceField == null || (sourceField != null && sourceField.config.transient )) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append("fetch=false");
    }
    if (field.config.filterBy != null) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append("jpqlFilter=\"" + field.config.filterBy + "\"");
    }
    if (sourceField != null && !field.refPath.equals(field.getName())) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append("path=\"" + field.refPath + "\"");
    }
    return sb.toString();

  }

}
