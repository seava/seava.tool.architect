package ro.seava.tool.architect.presenter.generator.templates.vm.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*
import static extension ro.seava.tool.architect.business.extensions.BmMember_Ext.*
import static extension ro.seava.tool.architect.presenter.scoping.PresenterScopeUtils.*

import java.util.HashMap
import java.util.Map

import java.util.List
import java.util.ArrayList
import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.presenter.IVmField
import ro.seava.tool.architect.presenter.presenter.VmOrderByField
import ro.seava.tool.architect.presenter.presenter.VmRefLookupByUk
import ro.seava.tool.architect.presenter.presenter.VmFieldCall
import ro.seava.tool.architect.business.business.IBmField
import ro.seava.tool.architect.business.business.BmRefField
import ro.seava.tool.architect.presenter.presenter.VmRefField
import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate

class VmGeneratedModelJ4eTpl extends AbstractPresenterTemplate {

  def doJavaClass(Vm vm) {
    var CharSequence body = vm.doBody;
    '''
      «copyright»
      package «vm.packageGeneratedModel»;
      
      «imports»
      
      «body»
    '''
  }

  def private doBody(Vm vm) {
    var List<IVmField> fields = vm.collect_members_as_type(IVmField, true)
    '''
      «vm.docs»
      «IF !vm.isAbstract»«vm.annotations_tpl»«ENDIF»
      public class «vm.simpleNameModel»«IF vm.isAbstract»<E>«ENDIF»«vm.extends_tpl»«vm.implements_tpl» {
      	
      	public static final String ALIAS = "«vm.alias»";
      	
      	«fields.field_names_tpl»
      	«fields.fields_tpl»
      	
      	public «vm.simpleNameModel»() {
      		super();
      	}
      	
      	public «vm.simpleNameModel»(«IF vm.isAbstract»E«ELSE»«vm.source.resolveBm.name»«ENDIF» e) {
      		super(e);
      	}
      	«fields.accessors_tpl»
      }
    '''
  }

  /**
   * Static field name constants
   */
  def private field_names_tpl(List<IVmField> fields) {
    '''
      «FOR f : fields»
        public static final String «f.fieldNameStaticConstantName» = "«f.name»";
      «ENDFOR»
    '''
  }

  /**
   * Field declarations
   */
  def private fields_tpl(List<IVmField> fields) {
    '''
      «FOR f : fields»
        
        @«simpleNameFromFQN("seava.lib.j4e.api.presenter.annotation.DsField")»«var xx = f.vmFieldAnnotationAttributes()»«IF xx !=
        null && !xx.matches("")»(«xx»)«ENDIF»
        private «f.dataType.javaType.simpleNameFromFQN» «f.name»;
      «ENDFOR»
    '''
  }

  /**
   * 
   */
  def private accessors_tpl(List<IVmField> fields) {
    '''
      «FOR f : fields»       
        «var type = f.dataType.javaType.simpleNameFromFQN»
        «var name = f.name»
        «var nameToFirstUpper = f.name.toFirstUpper»
        
        public «type» get«nameToFirstUpper»() {
          «IF f.config.getterFn != null»«f.config.getterFn»«ELSE»return this.«name»;«ENDIF»
        }
        
        public void set«nameToFirstUpper»(«type» «name») {
          «IF f.config.setterFn != null»«f.config.setterFn»«ELSE»this.«name» = «name»;«ENDIF»
        }
      «ENDFOR»
    '''
  }

  /**
   * 
   */
  def private default_filter_tpl(Vm vm) {
    if (vm.filterBy != null) {
      ''',jpqlWhere="«vm.filterBy»"'''
    } else if (vm.superType != null && vm.superType.filterBy != null) {
      ''',jpqlWhere="«vm.superType.filterBy»"'''
    }
  }

  /**
   * 
   */
  def private default_sort_tpl(Vm vm) {
    if (vm.orderBy != null) {
      ''', sort={«FOR token : vm.orderBy.fields SEPARATOR ', '»@«simpleNameFromFQN(
        "seava.lib.j4e.api.presenter.annotation.SortField")»(«token.sort_field_tpl(vm)»)«ENDFOR»}'''
    } else if (vm.superType != null && vm.superType.orderBy != null) {
      ''', sort={«FOR token : vm.superType.orderBy.fields SEPARATOR ', '»@«simpleNameFromFQN(
        "seava.lib.j4e.api.presenter.annotation.SortField")»(«token.sort_field_tpl(vm)»)«ENDFOR»}'''
    }
  }

  /**
   * 
   */
  def private sort_field_tpl(VmOrderByField token, Vm vm) {
    '''field=«vm.simpleNameModel».«token.field.fieldNameStaticConstantName»«IF token.desc», desc=true«ENDIF»'''
  }

  /**
   * 
   */
  def private extends_tpl(Vm vm) {
    if (vm.isAbstract) {
      if (vm.superType != null) {
        ''' extends «simpleNameFromFQN(vm.superType.canonicalNameModel)»<E>'''
      } else {
        ''' extends «simpleNameFromFQN("seava.lib.j4e.presenter.model.AbstractDsModel")»<E>'''
      }
    } else {
      if (vm.superType != null) {
        ''' extends «simpleNameFromFQN(vm.superType.canonicalNameModel)»<«vm.source.resolveBm.name»>'''
      } else {
        ''' extends «simpleNameFromFQN("seava.lib.j4e.presenter.model.AbstractDsModel")»<«vm.source.resolveBm.
          name»>'''
      }
    }
  }

  /**
   * 
   */
  def private implements_tpl(Vm vm) {
    var CharSequence cs = ''''''
    var idField = vm.idField;
    var clientIdField = vm.clientIdField;
    if (idField != null) {
      cs = ''' «simpleNameFromFQN("seava.lib.j4e.api.base.descriptor.IModelWithId")»<«idField.dataType.javaType.
        simpleNameFromFQN»>'''
    }
    if (clientIdField != null) {
      if (cs.length > 0) {
        cs = cs + ''', ''';
      }
      cs = cs + ''' «simpleNameFromFQN("seava.lib.j4e.api.base.descriptor.IModelWithClientId")»'''
    }
    if (cs.length > 0) {
      cs = ''' implements''' + cs;
    }
    return cs;
  }

  def private annotations_tpl(Vm vm) {
    var _a = '''entity=«vm.source.resolveBm.canonicalNameModel.simpleNameFromFQN».class«vm.default_filter_tpl»«vm.
      default_sort_tpl»'''
    var List<VmRefLookupByUk> _refLookups = new ArrayList<VmRefLookupByUk>();
    if (vm.refLookupRules!= null && vm.refLookupRules.rules.size > 0) {
      for (lr : vm.refLookupRules.rules) {
        if (lr instanceof VmRefLookupByUk) {
          _refLookups.add(lr as VmRefLookupByUk);
        }
      }
    }

    '''
      @«simpleNameFromFQN("seava.lib.j4e.api.presenter.annotation.Ds")»(«_a»)
      «IF vm.hints != null && vm.hints.size > 0»
        @«simpleNameFromFQN("seava.lib.j4e.api.presenter.annotation.DsQueryHints")»({
        «FOR hint : vm.hints SEPARATOR ','»
          @«simpleNameFromFQN("javax.persistence.QueryHint")»(name=«simpleNameFromFQN(
        "org.eclipse.persistence.config.QueryHints")».«hint.hint.name», value="«hint.value»")
        «ENDFOR»
        })
      «ENDIF»
      «IF vm.refLookupRules!= null && vm.refLookupRules.rules.size > 0»
        @«simpleNameFromFQN("seava.lib.j4e.api.presenter.annotation.RefLookups")»({
        	«FOR lr : _refLookups SEPARATOR ','»
        	  @«simpleNameFromFQN("seava.lib.j4e.api.presenter.annotation.RefLookup")»(«vm.ref_lookup_annotation_body_tpl(lr)»)
        	«ENDFOR»
        })
      «ENDIF»
    '''
  }

  def private ref_lookup_annotation_body_tpl(Vm vm, VmRefLookupByUk lr) {

    var field = lr.ref;
    var entity = (field as VmRefField).vmFromItem.resolveBm;

    var tpl_refId = '''refId = «vm.simpleNameModel».«lr.ref.getFieldNameStaticConstantName»''';

    if (lr.ukRule != null) {
      var List<VmFieldCall> keys = new ArrayList<VmFieldCall>();
      var Map<VmFieldCall, IBmField> map = new HashMap<VmFieldCall, IBmField>();
      var int j = 0;

      for (f : lr.fields) {
        keys.add(f);
        map.put(f, lr.ukRule.fields.get(j));
        j = j + 1;
      }
      var tpl_nq = '''namedQuery = «entity.canonicalNameModel.simpleNameFromFQN».NQ_FIND_BY_«lr.ukRule.name.
        replace("uk_", "").toUpperCase»«IF lr.ukRule.fields.filter(t|t.refField == true).size > 0»_PRIMITIVE«ENDIF»''';
      var tpl_nqp = '''
      params = {
      	«FOR key : keys SEPARATOR ','»
      	  @«simpleNameFromFQN("seava.lib.j4e.api.presenter.annotation.Param")»(name = "«map.get(key).name»«IF map.get(key) instanceof BmRefField»Id«ENDIF»", «IF key.
        staticValue != null»value = "«key.staticValue»"«ELSE»field = «vm.simpleNameModel».«key.field.
        getFieldNameStaticConstantName»«ENDIF»)
      	«ENDFOR»
      }''';
      return '''«tpl_refId», «tpl_nq», «tpl_nqp»'''
    } else {
      return '''«tpl_refId»'''
    }
  }

// ==================== utilities =================== 
}
