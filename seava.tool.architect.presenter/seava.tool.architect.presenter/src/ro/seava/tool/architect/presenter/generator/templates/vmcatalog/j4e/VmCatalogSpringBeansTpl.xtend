package ro.seava.tool.architect.presenter.generator.templates.vmcatalog.j4e

import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.Vm_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.VmDelegate_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*

import ro.seava.tool.architect.presenter.presenter.VmCatalog
import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.presenter.Rpc
import ro.seava.tool.architect.presenter.presenter.E_Rpc
import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate

class VmCatalogSpringBeansTpl extends AbstractPresenterTemplate {

  def doBeansXml(VmCatalog c) {
    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
        xsi:schemaLocation="
            http://www.springframework.org/schema/beans
              http://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context 
              http://www.springframework.org/schema/context/spring-context.xsd">
        «FOR vm : c.items»
          «vm.bean_declaration_tpl»
        «ENDFOR»
      </beans>
    '''
  }

  def private bean_declaration_tpl(Vm vm) {
    if (vm.hasCustomService) {
      return vm.custom_bean_tpl
    } else if (vm.hasGeneratedService) {
      return vm.generated_bean_tpl
    } else {
      return vm.default_bean_tpl
    }
  }

  /**
   * 
   */
  def private default_bean_tpl(Vm vm) {
    '''
      
      <bean id="«vm.alias»" scope="singleton" lazy-init="true"
        class="seava.lib.j4e.presenter.service.ds.DefaultEntityDsService">
        «vm.properties_tpl»
        «vm.rpc_tpl»
      </bean>
    '''
  }

  /**
   * 
   */
  def private generated_bean_tpl(Vm vm) {
    '''
      
      <bean id="«vm.alias»" scope="singleton" lazy-init="true"
        class="«vm.canonicalNameService»">
        «vm.properties_tpl»
        «vm.rpc_tpl»
      </bean>
    '''
  }

  /**
   * 
   */
  def private custom_bean_tpl(Vm vm) {
    '''
      
      <bean id="«vm.alias»" scope="singleton" lazy-init="true"
        class="«vm.canonicalNameCustomService»">
        «vm.properties_tpl»
        «vm.rpc_tpl»
      </bean>
    '''
  }

  /**
   * 
   */
  def private properties_tpl(Vm vm) {
    '''
      <property name="modelClass" value="«vm.canonicalNameModel»"/>
      <property name="entityClass" value="«vm.source.resolveBm.canonicalNameModel»"/>
      «IF vm.hasFilter»
        <property name="filterClass" value="«vm.canonicalNameFilter»"/>
      «ENDIF»
      «IF vm.hasParam»
        <property name="paramClass" value="«vm.canonicalNameParam»"/>
      «ENDIF»        
      «IF vm.hasQueryBuilder»
        <property name="queryBuilderClass" value="«vm.canonicalNameQueryBuilder»" />
      «ENDIF» 
      «IF vm.hasConverter»
        <property name="converterClass" value="«vm.canonicalNameConverter»" />
      «ENDIF» 
      «IF vm.noInsert»
        <property name="noInsert" value="true" />
      «ENDIF» 
      «IF vm.noUpdate»
        <property name="noUpdate" value="true" />
      «ENDIF»
      «IF vm.noDelete»
        <property name="noDelete" value="true" />
      «ENDIF»
      «IF vm.readOnly»
        <property name="readOnly" value="true" />
      «ENDIF»
      «IF vm.summaries != null»
        <property name="summaryRules" >
          <map>
            «FOR s : vm.summaries.rules»
              <entry key="«s.vmField.name»" value="«s.type.getName().toLowerCase»(e.«IF s.source != null»«s.source.refPath»«ELSE»«s.
        vmField.refPath»«ENDIF»)" />
            «ENDFOR»
          </map>  
        </property>
      «ENDIF»
    '''
  }

  /**
   * Generate rpc configuration. Collect all rpc types
   */
  def private rpc_tpl(Vm vm) {
    '''
      «vm.rpc_filter_tpl»
      «vm.rpc_data_tpl»
    '''
  }

  /**
   * Generate rpc filter configuration
   */
  def private rpc_filter_tpl(Vm vm) {
    var Iterable<Rpc> list = vm.rpcList.filter[t|t.type == E_Rpc.RPC_FILTER]
    if (list.size > 0) {
      '''
        <property name="rpcFilter" >
          <map>
            «FOR rpc : list»
              <entry key="«rpc.name»">
                <bean class="seava.lib.j4e.presenter.descriptor.viewmodel.RpcDefinition">
                  <constructor-arg name="delegateClass" value="«rpc.delegate.canonicalName»"/>
                  <constructor-arg name="methodName" value="«rpc.method.name»" />
                </bean>
              </entry>
            «ENDFOR»
          </map>  
        </property>
      '''
    }
  }

  /**
   * Generate rpc data configuration
   */
  def private rpc_data_tpl(Vm vm) {
    var Iterable<Rpc> list = vm.rpcList.filter[t|t.type == E_Rpc.RPC_DATA || t.type == E_Rpc.RPC_LIST_DATA]
    if (list.size > 0) {
      '''
        <property name="rpcData" >
          <map>
            «FOR rpc : list»
              <entry key="«rpc.name»">
                <bean class="seava.lib.j4e.presenter.descriptor.viewmodel.RpcDefinition">
                  <constructor-arg name="delegateClass" value="«rpc.delegate.canonicalName»"/>
                  <constructor-arg name="methodName" value="«rpc.method.name»" />
                </bean>
              </entry>
            «ENDFOR»
          </map>  
        </property>
      '''
    }
  }

}
