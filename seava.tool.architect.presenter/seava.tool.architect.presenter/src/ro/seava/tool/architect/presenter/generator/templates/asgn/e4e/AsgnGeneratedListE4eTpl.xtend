package ro.seava.tool.architect.presenter.generator.templates.asgn.e4e

import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Asgn
import ro.seava.tool.architect.presenter.presenter.AsgnField

class AsgnGeneratedListE4eTpl extends AbstractPresenterTemplate {

  def doJsClass(Asgn asgn) {
    '''
      «copyright»
      Ext.define( "«asgn.canonicalNameListUiExtjs»", {
        extend: "seava.lib.e4e.js.asgn.AbstractAsgnGrid",
        alias:[ "widget.«asgn.simpleNameModel»List$Left","widget.«asgn.simpleNameModel»List$Right" ],
        _defineColumns_: function () {
          this._getBuilder_()
          «FOR f : asgn.members.filter(typeof(AsgnField))»
            .addTextColumn({name:"«f.name»", dataIndex:"«f.name»"«IF f.noSort», sortable:false«ENDIF»«IF f.notVisible», hidden:true«ENDIF»«IF f.
        width > 0», width:«f.width»«ENDIF»})
          «ENDFOR»             
        } 
      });
    '''
  }
}
