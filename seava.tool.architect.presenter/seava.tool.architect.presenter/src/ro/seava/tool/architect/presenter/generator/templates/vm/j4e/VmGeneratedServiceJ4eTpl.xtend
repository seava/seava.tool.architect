package ro.seava.tool.architect.presenter.generator.templates.vm.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Ext.*
import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Vm

class VmGeneratedServiceJ4eTpl extends AbstractPresenterTemplate {

  /**
   * 
   */
  def doJavaClass(Vm vm) {
    var CharSequence body = vm.body_tpl;
    ''' 
      «copyright»
      package «vm.packageGeneratedService»;
      
      «imports»
      
      «body»
    '''
  }

  /**
   * 
   */
  def private body_tpl(Vm vm) {
    '''
      public class «vm.simpleNameCustomService» «vm.extends_tpl» «vm.implements_tpl» {
          
        «vm.model_classes_tpl»
        
      }
    '''
  }

  def model_classes_tpl(Vm vm) {
    ''''''
  }

  /**
   * 
   */
  def extends_tpl(Vm vm) {
    '''extends «"seava.lib.j4e.presenter.service.ds.AbstractEntityDsService".simpleNameFromFQN»<«vm.
      canonicalNameModel.simpleNameFromFQN», «IF vm.hasFilter»«vm.canonicalNameFilter.simpleNameFromFQN»«ELSE»«vm.
      canonicalNameModel.simpleNameFromFQN»«ENDIF», «IF vm.hasParam»«vm.canonicalNameParam.simpleNameFromFQN»«ELSE»Object«ENDIF», «vm.
      source.resolveBm.canonicalNameModel.simpleNameFromFQN()»>'''

  }

  /**
   * 
   */
  def implements_tpl(Vm vm) {
    '''implements «simpleNameFromFQN("seava.lib.j4e.api.presenter.service.IDsService")»<«vm.canonicalNameModel().
      simpleNameFromFQN»,«IF vm.hasFilter»«vm.canonicalNameFilter.simpleNameFromFQN»«ELSE»«vm.canonicalNameModel().
      simpleNameFromFQN»«ENDIF», «IF vm.hasParam»«vm.canonicalNameParam.simpleNameFromFQN»«ELSE»Object«ENDIF»>'''

  }

}
