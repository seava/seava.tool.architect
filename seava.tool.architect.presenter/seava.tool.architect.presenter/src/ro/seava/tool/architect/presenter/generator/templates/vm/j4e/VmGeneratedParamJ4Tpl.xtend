package ro.seava.tool.architect.presenter.generator.templates.vm.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.presenter.scoping.PresenterScopeUtils.*

import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.presenter.VmParam
import java.util.List
import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate

class VmGeneratedParamJ4Tpl extends AbstractPresenterTemplate {

  def doJavaClass(Vm vm) {
    var CharSequence body = vm.doBody;
    '''
      «copyright»
      package «vm.packageGeneratedParam»;
      
      «imports»
      «body»
    '''
  }

  def private doBody(Vm vm) {
    var List<VmParam> params = vm.collect_members_as_type(VmParam, true)
    '''   
      public class «vm.simpleNameParam» {
        «params.field_names_tpl»
        «params.fields_tpl»
        «params.accessors_tpl»
      }
    '''
  }

  def private field_names_tpl(List<VmParam> params) {
    '''
      
      «FOR p : params»
        «var name = p.name»
        public static final String f_«name» = "«name»";
      «ENDFOR»
    '''
  }

  def private fields_tpl(List<VmParam> params) {
    '''
      «FOR p : params»
        
        private «p.dataType.javaType.simpleNameFromFQN»  «p.name»;
      «ENDFOR»
    '''
  }

  def private accessors_tpl(List<VmParam> params) {
    '''
      «FOR p : params»
        «var type = p.dataType.javaType.simpleNameFromFQN»
        «var name = p.name»
        «var nameToFirstUpper = p.name.toFirstUpper»
        
        public «type» get«nameToFirstUpper»() {
          return this.«name»;
        }
        
        public void set«nameToFirstUpper»(«type» «name») {
          this.«name» = «name»;
        }
      «ENDFOR»
    '''
  }

}
