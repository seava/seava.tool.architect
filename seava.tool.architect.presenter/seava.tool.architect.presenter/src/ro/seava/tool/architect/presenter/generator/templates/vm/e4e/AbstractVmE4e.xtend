package ro.seava.tool.architect.presenter.generator.templates.vm.e4e

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*

import ro.seava.tool.architect.presenter.presenter.VmParam
import ro.seava.tool.architect.presenter.presenter.IVmField
import ro.seava.tool.architect.presenter.presenter.InitValExpression
import ro.seava.tool.architect.presenter.presenter.IVmMember
import ro.seava.tool.architect.base.base.DataType
import ro.seava.tool.architect.presenter.presenter.StaticValue
import ro.seava.tool.architect.presenter.presenter.BuiltInVarRef
import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate

abstract class AbstractVmE4e extends AbstractPresenterTemplate {

  /**
   * 
   */
  def protected modelDataFormat(VmParam p) {
    '''«IF p.dataDomain.dataType.isDate», dateFormat:Main.MODEL_DATE_FORMAT«ENDIF»'''
  }

  /**
   * 
   */
  def protected modelDataFormat(IVmField f) {
    '''«IF f.dataType.isDate», dateFormat:Main.MODEL_DATE_FORMAT«ENDIF»'''
  }

  /**
   * 
   */
  def protected _init_val_tpl(InitValExpression iv, IVmMember m) {
    var DataType dt = m.dataType;
    if (iv instanceof StaticValue) {
      if (dt.string) {
        return '''"«(iv as StaticValue).value»"''';
      } else {
        return '''«(iv as StaticValue).value»''';
      }
    } else {
      '''«(iv as BuiltInVarRef).value.extjsExpression»'''
    }
  }

}
