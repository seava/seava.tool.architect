package ro.seava.tool.architect.presenter.extensions

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.presenter.presenter.P_Package
import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.project.project.Module
import ro.seava.tool.architect.project.project.Unit
import ro.seava.tool.architect.presenter.presenter.VmParam
import ro.seava.tool.architect.presenter.presenter.VmMemberValuesUsage

class Vm_Cfg {

  /* ====================== VM OWNERS ====================== */
  /**
   * Get containing package
   */
  def static P_Package getPackage(Vm vm) {
    return vm.eContainer as P_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(Vm vm) {
    return vm.package.module;
  }

  /**
   * Get containing unit
   */
  def static Unit getUnit(Vm vm) {
    return vm.package.unit;
  }

  /* ====================== SIMPLE NAMES ====================== */
  // java
  def static String simpleNameModel(Vm vm) {
    return vm.name + "_Ds";
  }

  def static String simpleNameFilter(Vm vm) {
    return vm.simpleNameModel + "Filter";
  }

  def static String simpleNameParam(Vm vm) {
    return vm.simpleNameModel + "Param";
  }

  //  def static String simpleNameService(Vm vm) {
  //    return vm.simpleNameModel + "Service";
  //  }
  def static String simpleNameGeneratedService(Vm vm) {
    return vm.simpleNameModel + "Service";
  }

  def static String simpleNameCustomService(Vm vm) {
    return vm.simpleNameModel + "Service";
  }

  def static String simpleNameGeneratedQueryBuilder(Vm vm) {
    return vm.simpleNameModel + "Qb";
  }

  def static String simpleNameCustomQueryBuilder(Vm vm) {
    return vm.simpleNameModel + "Qb";
  }

  def static String simpleNameConverter(Vm vm) {
    return vm.simpleNameModel + "Cnv";
  }

  // extjs
  def static String simpleNameModelUiExtjs(Vm vm) {
    return vm.getName() + "_Ds";
  }

  def static String simpleNameFilterUiExtjs(Vm vm) {
    return vm.simpleNameModel + "Filter";
  }

  def static String simpleNameParamUiExtjs(Vm vm) {
    return vm.simpleNameModel + "Param";
  }

  /* ====================== ROOT PACKAGES ====================== */
  def static String getRootPackagePresenter(Vm vm) {
    vm.module.config.presenter.rootPackage + ".presenter"
  }

  def static String getRootPackageUiExtjs(Vm vm) {
    vm.module.config.ui.rootPackage + ".ui.extjs"
  }

  def static String getRootPackageTrl(Vm vm) {
    vm.module.config.trl.rootPackage + ".i18n.extjs"
  }

  /* ====================== PACKAGES ====================== */
  def static String getPackageGeneratedModel(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".generated." + unit.name + ".model";
    } else {
      vm.rootPackagePresenter + ".generated.model";
    }
  }

  def static String getPackageGeneratedFilter(Vm vm) {
    return vm.packageGeneratedModel;
  }

  def static String getPackageGeneratedParam(Vm vm) {
    return vm.packageGeneratedModel;
  }

  def static String getPackageCustomModel(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".extensions." + unit.name + ".model";
    } else {
      vm.rootPackagePresenter + ".extensions.model";
    }
  }

  def static String getPackageCustomFilter(Vm vm) {
    return vm.packageCustomModel;
  }

  def static String getPackageCustomParam(Vm vm) {
    return vm.packageCustomModel;
  }

  def static String getPackageGeneratedModelUiExtjs(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackageUiExtjs + ".generated." + unit.name + ".ds";
    } else {
      vm.rootPackageUiExtjs + ".generated.ds";
    }
  }

  def static String getPackageGeneratedFilterUiExtjs(Vm vm) {
    return vm.packageGeneratedModelUiExtjs;
  }

  def static String getPackageGeneratedParamUiExtjs(Vm vm) {
    return vm.packageGeneratedModelUiExtjs;
  }

  def static String getPackageCustomModelUiExtjs(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackageUiExtjs + ".extensions." + unit.name + ".ds";
    } else {
      vm.rootPackageUiExtjs + ".extensions.ds";
    }
  }

  def static String getPackageCustomFilterUiExtjs(Vm vm) {
    return vm.packageCustomModelUiExtjs;
  }

  def static String getPackageCustomParamUiExtjs(Vm vm) {
    return vm.packageCustomModelUiExtjs;
  }

  def static String getPackageGeneratedModelTrlExtjs(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackageTrl + "." + vm.module.config.refLanguage + ".generated." + unit.name + ".ds";
    } else {
      vm.rootPackageTrl + "." + vm.module.config.refLanguage + ".generated.ds";
    }
  }

  def static String getPackageGeneratedService(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".generated." + unit.name + ".service";
    } else {
      vm.rootPackagePresenter + ".generated.service"
    }
  }

  def static String getPackageCustomService(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".extensions." + unit.name + ".service";
    } else {
      vm.rootPackagePresenter + ".extensions.service";
    }
  }

  def static String getPackageGeneratedQueryBuilder(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".generated." + unit.name + ".qb";
    } else {
      vm.rootPackagePresenter + ".generated.qb"
    }
  }

  def static String getPackageCustomQueryBuilder(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".extensions." + unit.name + ".qb";
    } else {
      vm.rootPackagePresenter + ".extensions.qb";
    }
  }

  def static String getPackageGeneratedConverter(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".generated." + unit.name + ".cnv";
    } else {
      vm.rootPackagePresenter + ".generated.cnv"
    }
  }

  def static String getPackageCustomConverter(Vm vm) {
    var unit = vm.unit;
    if (unit != null) {
      vm.rootPackagePresenter + ".extensions." + unit.name + ".cnv";
    } else {
      vm.rootPackagePresenter + ".extensions.cnv"
    }
  }

  /* ====================== CANONICAL NAMES ====================== */
  def static String alias(Vm vm) {
    return vm.module.config.aliasPrefixPresenter + vm.name + "_Ds"
  }

  def static String canonicalNameGeneratedModel(Vm vm) {
    return vm.packageGeneratedModel + '.' + vm.simpleNameModel;
  }

  def static String canonicalNameCustomModel(Vm vm) {
    return vm.packageCustomModel + '.' + vm.simpleNameModel;
  }

  def static String canonicalNameModel(Vm vm) {
    if (vm.hasCustomModel) {
      return vm.canonicalNameCustomModel
    } else {
      return vm.canonicalNameGeneratedModel
    }
  }

  def static String canonicalNameGeneratedModelUiExtjs(Vm vm) {
    return vm.packageGeneratedModelUiExtjs + '.' + vm.simpleNameModelUiExtjs;
  }

  def static String canonicalNameCustomModelUiExtjs(Vm vm) {
    return vm.packageCustomModelUiExtjs + '.' + vm.simpleNameModelUiExtjs;
  }

  def static String canonicalNameModelUiExtjs(Vm vm) {
    if (vm.hasCustomModel) {
      return vm.canonicalNameCustomModelUiExtjs
    } else {
      return vm.canonicalNameGeneratedModelUiExtjs
    }
  }

  def static String canonicalNameGeneratedModelTrlExtjs(Vm vm) {
    return vm.packageGeneratedModelTrlExtjs + '.' + vm.simpleNameModelUiExtjs;
  }

  def static String canonicalNameGeneratedFilter(Vm vm) {
    return vm.packageGeneratedFilter + '.' + vm.simpleNameFilter;
  }

  def static String canonicalNameCustomFilter(Vm vm) {
    return vm.packageCustomFilter + '.' + vm.simpleNameFilter;
  }

  def static String canonicalNameFilter(Vm vm) {
    if (vm.hasCustomFilter) {
      return vm.canonicalNameCustomFilter
    } else {
      return vm.canonicalNameGeneratedFilter
    }
  }

  def static String canonicalNameGeneratedFilterUiExtjs(Vm vm) {
    return vm.packageGeneratedFilterUiExtjs + '.' + vm.simpleNameFilterUiExtjs;
  }

  def static String canonicalNameCustomFilterUiExtjs(Vm vm) {
    return vm.packageCustomFilterUiExtjs + '.' + vm.simpleNameFilterUiExtjs;
  }

  def static String canonicalNameFilterUiExtjs(Vm vm) {
    if (vm.hasCustomFilter) {
      return vm.canonicalNameCustomFilterUiExtjs
    } else {
      return vm.canonicalNameGeneratedFilterUiExtjs
    }
  }

  def static String canonicalNameGeneratedFilterTrlExtjs(Vm vm) {
    return vm.packageGeneratedModelTrlExtjs + '.' + vm.simpleNameFilterUiExtjs;
  }

  def static String canonicalNameGeneratedParam(Vm vm) {
    return vm.packageGeneratedParam + '.' + vm.simpleNameParam;
  }

  def static String canonicalNameCustomParam(Vm vm) {
    return vm.packageCustomParam + '.' + vm.simpleNameParam;
  }

  def static String canonicalNameParam(Vm vm) {
    if (vm.hasCustomParam) {
      return vm.canonicalNameCustomParam
    } else {
      return vm.canonicalNameGeneratedParam
    }
  }

  def static String canonicalNameGeneratedParamUiExtjs(Vm vm) {
    return vm.packageGeneratedParamUiExtjs + '.' + vm.simpleNameParamUiExtjs;
  }

  def static String canonicalNameCustomParamUiExtjs(Vm vm) {
    return vm.packageCustomParamUiExtjs + '.' + vm.simpleNameParamUiExtjs;
  }

  def static String canonicalNameParamUiExtjs(Vm vm) {
    if (vm.hasCustomParam) {
      return vm.canonicalNameCustomParamUiExtjs
    } else {
      return vm.canonicalNameGeneratedParamUiExtjs
    }
  }

  def static String canonicalNameGeneratedParamTrlExtjs(Vm vm) {
    return vm.packageGeneratedModelTrlExtjs + '.' + vm.simpleNameParamUiExtjs;
  }

  def static String canonicalNameGeneratedService(Vm vm) {
    return vm.packageGeneratedService + '.' + vm.simpleNameGeneratedService;
  }

  def static String canonicalNameCustomService(Vm vm) {
    return vm.packageCustomService + '.' + vm.simpleNameCustomService;
  }

  def static String canonicalNameService(Vm vm) {
    if (vm.hasCustomService) {
      return vm.canonicalNameCustomService
    } else {
      return vm.canonicalNameGeneratedService
    }
  }

  def static String canonicalNameGeneratedQueryBuilder(Vm vm) {
    return vm.packageGeneratedQueryBuilder + '.' + vm.simpleNameGeneratedQueryBuilder;
  }

  def static String canonicalNameCustomQueryBuilder(Vm vm) {
    return vm.packageCustomQueryBuilder + '.' + vm.simpleNameCustomQueryBuilder;
  }

  def static String canonicalNameQueryBuilder(Vm vm) {
    if (vm.hasCustomQueryBuilder) {
      return vm.canonicalNameCustomQueryBuilder
    } else {
      return vm.canonicalNameGeneratedQueryBuilder
    }
  }

  def static String canonicalNameGeneratedConverter(Vm vm) {
    return vm.packageGeneratedConverter + '.' + vm.simpleNameConverter;
  }

  def static String canonicalNameCustomConverter(Vm vm) {
    return vm.packageCustomConverter + '.' + vm.simpleNameConverter;
  }

  def static String canonicalNameConverter(Vm vm) {
    if (vm.hasCustomConverter) {
      return vm.canonicalNameCustomConverter
    } else {
      return vm.canonicalNameGeneratedConverter
    }
  }

  /* ====================== GENERATED CONTENT ====================== */
  def static boolean hasGeneratedModel(Vm vm) {
    return true
  }

  def static boolean hasCustomModel(Vm vm) {
    return false
  }

  def static boolean hasModel(Vm vm) {
    return vm.hasGeneratedModel || vm.hasCustomModel
  }

  def static boolean hasGeneratedFilter(Vm vm) {
    return vm.members.exists(e|e.config.filterRange == true) || //|| e.config.getterFn != null
    (vm.defaultValues != null &&
      vm.defaultValues.memberValues.exists(e|e.usage == VmMemberValuesUsage::INIT_FILTER_CLIENT) );
  }

  def static boolean hasCustomFilter(Vm vm) {
    return false
  }

  def static boolean hasFilter(Vm vm) {
    return vm.hasGeneratedFilter || vm.hasCustomFilter
  }

  def static boolean hasGeneratedParam(Vm vm) {
    return (vm.members.findFirst[t|t instanceof VmParam] != null )
  }

  def static boolean hasCustomParam(Vm vm) {
    return false
  }

  def static boolean hasParam(Vm vm) {
    return vm.hasGeneratedParam || vm.hasCustomParam
  }

  def static boolean hasGeneratedService(Vm vm) {
    return vm.withCustomService
  }

  def static boolean hasCustomService(Vm vm) {
    return vm.withCustomService
  }

  def static boolean hasService(Vm vm) {
    return vm.hasGeneratedService || vm.hasCustomService
  }

  def static boolean hasGeneratedQueryBuilder(Vm vm) {
    return (      
          vm.members.exists(e|e.config.filterBy != null) || ( vm.defaultValues != null &&
      vm.defaultValues.memberValues.exists(t|t.usage == VmMemberValuesUsage::SET_FILTER_SERVER) )
      );
  }

  def static boolean hasCustomQueryBuilder(Vm vm) {
    return vm.withCustomQuery
  }

  def static boolean hasQueryBuilder(Vm vm) {
    return vm.hasGeneratedQueryBuilder || vm.hasCustomQueryBuilder
  }

  def static boolean hasGeneratedConverter(Vm vm) {
    return false
  }

  def static boolean hasCustomConverter(Vm vm) {
    return false
  }

  def static boolean hasConverter(Vm vm) {
    return vm.hasGeneratedConverter || vm.hasCustomConverter
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  def static String fileLocationGeneratedModel(Vm vm) {
    return vm.module.config.presenter.path + "/src/main/java/" + vm.canonicalNameModel.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedFilter(Vm vm) {
    return vm.module.config.presenter.path + "/src/main/java/" + vm.canonicalNameFilter.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedParam(Vm vm) {
    return vm.module.config.presenter.path + "/src/main/java/" + vm.canonicalNameParam.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedModelUiExtjs(Vm vm) {
    return vm.module.config.ui.path + "/src/main/resources/public/" + vm.canonicalNameModelUiExtjs.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedFilterUiExtjs(Vm vm) {
    return vm.module.config.ui.path + "/src/main/resources/public/" + vm.canonicalNameFilterUiExtjs.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedParamUiExtjs(Vm vm) {
    return vm.module.config.ui.path + "/src/main/resources/public/" + vm.canonicalNameParamUiExtjs.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedModelTrlExtjs(Vm vm) {
    return vm.module.config.trl.path + "/src/main/resources/public/" +
      vm.canonicalNameGeneratedModelTrlExtjs.pathFromFQN + ".js"
  }

  def static String fileLocationGeneratedFilterTrlExtjs(Vm vm) {
    return vm.module.config.trl.path + "/src/main/resources/public/" +
      vm.canonicalNameGeneratedFilterTrlExtjs.pathFromFQN + ".js"
  }

  def static String fileLocationGeneratedParamTrlExtjs(Vm vm) {
    return vm.module.config.trl.path + "/src/main/resources/public/" +
      vm.canonicalNameGeneratedParamTrlExtjs.pathFromFQN + ".js"
  }

  def static String fileLocationGeneratedService(Vm vm) {
    return vm.module.config.presenter.path + "/src/main/java/" + vm.canonicalNameGeneratedService.pathFromFQN +
      ".java"
  }

  def static String fileLocationCustomService(Vm vm) {
    return vm.module.config.presenter.path + "/src/main/java/" + vm.canonicalNameCustomService.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedQueryBuilder(Vm vm) {
    return vm.module.config.presenter.path + "/src/main/java/" + vm.canonicalNameGeneratedQueryBuilder.pathFromFQN +
      ".java"
  }

  def static String fileLocationCustomQueryBuilder(Vm vm) {
    return vm.module.config.presenter.path + "/src/main/java/" + vm.canonicalNameCustomQueryBuilder.pathFromFQN +
      ".java"
  }

}
