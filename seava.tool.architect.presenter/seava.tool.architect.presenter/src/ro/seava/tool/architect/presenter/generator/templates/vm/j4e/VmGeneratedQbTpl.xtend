package ro.seava.tool.architect.presenter.generator.templates.vm.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.presenter.VmMemberValuesUsage
import ro.seava.tool.architect.presenter.presenter.InitValExpression
import ro.seava.tool.architect.base.base.DataType
import ro.seava.tool.architect.presenter.presenter.StaticValue
import ro.seava.tool.architect.presenter.presenter.BuiltInVarRef
import ro.seava.tool.architect.presenter.presenter.VmParam

class VmGeneratedQbTpl extends AbstractPresenterTemplate {

  /**
   * 
   */
  def doJavaClass(Vm vm) {
    var CharSequence body = vm.doBody;
    '''
      «copyright»
      package «vm.packageGeneratedQueryBuilder»;
      
      «imports»
      «body»
    '''
  }

  /**
   * 
   */
  def private doBody(Vm vm) {
    '''
      import seava.lib.j4e.api.base.session.Session;
      
      public class «vm.simpleNameGeneratedQueryBuilder» extends «"seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql".
        simpleNameFromFQN»<«vm.canonicalNameModel.simpleNameFromFQN»,«IF vm.hasFilter»«vm.canonicalNameFilter.
        simpleNameFromFQN»«ELSE»«vm.canonicalNameModel.simpleNameFromFQN»«ENDIF», «IF vm.hasParam»«vm.canonicalNameParam.
        simpleNameFromFQN»«ELSE»Object«ENDIF»> {
        «vm.setFilterTpl»
        «vm.beforeBuildWhereTpl»
      }
    '''
  }

  /**
   * 
   */
  def private setFilterTpl(Vm vm) {
    if (vm.defaultValues != null) {
      var initData = vm.defaultValues.memberValues.findFirst(t|t.usage == VmMemberValuesUsage::SET_FILTER_SERVER);
      if (initData != null) {
        '''
          
          @Override
          public void setFilter(«IF vm.hasFilter»«vm.canonicalNameFilter.simpleNameFromFQN»«ELSE»«vm.canonicalNameModel.
            simpleNameFromFQN»«ENDIF» filter) {
            «FOR f : initData.values»
              «IF f.ifNull»
                if (filter.get«f.member.name.toFirstUpper()»() == null«IF f.member.dataType.string» || filter.get«f.
            member.name.toFirstUpper()»().equals("")«ENDIF») {
                  filter.set«f.member.name.toFirstUpper()»(«f.value._init_val_tpl(f.member.dataType)»);
                }
              «ELSE»
                filter.set«f.member.name.toFirstUpper()»(«f.value._init_val_tpl(f.member.dataType)»);
              «ENDIF»
            «ENDFOR»
            super.setFilter(filter);
          }
        '''
      }
    }
  }

  /**
   * 
   */
  def private _init_val_tpl(InitValExpression iv, DataType dt) {
    if (iv instanceof StaticValue) {
      if (dt.string) {
        return '''"«(iv as StaticValue).value»"''';
      } else {
        return '''(iv as StaticValue).value»''';
      }
    } else {
      '''«(iv as BuiltInVarRef).value.javaExpression»'''
    }
  }

  /**
   * 
   */
  def private beforeBuildWhereTpl(Vm vm) {
    '''
      «IF vm.members.filter(typeof(VmParam)).exists(e|e.config.filterBy != null)»
        
        @Override
        public void beforeBuildWhere() {
          «FOR p : vm.members.filter(typeof(VmParam)).filter(e|e.config.filterBy != null)»
            «IF p.config.filterConditionIf != null»
              if («p.config.filterConditionIf») {
                addFilterCondition(" «p.config.filterBy»");
              }
            «ELSE»
              if (this.params != null && this.params.get«p.name.toFirstUpper()»() != null && !"".equals(this.params.get«p.
        name.toFirstUpper()»())) {
                addFilterCondition(" «p.config.filterBy»"); 
                addCustomFilterItem("«p.name»",this.params.get«p.name.toFirstUpper()»());
                «IF p.config.filterRuleParams.length > 0»
                  «FOR frp : p.config.filterRuleParams»
                    addCustomFilterItem("«frp.name»",this.params.get«frp.name.toFirstUpper()»());
                  «ENDFOR»
                «ENDIF»
              }
            «ENDIF»
          «ENDFOR»    
        }
      «ENDIF»
    '''
  }

}
