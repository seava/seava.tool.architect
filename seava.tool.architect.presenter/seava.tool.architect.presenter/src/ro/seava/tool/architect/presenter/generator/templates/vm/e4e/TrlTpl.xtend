package ro.seava.tool.architect.presenter.generator.templates.vm.e4e

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.business.scoping.BusinessScopeUtils.*
import static extension ro.seava.tool.architect.presenter.scoping.PresenterScopeUtils.*
import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*

import ro.seava.tool.architect.presenter.presenter.Vm
import java.util.HashMap
import java.util.Map
import ro.seava.tool.architect.presenter.presenter.VmParam
import ro.seava.tool.architect.presenter.presenter.VmFromBm
import ro.seava.tool.architect.presenter.presenter.VmAttrField
import ro.seava.tool.architect.presenter.presenter.VmRefField
import ro.seava.tool.architect.presenter.presenter.VmFromAlias
import ro.seava.tool.architect.business.business.BmRefField
import ro.seava.tool.architect.business.business.BmAttrField

class TrlTpl {

  /**
   * Create translation file for parameters
   */
  def doParam(Vm vm) {

    var Map<String, String> map = new HashMap<String, String>();

    // collect elements 
    for (p : vm.collect_members_as_type(VmParam)) {
      var String k = p.name + "__lbl"
      var String v = p.config.title;
      if (v == null) {
        v = p.name.toTitleCase();
      }
      map.put(k, v)
    }

    '''
      Ext.define("«vm.canonicalNameGeneratedParamTrlExtjs»", {
        «FOR key : map.keySet.sort SEPARATOR ','»
          «key»: "«map.get(key)»"
        «ENDFOR»
      });
    '''
  }

  def doModel(Vm vm) {

    var Map<String, String> map = new HashMap<String, String>();

    // attribute fields
    for (f : vm.collect_members_as_type(VmAttrField)) {
      var String k = f.name + "__lbl"
      var String v = f.config.title;
      if (v == null) {
        v = f.name.toTitleCase();
      }
      map.put(k, v)
    }

    // reference fields
    for (f : vm.collect_members_as_type(VmRefField)) {

      var String k = f.name + "__lbl"
      var String v = f.config.title;

      if (v == null) {

        if (f.vmFromItem instanceof VmFromBm) {
          if (f.sourceField.config.title != null) {
            v = f.sourceField.config.title
          } else {
            v = f.sourceField.name.toTitleCase
          }
        } else if (f.vmFromItem instanceof VmFromAlias) {

          var VmFromAlias vmFromAlias = f.vmFromItem as VmFromAlias;
          var BmRefField rf = vmFromAlias.sourceRefField;

          //var EntityFieldRef rf = f.rootRefField;
          var srcFieldName = f.sourceField.name;

          if (srcFieldName.matches("id") || srcFieldName.matches("code") || srcFieldName.matches("name") ||
            srcFieldName.matches("refid")) {
            v = rf.config.title;
            if (v == null) {
              v = rf.name.toTitleCase
            }
            if (srcFieldName.matches("id")) {
              v = v + '(ID)'
            } else if (srcFieldName.matches("name")) {
              if (rf.bm.collect_members_as_type(BmAttrField).exists[t|t.name.matches("code")]) {
                v = v + '(Name)'
              }
            } else if (srcFieldName.matches("refid")) {
              v = v + '(Ref-ID)'
            }
          } else {
            v = f.sourceField.config.title;
            if (v == null) {
              v = srcFieldName.toTitleCase
            }
          }
        }
        map.put(k, v)
      }
    }

    '''
      Ext.define("«vm.canonicalNameGeneratedModelTrlExtjs»", {
        «FOR key : map.keySet.sort SEPARATOR ','»
          «key»: "«map.get(key)»"
        «ENDFOR»
      });
    '''
  }
}
