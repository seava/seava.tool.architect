package ro.seava.tool.architect.presenter.generator.templates.asgn.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*
import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*
import static extension ro.seava.tool.architect.business.extensions.BmMember_Ext.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Asgn
import ro.seava.tool.architect.presenter.presenter.AsgnField

class AsgnGeneratedModelJ4eTpl extends AbstractPresenterTemplate {

  def doJavaClass(Asgn asgn) {
    var CharSequence body = asgn.body_tpl;
    '''
      «copyright»
      package «asgn.packageGeneratedModel»;
      
      «imports»
      
      «body»
    '''
  }

  def protected body_tpl(Asgn asgn) {
    ''' 
      «asgn.docs»
      «asgn.annotations_tpl»
      public class «asgn.simpleNameModel»«asgn.extends_tpl»«asgn.implements_tpl» {
        
        public static final String ALIAS = "«asgn.alias»";
        
        «asgn.field_names_tpl»
        «asgn.fields_tpl»
        
        public «asgn.simpleNameModel»() {
        }
        
        public «asgn.simpleNameModel»(«asgn.sourceBm.simpleNameModel» e) {
          super();
          «asgn.constructor_fields_tpl»
        }
        «asgn.accessors_tpl»
      }
    '''
  }

  /**
   * 
   */
  def protected extends_tpl(Asgn asgn) {
    return '''  extends «"seava.lib.j4e.presenter.model.AbstractAsgnModel".simpleNameFromFQN»<«asgn.sourceBm.
      simpleNameModel»>'''
  }

  /**
   * 
   */
  def protected implements_tpl(Asgn asgn) {
    return ''''''
  }

  /**
   * 
   */
  def protected annotations_tpl(Asgn asgn) {
    return '''@«"seava.lib.j4e.api.presenter.annotation.Ds".simpleNameFromFQN»(entity=«asgn.sourceBm.canonicalNameModel.
      simpleNameFromFQN».class«asgn.filter_tpl»«asgn.sort_tpl»)'''
  }

  /**
   * Static field names
   */
  def protected field_names_tpl(Asgn asgn) {
    '''
      
      «FOR f : asgn.members.filter(typeof(AsgnField))»
        public static final String f_«f.name» = "«f.name»";
      «ENDFOR»
    '''
  }

  /**
   * Fields template
   */
  def protected fields_tpl(Asgn asgn) {
    '''
      «FOR f : asgn.members.filter(typeof(AsgnField))»
        
        @«simpleNameFromFQN("seava.lib.j4e.api.presenter.annotation.DsField")»(path="«f.source.name»"«IF f.source.config.transient»,fetch=false«ENDIF»)
        private «f.source.dataTypeFqn.simpleNameFromFQN» «f.name»;
      «ENDFOR»
    '''
  }

  /**
   * Getters setters 
   */
  def protected accessors_tpl(Asgn asgn) {
    '''
      «FOR f : asgn.members.filter(typeof(AsgnField))»
        «var type = f.source.dataTypeFqn.simpleNameFromFQN»
        «var name = f.name»
        «var nameToFirstUpper = f.name.toFirstUpper»
        
        public «type» get«nameToFirstUpper»() {
          return this.«name»;
        }
        
        public void set«nameToFirstUpper»(«type» «name») {
          this.«name» = «name»;
        }
      «ENDFOR»
    '''
  }

  /**
   * Constructor fields
   */
  def protected constructor_fields_tpl(Asgn asgn) {
    '''
      «FOR f : asgn.members.filter(typeof(AsgnField))»
        this.«f.name» = e.get«f.source.name.toFirstUpper()»();
      «ENDFOR»
    '''
  }

  /**
   * Default order by template
   */
  def protected sort_tpl(Asgn asgn) {
    '''«IF asgn.orderBy != null», sort={«FOR token : asgn.orderBy.fields SEPARATOR ','»@«simpleNameFromFQN(
      "seava.lib.j4e.api.presenter.annotation.SortField")»(field=«asgn.simpleNameModel()».f_«token.field.name»«IF token.desc»,desc=true«ENDIF»)«ENDFOR»}«ENDIF»'''
  }

  /**
   * Default filter template
   */
  def protected filter_tpl(Asgn asgn) {
    '''«IF asgn.filterByJpql != null», jpqlWhere="«asgn.filterByJpql»"«ENDIF»'''
  }

}
