package ro.seava.tool.architect.presenter.scoping

import java.util.List
import ro.seava.tool.architect.presenter.presenter.Vm
import java.util.ArrayList
import ro.seava.tool.architect.presenter.presenter.IVmMember

class PresenterScopeUtils {

  /**
   * Collect all members of the given type
   */
  def static <T> List<T> collect_members_as_type(Vm vm, Class<T> clazz) {
    return collect_members_as_type(vm, clazz, false);
  }

  /**
     * Collect declared/all members of the given type
     */
  def static <T> List<T> collect_members_as_type(Vm vm, Class<T> clazz, boolean declaredMembersOnly) {
    var List<T> collector = new ArrayList<T>();
    collect_members_as_type(vm, collector, clazz, declaredMembersOnly);
    return collector;
  }

  /**
   * 
   */
  def static private <T> void collect_members_as_type(Vm vm, List<T> collector, Class<T> clazz,
    boolean declaredMembersOnly) {

    for (e : vm.members.filter(typeof(IVmMember)).filter(
      e|clazz.isAssignableFrom(e.eClass.instanceClass)
    )) {
      collector.add(e as T);
    }

    if (!declaredMembersOnly && vm.superType != null) {
      collect_members_as_type(vm.superType, collector, clazz, declaredMembersOnly)
    }
  }

  // ============================
  /**
   * Collect all members of the given type
   */
  def static List<IVmMember> collect_members(Vm vm, Class<?> clazz) {
    return collect_members(vm, clazz, false);
  }

  /**
   * Collect declared/all members of the given type
   */
  def static List<IVmMember> collect_members(Vm vm, Class<?> clazz, boolean declaredMembersOnly) {
    var List<IVmMember> collector = new ArrayList<IVmMember>();
    collect_members(vm, collector, clazz, declaredMembersOnly);
    return collector;
  }

  /**
   * 
   */
  def static private void collect_members(Vm vm, List<IVmMember> collector, Class<?> clazz,
    boolean declaredMembersOnly) {

    collector.addAll(vm.members.filter(typeof(IVmMember)).filter(e|clazz.isAssignableFrom(e.eClass.instanceClass)))
    if (!declaredMembersOnly && vm.superType != null) {
      collect_members(vm.superType, collector, clazz, declaredMembersOnly)
    }
  }

//  
//  // ---------------
//  def static List<IVmMember> collect_members(Vm vm, Class<?> clazz) {
//    var List<IVmMember> collector = new ArrayList<IVmMember>();
//    collect_members(vm, collector, clazz);
//    return collector;
//  }
//
//  def private static void collect_members(Vm vm, List<IVmMember> collector, Class<?> clazz) {
//
//    for (f : vm.members) {
//
//      if (f.eClass.instanceClass.isAssignableFrom(clazz)) {
//        System.out.println("f.eClass.instanceClass.isAssignableFrom(clazz) = true");
//      }
//      if (clazz.isAssignableFrom(f.eClass.instanceClass)) {
//        System.out.println("clazz.isAssignableFrom(f.eClass.instanceClass) = true");
//      }
//      System.out.println("f.class.canonicalName = " + f.class.canonicalName);
//      System.out.println("f.eClass = " + f.eClass.instanceClass);
//    }
//
//    collector.addAll(vm.members.filter(e|clazz.isAssignableFrom(e.eClass.instanceClass)))
//    if (vm.superType != null) {
//      collect_members(vm.superType, collector, clazz)
//    }
//  }
}
