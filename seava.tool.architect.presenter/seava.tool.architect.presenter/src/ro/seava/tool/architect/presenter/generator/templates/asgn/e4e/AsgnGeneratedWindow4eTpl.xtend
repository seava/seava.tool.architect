package ro.seava.tool.architect.presenter.generator.templates.asgn.e4e

import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Asgn
import ro.seava.tool.architect.presenter.presenter.AsgnField

class AsgnGeneratedWindow4eTpl extends AbstractPresenterTemplate {

  def CharSequence doDependencyFile(Asgn asgn) {
    '''
      [
        "«asgn.canonicalNameModelUiExtjs»",
        «IF asgn.hasParam»"«asgn.canonicalNameParamUiExtjs»",«ENDIF»
        "«asgn.canonicalNameListUiExtjs»"
      ]
    '''
  }

  def doJsClass(Asgn asgn) {
    '''
      «copyright»
      Ext.define("«asgn.canonicalNameWindowUiExtjs»", {
        extend: "seava.lib.e4e.js.asgn.AbstractAsgnUi",
        width:«asgn.width»,
        height:«asgn.height»,
        title:"«asgn.title»",
        _filterFields_: [
          «FOR f : asgn.members.filter(typeof(AsgnField)).filter(e|!e.noFilter) SEPARATOR ','»
            ["«f.name»"]
          «ENDFOR»
        ],
        _defaultFilterField_ : "«asgn.defaultFilterField.name»",
      
        _defineElements_: function () {
          this._getBuilder_()
            .addLeftGrid({ xtype:"«asgn.simpleNameModel»List$Left"})
            .addRightGrid({ xtype:"«asgn.simpleNameModel»List$Right"})
        }
      });
    '''
  }

}
