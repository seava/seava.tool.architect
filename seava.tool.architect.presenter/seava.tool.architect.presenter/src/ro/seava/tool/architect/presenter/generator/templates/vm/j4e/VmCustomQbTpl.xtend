package ro.seava.tool.architect.presenter.generator.templates.vm.j4e

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*

import ro.seava.tool.architect.presenter.generator.templates.AbstractPresenterTemplate
import ro.seava.tool.architect.presenter.presenter.Vm

class VmCustomQbTpl extends AbstractPresenterTemplate {

  /**
   * 
   */
  def doJavaClass(Vm vm) {
    var CharSequence body = vm.doBody;
    '''
      «copyright»
      package «vm.packageCustomQueryBuilder»;
      
      «imports»
      «body»
    '''
  }

  /**
   * 
   */
  def private doBody(Vm vm) {
    '''
      import «vm.canonicalNameModel»;
      import seava.lib.j4e.api.session.Session;
      
      public class «vm.simpleNameCustomQueryBuilder» extends «"seava.lib.j4e.presenter.action.query.QueryBuilderWithJpql".
        simpleNameFromFQN»<«vm.simpleNameModel»,«IF vm.hasFilter»«vm.canonicalNameFilter.simpleNameFromFQN»«ELSE»«vm.
        canonicalNameModel.simpleNameFromFQN»«ENDIF», «IF vm.hasParam»«vm.canonicalNameParam.simpleNameFromFQN»«ELSE»Object«ENDIF»> {
      
        // Implement me 
      
      }
    '''
  }

}
