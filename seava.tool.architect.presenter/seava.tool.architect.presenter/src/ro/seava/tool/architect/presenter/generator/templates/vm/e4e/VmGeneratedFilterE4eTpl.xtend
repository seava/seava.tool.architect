package ro.seava.tool.architect.presenter.generator.templates.vm.e4e

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.Vm_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*

import ro.seava.tool.architect.presenter.presenter.Vm
import ro.seava.tool.architect.presenter.presenter.VmMemberValuesUsage
import ro.seava.tool.architect.presenter.presenter.VmMemberValues

class VmGeneratedFilterE4eTpl extends AbstractVmE4e {

  def doExtjsClass(Vm vm) {

    //var Module module = vm.module;
    var ff = vm.rangedFilterFields;

    //var initData = vm.memberValueList.findFirst(t|t.usage == VmMemberValuesUsage::INIT_FILTER_CLIENT);
    var VmMemberValues initData = null;
    if (vm.defaultValues != null) {
      initData = vm.defaultValues.memberValues.findFirst(t|t.usage == VmMemberValuesUsage::INIT_FILTER_CLIENT);
    }

    '''
      «copyright»
      
      Main.createFilterModelFromRecordModel({
        «IF initData != null»                         
          initFilter: function() {
            «FOR f : initData.values»
              this.set("«f.member.name»", «f.value._init_val_tpl(f.member)»);
            «ENDFOR»
          },
        «ENDIF»
        «IF ff.size > 0»
          fields: [
            «FOR f : ff SEPARATOR ","»
              {name:"«f.name»_From",type:"«f.dataType.extjsType»"«f.modelDataFormat»«IF f.dataType.isNumber()», useNull:true«ENDIF»},
              {name:"«f.name»_To",type:"«f.dataType.extjsType»"«f.modelDataFormat»«IF f.dataType.isNumber()», useNull:true«ENDIF»}
            «ENDFOR»
          ],
        «ENDIF»
        recordModelFqn: "«vm.canonicalNameModelUiExtjs»"
      });
    '''
  }
}
