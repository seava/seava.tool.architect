package ro.seava.tool.architect.project.generator.templates.module

import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.project.project.Module

class EclipselinkWeaveTpl {

  /**
   * Generate ant script to weave entities
   */
  def doFile(Module m) {
    if (m.config.domain != null) {
      var artifact = m.config.domain.artifactId;
      '''
        <?xml version="1.0" encoding="UTF-8"?>
        <project name="«artifact».weave" default="run" basedir=".">
          <description>Ant task for static weaving on eclipselink model classes.</description>
          
          <target name="define.task" description="New task definition for eclipseLink static weaving">
            <taskdef name="weave" classname="org.eclipse.persistence.tools.weaving.jpa.StaticWeaveAntTask">
              <classpath>
                <path path="${compile_classpath}" />
              </classpath>
            </taskdef>
          </target>
          
          <target name="run" description="Perform weaving" depends="define.task">
            <echo>Performing static weaving on model classes</echo>
            <echo>Source path: ${path_source} </echo>
            <weave source="${path_source}" target="${path_target}" persistenceinfo="${path_persistenceinfo}" loglevel="FINE">
              <classpath>
                <path path="${compile_classpath}" />
              </classpath>
            </weave>
          </target>
          
        </project>
      '''
    }
  }
}
