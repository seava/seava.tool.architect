package ro.seava.tool.architect.project.extensions;

/**
 * A dependency definition class to collect dependencies declared directly or
 * through a list of hierarchical dependency templates.
 * 
 * @author amathe
 * 
 */
public class PomDependencyDefinition {

	String artifactId;
	String groupId;
	String version;
	String type;
	String scope;

	String moduleFqn;

	public String getArtifactId() {
		return artifactId;
	}

	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getModuleFqn() {
		return moduleFqn;
	}

	public void setModuleFqn(String moduleFqn) {
		this.moduleFqn = moduleFqn;
	}

}
