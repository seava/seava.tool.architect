package ro.seava.tool.architect.project.generator.templates.module

import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.project.project.Module

class PackDbTpl {

  /**
   * Generate ant script to pack database resources 
   */
  def doFile(Module m) {
    if (m.config.db != null) {
      var artifact = m.config.db.artifactId;
      '''
        <?xml version="1.0" encoding="UTF-8"?>
        <project name="«artifact».pack" default="run" basedir=".">
          <description>Pack database scripts for «artifact»</description>
        
          <property name="sourcePath" value="${basedir}/src/main/resources" />
          <property name="targetPath" value="${basedir}/target/classes" />
        
          <target name="run" depends="pack">
            <echo>Done.</echo>
          </target>
        
          <target name="pack">
            <concat destfile="${targetPath}/«artifact.replaceAll("\\.", "-")»-mysql.sql">
              <fileset dir="${sourcePath}" includes="**/mysql/install/table/*" />
              <fileset dir="${sourcePath}" includes="**/mysql/install/fk/*" />
            </concat>
            <echo message="Pack done." />
          </target>
        </project>
      '''
    }

  }
}
