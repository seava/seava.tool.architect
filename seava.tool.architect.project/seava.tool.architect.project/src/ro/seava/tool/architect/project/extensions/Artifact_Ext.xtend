package ro.seava.tool.architect.project.extensions

import ro.seava.tool.architect.project.project.Artifact
import ro.seava.tool.architect.project.project.Group
import ro.seava.tool.architect.project.project.Version
import ro.seava.tool.architect.project.project.Library
import ro.seava.tool.architect.project.project.Product
import ro.seava.tool.architect.project.project.MvnDependencyElement
import java.util.Set
import java.util.List
import java.util.ArrayList
import ro.seava.tool.architect.project.project.ArtifactMvn
import java.util.HashSet
import ro.seava.tool.architect.project.project.MvnDependencyTemplate
import ro.seava.tool.architect.project.project.Module

class Artifact_Ext {

  def static final String rootPackage(Artifact a) {
    return a.artifactId;
  }

  /**
   * Resolve the prefix for artifactId for the given Product
   */
  def static final String resolveArtifactPrefix(Product p) {
    if (p.group.artifactIdPrefix != null) {
      return p.group.artifactIdPrefix;
    }
    return "";
  }

  /**
   * Resolve artifactId for the given Product
   */
  def static final String getArtifactId(Product p) {
    return p.resolveArtifactPrefix + p.name.replaceAll("_", ".");
  }

  /**
   * 
   */
  def static final Version resolveVersion(Artifact a) throws Exception {
    if (a.module != null) {
      return a.module.version;
    } else {
      if (a.parent != null) {
        return a.parent.resolveVersion
      }
    }
    throw new Exception("Cannot resolve version for artifact `" + a.name + "`");
  }

  /**
   * 
   */
  def static final String versionValue(Version v) throws Exception {
    if (v.isSnapshot) {
      return v.value + "-SNAPSHOT";
    } else {
      return v.value;
    }
  }

  /**
   * 
   */
  def static final Group resolveGroup(Artifact a) throws Exception {
    if (a.module != null) {
      return a.module.group;
    } else {
      if (a.parent != null) {
        return a.parent.resolveGroup
      }
    }
    throw new Exception("Cannot resolve group for artifact `" + a.name + "`");
  }

  /**
   * 
   */
  def static final String getGroupId(Artifact a) throws Exception {
    return a.resolveGroup.groupId;
  }

  /**
   * 
   */
  def static final Module resolveModule(Artifact a) throws Exception {
    if (a.module != null) {
      return a.module;
    } else {
      if (a.parent != null) {
        return a.parent.resolveModule
      }
    }
    throw new Exception("Cannot resolve module for artifact `" + a.name + "`");
  }

  /**
   * Resolve the prefix for artifactId for the given Artifact
   */
  def static final String resolveArtifactPrefix(Artifact a) {
    if (a.module != null && a.module.group.artifactIdPrefix != null) {
      return a.module.group.artifactIdPrefix;
    } else {
      if (a.parent != null) {
        return a.parent.resolveArtifactPrefix
      }
    }
    return "";
  }

  /**
   * Resolve artifactId for the given Artifact
   */
  def static final String getArtifactId(Artifact a) {
    if (a.artifactName != null) {
      if (a.noPrefix) {
        return a.artifactName
      } else {
        return a.resolveArtifactPrefix + a.artifactName
      }
    } else {
      return a.resolveArtifactPrefix + a.name.replaceAll("_", ".");
    }
  }

  /**
   * Resolve the prefix for artifactId for the given Library
   */
  def static final String resolveArtifactPrefix(Library l) {
    if (l.group.artifactIdPrefix != null) {
      return l.group.artifactIdPrefix
    } else {
      return "";
    }
  }

  /**
   * Resolve artifactId for the given Library
   */
  def static final String getArtifactId(Library l) {
    if (l.artifactName != null) {
      if (l.noPrefix) {
        return l.artifactName
      } else {
        return l.resolveArtifactPrefix + l.artifactName
      }
    } else {
      return l.resolveArtifactPrefix + l.name;
    }
  }

  /**
   * Resolve the location of the artifact
   */
  def static final String getPath(Artifact a) {
    var StringBuffer sb = new StringBuffer();
    var Artifact _a = a;
    while (_a != null && !_a.isAbstract) {

      // include it in the path only for itself, but not if the path is
      // calculated for a child
      if (!_a.isChildrenNotNested()) {
        if (!_a.isRootOfGeneratedSrcProject()) {
          if (sb.length() > 0) {
            sb.insert(0, "/");
          }
          sb.insert(0, _a.artifactId);
        }
      } else {
        if (sb.length() == 0 && !_a.isRootOfGeneratedSrcProject()) {
          sb.insert(0, getArtifactId(_a));
        }
      }
      if (_a.getParent() != null && _a.getModule() == null) {
        _a = _a.getParent();
      } else {
        _a = null;
      }
    }
    return sb.toString();
  }

  /**
   * 
   */
  def static List<Module> collectRequiredModules(Module m) {
    var List<Module> result = new ArrayList<Module>()
    m.collectRequiredModules(result)
    return result
  }

  /**
   * 
   */
  def static void collectRequiredModules(Module m, List<Module> result) {
    var list = m.modules
    if (list != null && list.size > 0) {
      result.addAll(list)
      for (_m : list) {
        _m.collectRequiredModules(result)
      }
    }
  }

  /**
   * 
   */
  def static List<MvnDependencyElement> collectDependencyElementsFromDependenciesManagement(ArtifactMvn ac) {
    var List<MvnDependencyElement> l = new ArrayList<MvnDependencyElement>();

    //collect versions from dependency management templates
    if (ac.dependencyManagementTemplates != null) {
      for (dmt : ac.dependencyManagementTemplates) {
        for (dm : dmt.elements) {
          l.add(dm)
        }
      }
    }

    //collect versions from dependency management
    if (ac.dependencyManagement != null) {
      for (dm : ac.dependencyManagement) {
        l.add(dm)
      }
    }
    return l;
  }

  /**
   * 
   */
  def static Set<MvnDependencyElement> collectDependencyElementsFromDependencies(ArtifactMvn ac) {
    var Set<MvnDependencyElement> collector = new HashSet<MvnDependencyElement>();
    collector.addAll(ac.dependencies);
    if (ac.dependencyTemplates != null) {
      for (MvnDependencyTemplate dmt : ac.dependencyTemplates) {
        for (dm : dmt.elements) {
          collector.add(dm)
        }
      }
    }
    return collector;
  }

  /**
   * 
   */
  def static List<PomDependencyDefinition> collectPomDependenciesOfTypeArtifact(
    Set<MvnDependencyElement> dependencies) {
    var List<PomDependencyDefinition> _deps = new ArrayList<PomDependencyDefinition>();

    for (dpd : dependencies.filter(e|e.dependency instanceof ArtifactMvn)) {
      var a = dpd.dependency as ArtifactMvn
      var _d = new PomDependencyDefinition();
      _d.setArtifactId(a.artifact.artifactId)
      _d.setGroupId(a.artifact.groupId)
      _d.setType(a.artifact.type.name)
      _d.setVersion(a.artifact.resolveVersion.value)
      if (dpd.scope != null) {
        _d.setScope(dpd.scope.name)
      }
      _d.setModuleFqn(a.artifact.resolveVersion.name);
      _deps.add(_d)
    }
    return _deps
  }

  /**
   * 
   */
  def static List<PomDependencyDefinition> collectPomDependenciesOfTypeLibrary(
    Set<MvnDependencyElement> dependencies) {
    var List<PomDependencyDefinition> _deps = new ArrayList<PomDependencyDefinition>();

    for (dpd : dependencies.filter(e|e.dependency instanceof Library)) {
      var lib = dpd.dependency as Library
      var _d = new PomDependencyDefinition();

      _d.setArtifactId(lib.artifactId)
      _d.setGroupId(lib.group.groupId)

      if (dpd.scope != null) {
        _d.setScope(dpd.scope.name)
      }
      _deps.add(_d)
    }
    return _deps
  }

}
