/*
 * generated by Xtext
 */
package ro.seava.tool.architect.project.formatting

import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig
import com.google.inject.Inject;
import ro.seava.tool.architect.project.services.ProjectGrammarAccess

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
class ProjectFormatter extends AbstractDeclarativeFormatter {

  @Inject extension ProjectGrammarAccess

  override protected void configureFormatting(FormattingConfig c) {

    c.setAutoLinewrap(100)

    // import
    c.setLinewrap(1, 1, 2).after(importRule)

    // package
    c.setLinewrap(2).after(PJ_PackageAccess.leftCurlyBracketKeyword_2)
    c.setIndentationIncrement.after(PJ_PackageAccess.leftCurlyBracketKeyword_2)
    c.setIndentationDecrement.before(PJ_PackageAccess.rightCurlyBracketKeyword_5)
    c.setLinewrap(2).before(PJ_PackageAccess.rightCurlyBracketKeyword_5)

    // comments
    c.setLinewrap(0, 1, 2).before(SL_COMMENTRule)
    c.setLinewrap(0, 1, 2).after(SL_COMMENTRule)

    c.setLinewrap(1, 1, 2).before(ML_COMMENTRule)
    c.setLinewrap(1, 1, 2).after(ML_COMMENTRule)

    // elements
    c.setLinewrap(1, 1, 2).before(licenseRule)
    c.setLinewrap(1, 1, 2).before(artifactTypeRule)
    c.setLinewrap(1, 1, 2).before(vendorRule)
    c.setLinewrap(1, 1, 2).before(groupRule)
    c.setLinewrap(1, 1, 2).before(versionRule)
    c.setLinewrap(1, 1, 2).before(libraryRule)

    c.setLinewrap(1, 1, 2).before(mvnScopeRule)
    c.setLinewrap(1, 1, 2).before(mvnProfileRule)
    c.setLinewrap(1, 1, 2).before(mvnPluginRule)
    c.setLinewrap(1, 1, 2).before(mvnDependencyTemplateRule)

    c.setLinewrap(1, 1, 2).before(licenseRule)

    c.format_product
    c.format_module
    c.format_artifact
    c.format_artifact_maven
    c.format_artifact_maven_profile

    c.format_mvn_plugin_usage
    c.format_spring
    c.format_attribute_map
  }

  def private format_product(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(productRule)
    c.setIndentationIncrement.after(productAccess.productKeyword_0)
    c.setIndentationDecrement.before(productAccess.semicolonKeyword_24)
    c.setLinewrap(1).before(productAccess.groupKeyword_4)
    c.setLinewrap(1).before(productAccess.vendorKeyword_8)
    c.setLinewrap(1).before(productAccess.sourceCodeKeyword_17_0)
    c.setLinewrap(1).before(productAccess.librariesKeyword_19_0)
  }

  def private format_module(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(moduleRule)
    c.setIndentationIncrement.after(moduleAccess.moduleKeyword_0)
    c.setIndentationDecrement.before(moduleAccess.semicolonKeyword_20)
    c.setLinewrap(1).before(moduleAccess.vendorKeyword_6)
    c.setLinewrap(1).before(moduleAccess.sourceCodeKeyword_15_0)
    c.setLinewrap(1).before(moduleAccess.configKeyword_16)
    c.setLinewrap(1).before(moduleAccess.requiresModulesKeyword_18_0)

    // module config
    c.setIndentationIncrement.after(moduleCfgAccess.leftCurlyBracketKeyword_1)
    c.setIndentationDecrement.before(moduleCfgAccess.rightCurlyBracketKeyword_16)
    c.setLinewrap(1).before(moduleCfgAccess.rightCurlyBracketKeyword_16)
    c.setLinewrap(1).before(moduleCfgAccess.refLanguageKeyword_2)
    c.setLinewrap(1).before(moduleCfgAccess.dbArtifactKeyword_4_0)
    c.setLinewrap(1).before(moduleCfgAccess.domainArtifactKeyword_5_0)
    c.setLinewrap(1).before(moduleCfgAccess.businessApiArtifactKeyword_6_0)
    c.setLinewrap(1).before(moduleCfgAccess.businessArtifactKeyword_7_0)
    c.setLinewrap(1).before(moduleCfgAccess.presenterArtifactKeyword_8_0)
    c.setLinewrap(1).before(moduleCfgAccess.uiArtifactKeyword_9_0)
    c.setLinewrap(1).before(moduleCfgAccess.i18nArtifactKeyword_10_0)
    c.setLinewrap(1).before(moduleCfgAccess.aliasPrefixBusinessKeyword_12)
    c.setLinewrap(1).before(moduleCfgAccess.aliasPrefixPresenterKeyword_14)
  }

  def private format_artifact(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(artifactRule)
    c.setIndentationIncrement.after(artifactAccess.artifactKeyword_0)
    c.setIndentationDecrement.before(artifactAccess.semicolonKeyword_15)
    c.setLinewrap(1).before(artifactAccess.titleAssignment_9)
    c.setLinewrap(1).before(artifactAccess.descriptionAssignment_10)
  }

  def private format_artifact_maven(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(artifactMvnRule)
    c.setIndentationIncrement.after(artifactMvnAccess.artifactMavenConfigKeyword_0)
    c.setIndentationDecrement.before(artifactMvnAccess.semicolonKeyword_13)

    c.setLinewrap(1).before(artifactMvnAccess.pluginRepositoriesKeyword_3_0)
    c.setLinewrap(1).before(artifactMvnAccess.dependencyRepositoriesKeyword_4_0)
    c.setLinewrap(1).before(artifactMvnAccess.dependencyTemplatesKeyword_7_0)
    c.setLinewrap(1).before(artifactMvnAccess.dependencyManagementKeyword_6_0)
    c.setLinewrap(1).before(artifactMvnAccess.dependencyManagementTemplatesKeyword_5_0)
    c.setLinewrap(1).before(artifactMvnAccess.dependenciesKeyword_8_0)
    c.setLinewrap(1).before(artifactMvnAccess.profilesKeyword_9_0)
    c.setLinewrap(1).before(artifactMvnAccess.pomPropertiesKeyword_11_0)
    c.setLinewrap(1).before(artifactMvnAccess.modulesKeyword_10_0)
    c.setLinewrap(1).before(artifactMvnAccess.distributionManagementKeyword_12_0)

  }

  def private format_artifact_maven_profile(FormattingConfig c) {
    c.setLinewrap(1).before(artifactMvnProfileAccess.profileAssignment_0)
    c.setIndentationIncrement.after(artifactMvnProfileAccess.leftCurlyBracketKeyword_1)
    c.setIndentationDecrement.before(artifactMvnProfileAccess.rightCurlyBracketKeyword_5)
    c.setLinewrap(1).before(artifactMvnProfileAccess.rightCurlyBracketKeyword_5)

    c.setLinewrap(1).before(artifactMvnProfileAccess.pluginManagementKeyword_2_0)
    c.setLinewrap(1).before(artifactMvnProfileAccess.pluginsKeyword_3_0)
    c.setLinewrap(1).before(artifactMvnProfileAccess.buildConfigKeyword_4_0)
  }

  def private format_mvn_plugin_usage(FormattingConfig c) {
    c.setIndentationIncrement.after(mvnPluginUsagesAccess.leftSquareBracketKeyword_0)
    c.setIndentationDecrement.before(mvnPluginUsagesAccess.rightSquareBracketKeyword_3)
    c.setLinewrap(1).before(mvnPluginUsagesAccess.rightSquareBracketKeyword_3)
    c.setLinewrap(1).before(mvnPluginUsageRule)
  }

  def private format_spring(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(springFileTypeRule)
    c.setLinewrap(1, 1, 2).before(springNsRule)
    c.setLinewrap(1, 1, 2).before(springTemplateRule)
    c.setLinewrap(1, 1, 2).before(artifactSpringCfgRule)
    c.setLinewrap(1).before(artifactSpringCfgAccess.beforeIncludesKeyword_6_0)
    c.setLinewrap(1).before(artifactSpringCfgAccess.afterIncludesKeyword_7_0)
    c.setLinewrap(1).before(artifactSpringCfgAccess.namespacesKeyword_4_0)

  }

  def private format_attribute_map(FormattingConfig c) {
    c.setIndentationIncrement.after(attrMapAccess.leftCurlyBracketKeyword_0)
    c.setIndentationDecrement.before(attrMapAccess.rightCurlyBracketKeyword_3)
    c.setLinewrap(1).after(attrMapAccess.leftCurlyBracketKeyword_0)
    c.setLinewrap(1).before(attrMapAccess.rightCurlyBracketKeyword_3)
    c.setLinewrap(1, 1, 2).before(attrMapEntryRule)
  }

}
