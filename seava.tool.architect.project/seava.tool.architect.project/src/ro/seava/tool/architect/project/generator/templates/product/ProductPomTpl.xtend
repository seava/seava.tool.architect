package ro.seava.tool.architect.project.generator.templates.product

import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.project.project.Product
import ro.seava.tool.architect.project.project.Library

class ProductPomTpl {

  def doPomXml(Product p) {

    //        <parent>
    //          <groupId>«p.parent.groupId»</groupId>
    //          <artifactId>«p.parent.artifactId»</artifactId>
    //          <version>«p.parent.versionValue»</version>
    //        </parent>
    '''
      <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
      
        <modelVersion>4.0.0</modelVersion>
      
        <groupId>«p.group.groupId»</groupId>
        <artifactId>«p.artifactId»</artifactId>
        <version>«p.version.versionValue»</version>
        <name>«p.fullName»</name>
        <description>«p.description»</description>
        <packaging>pom</packaging>
        <url>«p.url»</url>
        
        <modules>
        <module>../«p.artifactId + ".config.dev"»</module>
        <module>../«p.artifactId + ".config.prod"»</module>
        <module>../«p.artifactId + ".ear"»</module>
        </modules>
      </project>
    '''
  }

  def doPomXmlConfigDev(Product p) {

    '''
      <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
      
        <modelVersion>4.0.0</modelVersion>
        <parent>
          <relativePath>../«p.artifactId»</relativePath>
          <groupId>«p.group.groupId»</groupId>
          <artifactId>«p.artifactId»</artifactId>
          <version>«p.version.versionValue»</version>
        </parent>
        <artifactId>«p.artifactId + ".config.dev"»</artifactId>
        <name>«p.fullName» - Default configuration for development mode</name>
        <description>«p.description»</description>
        <packaging>jar</packaging>
      </project>
    '''
  }

  def doPomXmlConfigProd(Product p) {

    '''
      <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
      
        <modelVersion>4.0.0</modelVersion>
        <parent>
          <relativePath>../«p.artifactId»</relativePath>
          <groupId>«p.group.groupId»</groupId>
          <artifactId>«p.artifactId»</artifactId>
          <version>«p.version.versionValue»</version>
        </parent>
        <artifactId>«p.artifactId + ".config.prod"»</artifactId>
        <name>«p.fullName» - Default configuration for production mode</name>
        <description>«p.description»</description>
        <packaging>jar</packaging>
      </project>
    '''
  }

  def doPomXmlEar(Product p) {
    '''
      <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
      
        <modelVersion>4.0.0</modelVersion>
        <parent>
          <relativePath>../«p.artifactId»</relativePath>
          <groupId>«p.group.groupId»</groupId>
          <artifactId>«p.artifactId»</artifactId>
          <version>«p.version.versionValue»</version>
        </parent>
        <artifactId>«p.artifactId + ".ear"»</artifactId>
        <name>«p.fullName» - Ear</name>
        <description>«p.description»</description>
        <packaging>ear</packaging>
        
        
        <properties>
          <build_number></build_number>
        </properties>
        
        <dependencies>
          «FOR m : p.modules»
            «FOR a : m.artifacts»
              <dependency>
                <groupId>«a.resolveGroup.groupId»</groupId>
                <artifactId>«a.artifactId»</artifactId>
                <version>«a.resolveVersion.versionValue»</version>
                <type>«a.type.name»</type>
                <exclusions>
                  <exclusion>
                    <groupId>*</groupId>
                    <artifactId>*</artifactId>
                  </exclusion>
                </exclusions>
              </dependency>
            «ENDFOR»
          «ENDFOR»
          «var ac = p.libSource»
            «IF (ac.elements.size > 0)»
              «FOR dpd : ac.elements.filter(e|((e.dependency instanceof Library)   ))»
                «var lib = dpd.dependency as Library»
                <dependency>
                  «lib._groupId_tpl»
                  «lib._artifactId_tpl»
                  «lib._version_tpl»
                  <type>«lib.type.name»</type>
                  <exclusions>
                    <exclusion>
                      <groupId>*</groupId>
                      <artifactId>*</artifactId>
                    </exclusion>
                  </exclusions>
                  «IF dpd.exclusions.size < 0»
                    <exclusions>
                      «FOR exclusion : dpd.exclusions»
                        <exclusion>
                          <groupId>«exclusion.group»</groupId>
                          <artifactId>«exclusion.artifactId»</artifactId>
                        </exclusion>
                      «ENDFOR»
                    </exclusions>
                  «ENDIF»
                </dependency>
              «ENDFOR»
            «ENDIF» 
        </dependencies>
        <build>
          <plugins>
            <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-ear-plugin</artifactId>
              <version>2.3.2</version>
              <configuration>
                <finalName>«p.artifactId»-«p.version.value»-${build_number}</finalName>
                <defaultLibBundleDir>lib</defaultLibBundleDir>
                <modules>
                  «FOR m : p.modules»
                    <!-- Module: «m.module.name»  -->
                    «FOR a : m.artifacts»
                      <jarModule>
                        <groupId>«a.resolveGroup.groupId»</groupId>
                        <artifactId>«a.artifactId»</artifactId>
                        <bundleDir>artifacts</bundleDir>
                      </jarModule>
                    «ENDFOR»
                  «ENDFOR»
                </modules>
              </configuration>
            </plugin>
          </plugins>
        </build>
      </project>
    '''
  }

  // library 
  def private _groupId_tpl(Library l) {
    if (l.group != null) {
      return '''<groupId>«l.group.groupId»</groupId>'''
    }
  }

  def private _artifactId_tpl(Library l) {
    '''<artifactId>«l.artifactId»</artifactId>'''
  }

  def private _version_tpl(Library l) {
    '''<version>«l.version.versionValue»</version>'''
  }
}
