package ro.seava.tool.architect.project.generator.templates.artifact

import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.project.project.ArtifactMvn

class ManifestTpl {

  /**
   * Create a default MANIFEST.MF file
   */
  def doFile(ArtifactMvn am) {
    '''   
      Manifest-Version: 1.0
      Bundle-ManifestVersion: 2
      Bundle-Name: «am.artifact.artifactId»
      Bundle-SymbolicName: «am.artifact.artifactId»
      Bundle-Vendor: «am.artifact.resolveModule.vendor.fullName»
      Bundle-Version: «am.artifact.resolveVersion.versionValue»
    '''
  }
}
