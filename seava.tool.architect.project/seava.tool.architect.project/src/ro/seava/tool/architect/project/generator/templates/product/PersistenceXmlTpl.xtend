package ro.seava.tool.architect.project.generator.templates.product

import ro.seava.tool.architect.project.project.Product

class PersistenceXmlTpl {

  def doFile(Product p) {

    '''
      <persistence xmlns="http://java.sun.com/xml/ns/persistence"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_1_0.xsd"
        version="1.0">
        <persistence-unit name="«p.name»" transaction-type="RESOURCE_LOCAL">
      
          <!-- ==================================================================================================== 
            ==================== Add here the mapping files from all the active modules ========================= 
            ===================================================================================================== -->
          «p.persistenceContext»
          <exclude-unlisted-classes>false</exclude-unlisted-classes>
          <properties>
            <!-- ================================================================================================ 
              ================ To see all the available properties and possible values check: ================ 
              http://www.eclipse.org/eclipselink/documentation/2.4/jpa/extensions/persistenceproperties_ref.htm 
              ================================================================================================= -->
        
          <property name="eclipselink.weaving" value="static"/>
          <property name="eclipselink.session.customizer" value="seava.lib.j4e.domain.sequence.UUIDSequence"/>
          <property name="eclipselink.target-database"
            value="org.eclipse.persistence.platform.database.MySQLPlatform"/>
        
          <!-- DDL generation -->
          <property name="eclipselink.ddl-generation" value="none"/>
          <property name="eclipselink.ddl-generation.output-mode" value="database"/>
          <property name="eclipselink.application-location" value=""/>
          <property name="eclipselink.create-ddl-jdbc-file-name" value="create_db.sql"/>
          <property name="eclipselink.drop-ddl-jdbc-file-name" value="drop_db.sql"/>
        
          <!-- Logging -->
          <property name="eclipselink.logging.level" value="FINEST"/>
          <property name="eclipselink.logging.level.sql" value="FINEST"/>
          <property name="eclipselink.logging.timestamp" value="true"/>
          <property name="eclipselink.logging.session" value="true"/>
          <property name="eclipselink.logging.thread" value="true"/>
          <property name="eclipselink.logging.exceptions" value="true"/>
        
          <!-- Cache control -->
          <property name="eclipselink.cache.type.default" value="SoftWeak"/>
          <property name="eclipselink.cache.size.default" value="1000"/>
          <property name="eclipselink.cache.shared.default" value="false"/>
          <property name="eclipselink.flush-clear.cache" value="DropInvalidate"/>
          </properties>
        </persistence-unit>
      </persistence>
    '''

  }
}
