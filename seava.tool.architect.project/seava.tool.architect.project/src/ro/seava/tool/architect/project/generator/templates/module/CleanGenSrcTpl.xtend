package ro.seava.tool.architect.project.generator.templates.module

import ro.seava.tool.architect.project.project.Module

/**
 * Clean generated source code
 */
class CleanGenSrcTpl {

  /**
   * Template for ant.xml to delete generated source code
   */
  def doAntXml(Module m, String moduleFqn, String srcPrjPath) {
    '''
      <project name="«moduleFqn».clean_gen_src" default="cleanUp" basedir=".">
        <description>Clean the generated source code for module: «moduleFqn»</description>
        
        <property name="srcRoot" value="«srcPrjPath»" />
        <property name="module" value="«moduleFqn»" />
        
        <target name="cleanUp" depends="clean">
          <echo>Done.</echo>
        </target>
        
        «m.clean»
       </project>
    '''

  //            «m.clean_business»
  //        «m.clean_presenter»
  //        «m.clean_ui_extjs»
  }

  /**
   * Clean module
   */
  def private clean(Module m) {
    '''
      
      <!-- =================================   Clean business  ==================================  -->
      
      <target name="clean">
        <echo>Deleting the generated files for business: </echo>
          <delete includeEmptyDirs="true" quiet="true" verbose="true">
              <fileset dir="${srcRoot}/${module}/src/main/java" includes="**/generated/**" />
              <fileset dir="${srcRoot}/${module}/src/main/resources" excludes="**/META-INF/**,**/extensions/**" />
              <fileset dir="${srcRoot}/${module}/src/main/resources/META-INF/spring/beans" includes="**" />
              <fileset dir="${srcRoot}/${module}/src/main/resources/META-INF" includes="*.xml" />
          </delete>
      </target>
    '''
  }

  /**
   * Clean business layer elements
   */
  def private clean_business(Module m) {
    '''
      
      <!-- =================================   Clean business  ==================================  -->
      
      <target name="cleanBusiness">
        <echo>Deleting the generated files for business: </echo>
        «IF m.config.db != null || m.config.domain != null || m.config.businessApi != null || m.config.business != null»
          <delete includeEmptyDirs="true" quiet="true" verbose="true">
            «IF m.config.db != null»
              <fileset dir="${srcRoot}/${module}.db/src/main/resources" excludes="**/extensions/**" />
            «ENDIF»
            «IF m.config.domain != null»
              <fileset dir="${srcRoot}/${module}.domain/src/main/java" includes="**/generated/**" />
              <fileset dir="${srcRoot}/${module}.domain/src/main/resources/META-INF" includes="*.xml" />
            «ENDIF»
            «IF m.config.businessApi != null»
              <fileset dir="${srcRoot}/${module}.business.api/src/main/java" includes="**/generated/**" />
            «ENDIF»
            «IF m.config.business != null»
              <fileset dir="${srcRoot}/${module}.business/src/main/java" includes="**/generated/**" />
              <fileset dir="${srcRoot}/${module}.business/src/main/resources/META-INF/spring/beans" includes="*.xml" />
            «ENDIF»          
          </delete>
        «ENDIF»
      </target>
    '''
  }

  /**
   * Clean presenter layer elements
   */
  def private clean_presenter(Module m) {
    '''
      
      <!-- =================================   Clean presenter  ==================================  -->
       
      <target name="cleanPresenter">
        <echo>Deleting generated files for presenter: </echo>
        «IF m.config.presenter != null»
          <delete includeEmptyDirs="true" quiet="true" verbose="true">
            «IF m.config.presenter != null»
              <fileset dir="${srcRoot}/${module}.presenter/src/main/java" includes="**/generated/**" />
              <fileset dir="${srcRoot}/${module}.presenter/src/main/resources/META-INF/spring/beans" includes="*.xml" />
            «ENDIF»
          </delete>
        «ENDIF»
      </target>
    '''
  }

  /**
   * Clean user interface elements
   */
  def private clean_ui_extjs(Module m) {
    '''
      
      <!-- =================================   Clean ui  ==================================  -->
      
      <target name="cleanUiExtjs">
        <echo>Deleting generated files for extjs user interface: </echo>
        «IF m.config.ui != null»
          <delete includeEmptyDirs="true">
            «IF m.config.ui != null»
              <fileset dir="${srcRoot}/${module}.ui.extjs/src/main/resources/webapp" includes="**/generated/**" />
            «ENDIF»
          </delete>
        «ENDIF»
        «IF m.config.trl != null»
          <delete includeEmptyDirs="true">
            <fileset dir="${srcRoot}/${module}.i18n/src/main/resources/webapp" includes="**/generated/**" />
          </delete>
        «ENDIF»
      </target>
    '''
  }

}
