package ro.seava.tool.architect.project.generator.templates.product

import ro.seava.tool.architect.project.project.Product

/**
 * Generate product level application-context.xml file
 */
class ApplicationContextTpl {

  /**
   * Generate file
   */
  def doFile(Product p) {
    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:aop="http://www.springframework.org/schema/aop"
        xmlns:context="http://www.springframework.org/schema/context"
        xmlns:tx="http://www.springframework.org/schema/tx"
        xsi:schemaLocation="
          http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd
          http://www.springframework.org/schema/aop
          http://www.springframework.org/schema/aop/spring-aop.xsd
          http://www.springframework.org/schema/context
          http://www.springframework.org/schema/context/spring-context.xsd
          http://www.springframework.org/schema/tx
          http://www.springframework.org/schema/tx/spring-tx.xsd">
      
        «p.applicationContext»
      
      </beans>
    '''

  //        <import resource="classpath:META-INF/spring/jee/j4e-application.xml"/>
  //      
  //        <import resource="classpath:META-INF/spring/jee/ad-security-authentic-context.xml"/>
  //        <import resource="classpath:META-INF/spring/jee/ad-security-authoriz-context.xml"/>
  //        
  //        «FOR m : p.modules»
  //          <import resource="classpath:META-INF/spring/jee/«m.module.name»-business-context.xml"/>
  //          <import resource="classpath:META-INF/spring/jee/«m.module.name»-presenter-context.xml"/>
  //        «ENDFOR»        
  }
}
