/*
 * generated by Xtext
 */
package ro.seava.tool.architect.project.scoping

import static extension ro.seava.tool.architect.base.scoping.BaseScopeUtils.*

import org.eclipse.xtext.scoping.IScope
import org.eclipse.emf.ecore.EReference
import ro.seava.tool.architect.project.project.Module
import ro.seava.tool.architect.project.project.License
import ro.seava.tool.architect.project.project.ArtifactMvn

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 *
 */
class ProjectScopeProvider extends org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider {

  def IScope scope_Attr(Module ctx, EReference ref) {
    return delegate.getScope(ctx, ref).filtered_Attr_scope("mvn-pom-scm")
  }

  def IScope scope_Attr(ArtifactMvn ctx, EReference ref) {
    return delegate.getScope(ctx, ref).filtered_Attr_scope("mvn-pom-property")
  }

  def IScope scope_Attr(License ctx, EReference ref) {
    return delegate.getScope(ctx, ref).filtered_Attr_scope("license-descriptor")
  }
}
