package ro.seava.tool.architect.project.generator.templates.artifact

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.project.project.ArtifactMvn
import ro.seava.tool.architect.project.project.AttrMap
import ro.seava.tool.architect.project.project.AttrMapEntry
import java.util.HashMap
import java.util.Map
import ro.seava.tool.architect.project.project.Library
import java.util.List
import ro.seava.tool.architect.project.project.Version
import java.util.ArrayList
import ro.seava.tool.architect.project.project.MvnPlugin
import ro.seava.tool.architect.project.project.ArtifactMvnProfile
import ro.seava.tool.architect.project.project.MvnDependencyElement
import ro.seava.tool.architect.project.extensions.PomDependencyDefinition

class PomTpl {

  def doFile(ArtifactMvn ac) {
    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
      	
      	<modelVersion>4.0.0</modelVersion>
      	 «IF ac.artifact.parent != null»
      	   <parent>
      	     «IF ac.artifact.parent.childrenNotNested»<relativePath>../«ac.artifact.parent.artifactId»</relativePath>«ENDIF»
      	     «ac.parent_groupId_tpl»
      	     «ac.parent_artifactId_tpl»
      	     «ac.parent_version_tpl»
      	   </parent>
      	 «ENDIF»
      	«ac.groupId_tpl»
      	«ac.artifactId_tpl»
      	«ac.version_tpl»
      	<name>«ac.artifact.title»</name>
      	<description>«ac.artifact.description»</description>
      	<packaging>«ac.artifact.type.name»</packaging>
      	«ac.url_tpl»
      	«ac.licenses_tpl»
      	«ac.scm_tpl»
      	«ac.pom_properties_tpl»
      	«ac.distribution_management_tpl»
      	«ac.dependency_management_tpl»
      	«ac.dependencies_tpl»
      	«ac.repositories_tpl»
      	«ac.build_tpl»
      	«ac.modules_tpl»
      </project>
    '''
  }

  /**
   * Template for `groupId` attribute
   */
  def private groupId_tpl(ArtifactMvn ac) {
    var boolean printIt = false;
    if (ac.artifact.parent == null) {
      printIt = true
    } else {
      if (ac.artifact.module != null) {
        if (ac.artifact.parent.module != null && ac.artifact.module.group != ac.artifact.parent.module.group) {
          printIt = true
        }
      }
    }
    if (printIt) {
      return '''<groupId>«ac.artifact.groupId»</groupId>'''
    }
  }

  /**
   * Template for `artifactId` attribute
   */
  def private artifactId_tpl(ArtifactMvn ac) {
    '''<artifactId>«ac.artifact.artifactId»</artifactId>'''
  }

  /**
   * Template for `version` attribute
   */
  def private version_tpl(ArtifactMvn ac) {
    var String _v = ac.artifact.resolveVersion.versionValue;
    var String _pv = null;
    if (ac.artifact.parent != null) {
      _pv = ac.artifact.parent.resolveVersion.versionValue;
    }
    if (!_v.equals(_pv)) {
      return '''<version>«_v»</version>'''
    }
  }

  // ================= parent artifact ==================
  /**
   * Template for parent `groupId` attribute
   */
  def private parent_groupId_tpl(ArtifactMvn ac) {
    return '''<groupId>«ac.artifact.parent.groupId»</groupId>'''
  }

  /**
   * Template for parent `artifactId` attribute
   */
  def private parent_artifactId_tpl(ArtifactMvn ac) {
    '''<artifactId>«ac.artifact.parent.artifactId»</artifactId>'''
  }

  /**
   * Template for parent `version` attribute
   */
  def private parent_version_tpl(ArtifactMvn ac) {
    return '''<version>«ac.artifact.parent.resolveVersion.versionValue»</version>'''
  }

  /**
   * Template for `url` attribute
   */
  def private url_tpl(ArtifactMvn ac) {
    var m = ac.artifact.module;
    if (m != null) {
      var _u = m.url;
      if (_u != null) {
        return '''<url>«_u»</url>'''
      }
    }
  }

  /**
   * Template for artifact licenses
   */
  def private licenses_tpl(ArtifactMvn ac) {
    var _lics = ac.artifact.licenses;
    if (_lics.size > 0) {
      '''
        
        <licenses>
          «FOR lic : _lics»
            <license>
              «lic.attrMap._attrMap_elements_tpl»
            </license>
          «ENDFOR»
        </licenses>
      '''
    }
  }

  /**
   * Template for artifact source-control-management
   */
  def private scm_tpl(ArtifactMvn ac) {
    var m = ac.artifact.module;
    if (m != null) {
      var scm = m.scm;
      if (scm != null) {
        '''
          
          <scm>
            «scm._attrMap_elements_tpl»
          </scm>
        '''
      }
    }
  }

  /**
   * Template for artifact properties
   */
  def private pom_properties_tpl(ArtifactMvn ac) {

    var boolean hasProperties = false;

    var Map<String, String> declaredProperties = new HashMap<String, String>()
    var List<MvnPlugin> plugins = new ArrayList<MvnPlugin>()
    var List<Version> versions = new ArrayList<Version>()

    // collect explicit properties
    if (ac.pomProperties != null && ac.pomProperties.entries.length > 0) {
      for (e : ac.pomProperties.entries) {
        declaredProperties.put(e.attr.attr_print_name, e.value)
      }
    }

    //collect plugin versions from build profiles
    if (ac.buildProfiles != null) {
      for (bp : ac.buildProfiles) {
        var mpu = bp.pluginManagement
        if (mpu != null) {
          for (pu : mpu.pluginUsages) {
            plugins.add(pu.plugin)
          }
        }
      }
    }

    // collect versions from dependencies
    var List<MvnDependencyElement> dpds = ac.collectDependencyElementsFromDependenciesManagement

    if (dpds.size > 0) {
      var src = dpds.filter(t|(t.dependency instanceof Library)).sortBy(t|(t.dependency as Library).version.name);
      for (e : src) {
        var v = (e.dependency as Library).version
        if (!versions.contains(v)) {
          versions.add(v);
        }
      }
    }

    if (versions.size > 0 || plugins.size > 0 || !declaredProperties.empty || ac.artifact.module != null) {
      hasProperties = true
    }

    if (hasProperties) {
      '''
        
        <properties>
          «IF plugins.size > 0»
            
            <!-- plugin version -->
            
            «FOR p : plugins.sortBy[t|t.name]»
              <version.plugin.«p.name»>«p.version»</version.plugin.«p.name»>
            «ENDFOR»
          «ENDIF»
          «IF versions.size > 0»
            
            <!-- library version -->
            
            «FOR v : versions.sortBy[t|t.name]»
              <version.«v.name»>«v.versionValue»</version.«v.name»>
            «ENDFOR»
          «ENDIF»
          «IF !declaredProperties.empty»
            
            <!-- properties -->
            
            «FOR p : declaredProperties.entrySet.sortBy[t|t.key]»
              <«p.key»>«p.value»</«p.key»>
            «ENDFOR»
          «ENDIF»
          «IF ac.artifact.module != null && !ac.artifact.module.noVersionProperty»
            <version.«ac.artifact.module.version.name»>«ac.artifact.module.version.versionValue»</version.«ac.artifact.
          module.version.name»>
            «var modules = ac.artifact.module.collectRequiredModules»
            «IF modules != null && modules.length > 0»
              «FOR m : modules»
                <version.«m.version.name»>«m.version.versionValue»</version.«m.version.name»>
              «ENDFOR»
            «ENDIF»
          «ENDIF»
        </properties>
      '''
    }

  //else if (ac.artifact.module != null) {
  //      var module = ac.artifact.module;
  //      var modules = module.modules;
  //      '''
  //        
  //        <properties>
  //          <version.«module.moduleFqn»>«module.version.versionValue»</version.«module.moduleFqn»>
  //          «IF modules != null && modules.length > 0»
  //            «FOR m : modules»
  //              <version.«m.moduleFqn»>«m.version.versionValue»</version.«m.moduleFqn»>
  //            «ENDFOR»
  //          «ENDIF»
  //        </properties>
  //      '''
  //    }
  }

  /**
   * Template for artifact distribution management
   */
  def private distribution_management_tpl(ArtifactMvn ac) {
    if (ac.distributionManagement != null) {
      '''
        
        <distributionManagement>
          «ac.distributionManagement»
        </distributionManagement>
      '''
    }
  }

  /**
   * Template for artifact dependency management 
   */
  def private dependency_management_tpl(ArtifactMvn ac) {
    var dpdElements = ac.collectDependencyElementsFromDependenciesManagement;
    if (dpdElements.size > 0) {
      '''
        
        <dependencyManagement>
          <dependencies>
            «FOR dpd : dpdElements.filter(e|e.dependency instanceof Library).sortBy(
          e|(e.dependency as Library).artifactId)»
              «var lib = dpd.dependency as Library»
              <dependency>
                <groupId>«lib.group.groupId»</groupId>
                <artifactId>«lib.artifactId»</artifactId>
                <version>${version.«lib.version.name»}</version>
                <type>«lib.type.name»</type>
                «IF dpd.exclusions.size > 0»
                  <exclusions>
                    «FOR exclusion : dpd.exclusions»
                      <exclusion>
                        <groupId>«exclusion.group»</groupId>
                        <artifactId>«exclusion.artifactId»</artifactId>
                      </exclusion>
                    «ENDFOR»
                  </exclusions>
                «ENDIF»
              </dependency>
            «ENDFOR»
          </dependencies>
        </dependencyManagement>
      '''
    }
  }

  /**
   * 
   */
  def private dependencies_tpl(ArtifactMvn ac) {

    var dependencies = ac.collectDependencyElementsFromDependencies;
    if (dependencies.size > 0) {
      '''
        
        <!-- ===================== Dependencies ===================== -->
        
        <dependencies>
        
          <!-- Internal -->
          
          «FOR pdd : dependencies.collectPomDependenciesOfTypeArtifact.sortBy(e|e.artifactId)»
            «pdd._pom_dependency_tpl(true)»
          «ENDFOR»
        
          <!-- External -->
          
          «FOR pdd : dependencies.collectPomDependenciesOfTypeLibrary.sortBy(e|e.artifactId)»
            «pdd._pom_dependency_tpl(false)»
          «ENDFOR»
        </dependencies>
      '''
    }
  }

  /**
   * 
   */
  def private _pom_dependency_tpl(PomDependencyDefinition pdd, boolean isInternal) {
    '''
      <dependency>
        <groupId>«pdd.groupId»</groupId>
        <artifactId>«pdd.artifactId»</artifactId>
        «IF isInternal»
          <version>${version.«pdd.moduleFqn»}</version>
        «ELSE»
          «IF pdd.version != null»
            <version>«pdd.version»</version>
          «ENDIF»
        «ENDIF»
        «IF pdd.type != null»
          <type>«pdd.type»</type>
        «ENDIF»       
        «IF pdd.scope != null»
          <scope>«pdd.scope»</scope>
        «ENDIF»
      </dependency>
    '''
  }

  /**
   * Template for artifact repositories
   */
  def private repositories_tpl(ArtifactMvn ac) {
    if (ac.dependencyRepositories != null || ac.pluginRepositories != null) {
      '''
        
        <!-- ===================== Repositories ===================== -->
        
        <!-- Add to your maven settings.xml the following repositories. -->
        
        «IF ac.dependencyRepositories != null && ac.dependencyRepositories.repositories.size > 0»
          <!-- 
          <repositories>  
            «FOR repo : ac.dependencyRepositories.repositories» 
              <repository>
                <id>«repo.id»</id>
                <name>«repo.title»</name>
                <url>«repo.url»</url>
              </repository>
            «ENDFOR»
          </repositories>
          -->
        «ENDIF»
        «IF ac.pluginRepositories != null && ac.pluginRepositories.repositories.size > 0» 
          <!-- 
          <pluginRepositories>
            «FOR repo : ac.pluginRepositories.repositories»
              <pluginRepository>
                <id>«repo.id»</id>
                <name>«repo.title»</name>
                <url>«repo.url»</url>
              </pluginRepository>
            «ENDFOR»
          </pluginRepositories>
          -->
        «ENDIF»
      '''
    }
  }

  /**
   * Template for artifact build rules
   */
  def private build_tpl(ArtifactMvn ac) {
    var CharSequence result = '''''';
    if (ac.buildProfiles != null && ac.buildProfiles.size > 0) {

      var profileBPs = ac.buildProfiles.filter[t|!t.profile.defaultProfile]
      var defaultBPs = ac.buildProfiles.filter[t|t.profile.defaultProfile]

      if (defaultBPs.size > 0) {
        result = result + '''
          
          <!-- ===================== Build ===================== -->
          
          «defaultBPs.get(0)._build_content_tpl»
        '''
      }

      if (profileBPs.size > 0) {
        result = result + '''
          
          <!-- ===================== Profiles ===================== -->
          
          <profiles>
            «FOR bp : profileBPs»
              <profile>
                <id>«bp.profile.value»</id>
                «bp._build_content_tpl»
              </profile>
            «ENDFOR»
            
          </profiles>
        '''
      }
    }
    return result
  }

  /**
   * 
   */
  def private _build_content_tpl(ArtifactMvnProfile ap) {

    if (ap.pluginManagement != null || ap.pluginDependency != null || ap.buildConfigs.size > 0) {
      '''
        <build>
          «IF ap.pluginManagement != null»
            <pluginManagement>
              <plugins>
                «FOR pluginUsage : ap.pluginManagement.pluginUsages»
                  «var plugin = pluginUsage.plugin»
                  <plugin>
                    <groupId>«plugin.group.groupId»</groupId>
                    <artifactId>«plugin.artifactId»</artifactId>
                    <version>${version.plugin.«plugin.name»}</version>
                    «IF pluginUsage.configRef != null»«pluginUsage.configRef.value»«ELSE»«pluginUsage.config»«ENDIF»
                    «IF pluginUsage.executionRef != null»«pluginUsage.executionRef.value»«ELSE»«pluginUsage.execution»«ENDIF»
                  </plugin>
                «ENDFOR» 
              </plugins>
            </pluginManagement>
          «ENDIF»
          «IF ap.pluginDependency != null»
            <plugins>
              «FOR plugin : ap.pluginDependency.pluginUsages»
                <plugin>
                  <groupId>«plugin.plugin.group.groupId»</groupId>
                  <artifactId>«plugin.plugin.artifactId»</artifactId>
                  «IF plugin.configRef != null»«plugin.configRef.value»«ELSE»«plugin.config»«ENDIF»
                  «IF plugin.executionRef != null»«plugin.executionRef.value»«ELSE»«plugin.execution»«ENDIF»
                </plugin>
              «ENDFOR»
            </plugins>
          «ENDIF»
          «IF ap.buildConfigs.size > 0»
            «FOR bc : ap.buildConfigs»
              «bc.value»
            «ENDFOR»
          «ENDIF»
        </build>
      '''
    }
  }

  /**
   * Template for artifact modules
   */
  def private modules_tpl(ArtifactMvn ac) {
    if (ac.modules.size > 0) {
      var path = "";
      if (ac.artifact.childrenNotNested) {
        path = "../";
      }
      '''
        
        <!-- ========================== Modules ========================== -->
        
        <modules>
          «FOR m : ac.modules»
            <module>«path»«m.artifact.artifactId»</module>
          «ENDFOR»
        </modules>
      '''
    }
  }

  def private _attrMap_elements_tpl(AttrMap s) {
    '''
      «FOR e : s.entries»
        «e._attrMap_element_tpl»
      «ENDFOR»
    '''
  }

  def private _attrMap_element_tpl(AttrMapEntry e) {
    var key = e.attr.propertyName
    if (key == null) {
      key = e.attr.name
    }
    return '''<«key»>«e.value»</«key»>'''
  }

}
