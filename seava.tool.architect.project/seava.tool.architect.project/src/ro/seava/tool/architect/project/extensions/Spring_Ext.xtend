package ro.seava.tool.architect.project.extensions

import ro.seava.tool.architect.project.project.ArtifactSpringCfg
import java.util.List
import ro.seava.tool.architect.project.project.SpringNs
import ro.seava.tool.architect.project.project.SpringTemplate
import java.util.ArrayList

class Spring_Ext {

  /**
   * 
   */
  def static List<SpringNs> collectNamespaces(ArtifactSpringCfg cf) {
    var List<SpringNs> collector = new ArrayList<SpringNs>();
    var List<SpringTemplate> processedTokens = new ArrayList<SpringTemplate>();
    for (SpringNs ns : cf.getNamespaces()) {
      if (!collector.contains(ns)) {
        collector.add(ns);
      }
    }
    for (SpringTemplate t : cf.getIncludes()) {
      collectNamespaces(t, collector, processedTokens);
    }
    return collector;
  }

  /**
   * 
   */
  def static void collectNamespaces(SpringTemplate t, List<SpringNs> collector, List<SpringTemplate> processedTokens) {
    if (processedTokens.contains(t)) {
      return;
    }
    for (SpringNs ns : t.getNamespaces()) {
      if (!collector.contains(ns)) {
        collector.add(ns);
      }
    }
    for (SpringTemplate i : t.getIncludes()) {
      collectNamespaces(i, collector, processedTokens);
    }
  }
}
