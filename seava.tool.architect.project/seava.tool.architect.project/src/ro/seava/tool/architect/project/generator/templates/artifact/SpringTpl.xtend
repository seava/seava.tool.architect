package ro.seava.tool.architect.project.generator.templates.artifact

import static extension ro.seava.tool.architect.project.extensions.Spring_Ext.*

import java.util.List
import java.util.ArrayList

import ro.seava.tool.architect.project.project.ArtifactSpringCfg
import ro.seava.tool.architect.project.project.SpringTemplate
import ro.seava.tool.architect.project.project.SpringNs

class SpringTpl {

  def doFile(ArtifactSpringCfg cf) {
    var List<SpringTemplate> processed = new ArrayList<SpringTemplate>();
    var List<SpringNs> namespaces = cf.collectNamespaces;
    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <beans xmlns="http://www.springframework.org/schema/beans"
      	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      	«cf.namespace_aliases_tpl(namespaces)»
      	xsi:schemaLocation="
      		http://www.springframework.org/schema/beans
      		http://www.springframework.org/schema/beans/spring-beans.xsd«FOR ns : namespaces»
      		«ns.namespace_location_tpl()»«ENDFOR»">
      «cf.beforeIncludes»
      «FOR i : cf.includes»
        «i.include_token_tpl(processed)»
      «ENDFOR»
      «cf.afterIncludes»
      </beans>
    '''
  }

  /**
   * 
   */
  private def namespace_aliases_tpl(ArtifactSpringCfg cf, List<SpringNs> namespaces) {
    '''
      «FOR ns : namespaces»
        xmlns:«ns.alias»="«ns.url»"
      «ENDFOR»
    '''
  }

  /**
   * 
   */
  private def namespace_location_tpl(SpringNs ns) {
    '''		
    «ns.url»
    «ns.location»'''
  }

  /**
   * 
   */
  private def CharSequence include_token_tpl(SpringTemplate t, List<SpringTemplate> processed) {
    var CharSequence s = '''''';
    if (!processed.contains(t)) {
      processed.add(t)
      s = '''
        «t.beforeIncludes»
        «FOR i : t.includes»
          «i.include_token_tpl(processed)»
        «ENDFOR»
        «t.afterIncludes»
      '''
    }

    return s;
  }

}
