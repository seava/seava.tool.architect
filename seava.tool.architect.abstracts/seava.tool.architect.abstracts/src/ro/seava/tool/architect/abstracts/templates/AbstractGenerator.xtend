package ro.seava.tool.architect.abstracts.templates

import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.emf.ecore.resource.Resource
import ro.seava.tool.architect.abstracts.Constants

class AbstractGenerator {

  /**
   * Replace the current meta project with the src project
   */
  def protected void setTargetProjectSrc(Resource resource, IFileSystemAccess fsa) {
    var String prj = resource.URI.segment(1);
    System.out.print(prj);
    var _fsa = fsa as EclipseResourceFileSystemAccess2;
    var _workspace = ResourcesPlugin::getWorkspace();
    var _project = _workspace.root.getProject(prj.replaceFirst(".meta", Constants.SRC_PROJECT_SUFFIX));
    _fsa.setProject(_project);
  }

  /**
   * Replace the current src project with the meta project
   */
  def protected void setTargetProjectMeta(Resource resource, IFileSystemAccess fsa) {
    var String prj = resource.URI.segment(1);
    var _fsa = fsa as EclipseResourceFileSystemAccess2;
    var _workspace = ResourcesPlugin::getWorkspace();
    var _project = _workspace.root.getProject(prj);
    _fsa.setProject(_project);
  }

}
