package ro.seava.tool.architect.abstracts.templates

class Abstract_Tpl {
	
	
	def String simpleNameFromFQN(String fqn) {
		return fqn.substring(fqn.lastIndexOf('.') + 1);
	}
	
	
	def static copyright() '''
	/**
	 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
	 * Use is subject to license terms.
	 */
	'''
}