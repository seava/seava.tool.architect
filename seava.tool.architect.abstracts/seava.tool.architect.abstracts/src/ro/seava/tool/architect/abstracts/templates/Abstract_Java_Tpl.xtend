package ro.seava.tool.architect.abstracts.templates

import java.util.Set
import java.util.HashSet
import java.util.Collections
import java.util.List

class Abstract_Java_Tpl extends Abstract_Tpl {
	
	private Set<String> _imports;
	
	override String simpleNameFromFQN(String fqn) {
		if (fqn.contains(".")) {
			if ( ! fqn.startsWith("java.lang")){
				if(_imports == null) {
					_imports = new HashSet<String>();
				}
				_imports.add(fqn);
			}
			return fqn.substring(fqn.lastIndexOf('.') + 1);
		} else {
			return fqn;
		}		
	}

	def protected getImports() {
		if(_imports == null) {
			_imports = new HashSet<String>();
		}
		var List<String> imports = this._imports.toList;
		Collections::sort(imports);
		
		var result = '''
		«FOR imp : imports»
			import «imp»;
		«ENDFOR»
		'''
		_imports = new HashSet<String>();
		return result;
	}
}