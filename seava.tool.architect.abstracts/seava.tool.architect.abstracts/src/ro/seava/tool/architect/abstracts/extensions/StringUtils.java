package ro.seava.tool.architect.abstracts.extensions;

public class StringUtils {

	public static String pathFromFQN(String s) {
		return s.replace('.', '/');
	}

	public static String firstTokenFromFQN(String s) {
		return s.split("\\.")[0];
	}

	public static String lastTokenFromFQN(String s) {
		String[] t = s.split("\\.");
		return t[t.length - 1];
	}

	public static String removeFirstTokenFromFQN(String s) {
		int i = s.indexOf(".");
		if (i < 0) {
			return "";
		} else {
			return s.substring(i + 1, s.length());
		}
	}

	// public static String simpleNameFromFQN(String fqn) {
	// return fqn.substring(fqn.lastIndexOf('.') + 1);
	// }

	/**
	 * Converts the string with a camel case or with underscores and replace it
	 * with spaces between each word, and underscores replaced by spaces. For
	 * example "javaProgramming" and "JAVA_PROGRAMMING" would both become Java
	 * Programming".
	 * 
	 * @param str
	 *            The String to convert
	 * @return The converted String
	 */
	public static String toTitleCase(String str) {
		if (str == null || str.length() == 0) {
			return str;
		}

		StringBuffer result = new StringBuffer();

		/*
		 * Pretend space before first character
		 */
		char prevChar = ' ';

		/*
		 * Change underscore to space, insert space before capitals
		 */
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '_') {
				result.append(' ');
			} else if (prevChar == ' ' || prevChar == '_') {
				result.append(Character.toUpperCase(c));
			} else if (Character.isUpperCase(c)
					&& !Character.isUpperCase(prevChar)) {
				/*
				 * Insert space before start of word if camel case
				 */
				result.append(' ');
				result.append(Character.toUpperCase(c));
			} else {
				result.append(c);
			}

			prevChar = c;
		}

		return result.toString();
	}
}
