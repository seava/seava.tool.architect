package ro.seava.tool.architect.abstracts.xtext_overrides;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import org.eclipse.xtext.generator.IOutputConfigurationProvider;
import org.eclipse.xtext.generator.OutputConfiguration;

public class MyOutputConfigurationProvider implements
		IOutputConfigurationProvider {

	public final static String SRC = "SRC";
	public final static String SRC_ONCE = "SRC_ONCE";

	public final static String META = "META";
	public final static String META_ONCE = "META_ONCE";

	/**
	 * @return a set of {@link OutputConfiguration} available for the generator
	 */
	public Set<OutputConfiguration> getOutputConfigurations() {
		OutputConfiguration srcOutput = new OutputConfiguration(SRC);
		srcOutput.setDescription("Src output");
		srcOutput.setOutputDirectory(".");
		srcOutput.setOverrideExistingResources(true);
		srcOutput.setCreateOutputDirectory(true);
		srcOutput.setCleanUpDerivedResources(true);
		srcOutput.setSetDerivedProperty(true);

		OutputConfiguration srcOnceOutput = new OutputConfiguration(SRC_ONCE);
		srcOnceOutput.setDescription("Src output (once)");
		srcOnceOutput.setOutputDirectory(".");
		srcOnceOutput.setOverrideExistingResources(false);
		srcOnceOutput.setCreateOutputDirectory(true);
		srcOnceOutput.setCleanUpDerivedResources(false);
		srcOnceOutput.setSetDerivedProperty(false);

		OutputConfiguration metaOutput = new OutputConfiguration(META);
		metaOutput.setDescription("Meta output");
		metaOutput.setOutputDirectory("./meta-gen");
		metaOutput.setOverrideExistingResources(true);
		metaOutput.setCreateOutputDirectory(true);
		metaOutput.setCleanUpDerivedResources(true);
		metaOutput.setSetDerivedProperty(true);

		OutputConfiguration metaOnceOutput = new OutputConfiguration(META_ONCE);
		metaOnceOutput.setDescription("Meta output (once)");
		metaOnceOutput.setOutputDirectory("./meta-gen-once");
		metaOnceOutput.setOverrideExistingResources(false);
		metaOnceOutput.setCreateOutputDirectory(true);
		metaOnceOutput.setCleanUpDerivedResources(false);
		metaOnceOutput.setSetDerivedProperty(false);

		return newHashSet(srcOutput, srcOnceOutput, metaOutput, metaOnceOutput);
	}

}
