package ro.seava.tool.architect.abstracts.ui;

import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper;

public class MyTokenToAttributeIdMapper extends
		DefaultAntlrTokenToAttributeIdMapper {

	@Override
	protected String calculateId(String tokenName, int tokenType) {
		if ("RULE_DOCUMENTATION".equals(tokenName)) {
			return MySyntaxColoringConfiguration.DOCUMENTATION_ID;
		}
		return super.calculateId(tokenName, tokenType);
	}
}
