package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.view

import ro.seava.tool.architect.viewdc.viewDc.DcView
import ro.seava.tool.architect.viewdc.viewDc.Field

abstract class AbstractDcPropGridViewTpl extends AbstractDcViewTpl {

  override _define_elements_tpl(DcView v) {
    '''
      
      /**
       * Components definition
       */
      _defineElements_: function() {
        this._getBuilder_()
          «FOR cmp : v.items.filter(typeof(Field))»
            «getFieldTpl().item_tpl(cmp)»
          «ENDFOR»   
      },
    '''
  }

  override protected _link_elements_tpl(DcView v) {
    ''''''
  }

}
