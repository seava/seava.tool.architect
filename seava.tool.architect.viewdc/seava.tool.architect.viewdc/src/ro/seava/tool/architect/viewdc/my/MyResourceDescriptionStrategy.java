package ro.seava.tool.architect.viewdc.my;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.util.IAcceptor;

public class MyResourceDescriptionStrategy extends
		DefaultResourceDescriptionStrategy {

	public boolean createEObjectDescriptions(EObject eObject,
			IAcceptor<IEObjectDescription> acceptor) {
		// if (eObject instanceof Attr) {
		// return this.descriptionForAttr((Attr) eObject, acceptor);
		// }
		return super.createEObjectDescriptions(eObject, acceptor);
	}

	// private boolean descriptionForAttr(Attr t,
	// IAcceptor<IEObjectDescription> acceptor) {
	// if (getQualifiedNameProvider() == null)
	// return false;
	// try {
	// QualifiedName qualifiedName = getQualifiedNameProvider()
	// .getFullyQualifiedName(t);
	// if (qualifiedName != null) {
	// Map<String, String> userData = new HashMap<String, String>();
	// userData.put("target", t.getTarget());
	// acceptor.accept(EObjectDescription.create(qualifiedName, t,
	// userData));
	// }
	// } catch (Exception exc) {
	// LOG.error(exc.getMessage());
	// }
	// return true;
	// }
}
