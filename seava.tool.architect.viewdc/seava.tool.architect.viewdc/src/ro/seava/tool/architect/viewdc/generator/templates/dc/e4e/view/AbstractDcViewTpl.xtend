package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.view

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.viewdc.extensions.Dc_Ext.*
import static extension ro.seava.tool.architect.viewdc.extensions.Lov_Cfg.*

import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.Abstract_Dc_Tpl
import ro.seava.tool.architect.viewdc.viewDc.DcView
import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component.AbstractControlTpl
import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component.PanelTpl
import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component.AbstractContainerTpl
import ro.seava.tool.architect.viewdc.viewDc.Field
import ro.seava.tool.architect.viewdc.viewDc.IContainer
import java.util.HashMap
import ro.seava.tool.architect.base.base.E_DataType

abstract class AbstractDcViewTpl extends Abstract_Dc_Tpl {

  private PanelTpl panelTpl = new PanelTpl;

  /**
   * Generate dependencies file
   */
  def doDependencyFile(DcView v) {
    '''
      [
        «FOR lovField : v.items.filter(typeof(Field)).filter(t|t.config.lov != null) SEPARATOR ', '»
          "«lovField.config.lov.canonicalNameLov»"
        «ENDFOR»
      ]
    '''
  }

  /**
   * Generate translation file for default system language
   */
  def doTrlFile(DcView v) {
    var keys = new HashMap<String, String>()

    for (item : v.items.filter(typeof(IContainer)).filter(e|e.config != null && e.config.title != null)) {
      keys.put(item.name + "__ttl", item.config.title)
    }

    for (item : v.items.filter(typeof(Field)).filter(e|e.config != null && e.config.title != null)) {
      keys.put(item.name + "__lbl", item.config.title)
    }

    '''
      «copyright»
      Ext.define("«v.canonicalNameGeneratedDcViewTrl»",{
        «FOR e : keys.entrySet.sortBy[e|e.key] SEPARATOR ','»
          «e.key»: "«e.value»"
        «ENDFOR»
      });
    '''
  }

  def CharSequence doJsClass(DcView v) {
    var CharSequence defFragment = v._define_elements_tpl;
    var CharSequence linkFragment = v._link_elements_tpl;
    '''
      «copyright»
      Ext.define("«v.canonicalNameGeneratedDcView»", {
        «v.attributes_tpl»
        «defFragment»
        «linkFragment»
        «v.functions.define_functions_tpl»
        
        extend: "«this.getExtendedClassFqn»",
        alias: "widget.«v.xtype»"
      });
    '''
  }

  /**
   * 
   */
  def protected CharSequence attributes_tpl(DcView v) {
    if (v.attrMap != null) {
      '''
        «FOR at : v.attrMap.entries»
          «at.attr.attr_print_name»:«IF at.attr.valueType == E_DataType.STRING»"«at.value»"«ELSE»«at.value»«ENDIF»,
        «ENDFOR»
      '''
    }
  }

  abstract def protected String getExtendedClassFqn();

  abstract def protected CharSequence _define_elements_tpl(DcView v)  ;

  abstract def protected CharSequence _link_elements_tpl(DcView v)  ;

  abstract def protected AbstractControlTpl getFieldTpl();

  def protected AbstractContainerTpl getPanelTpl() {
    return this.panelTpl;
  }

}
