package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component

import ro.seava.tool.architect.viewdc.viewDc.Panel
import ro.seava.tool.architect.viewdc.viewDc.IViewItem
import ro.seava.tool.architect.viewdc.viewDc.E_Layout

class PanelTpl extends AbstractContainerTpl {

  override def public item_tpl(IViewItem i) {
    var p = i as Panel;
    '''.addPanel({ name:"«p.name»"«p._commons_tpl»«p._layout_tpl»«p.config.attrMap.attribute_set_tpl(true)»«IF p.
      attrMapChildren != null», defaults: {«p.attrMapChildren.attribute_set_tpl(false)»}«ENDIF»«p.config._buttons»})'''
  }

  def private _commons_tpl(Panel p) {
    '''«IF p.name.matches("main")», autoScroll:true«ENDIF»«IF p.config.title != null», _hasTitle_: true«ENDIF»«p.
      _width_tpl»«p._height_tpl»'''
  }

  def private _layout_tpl(Panel p) {
    var t = p.layout
    if (t == E_Layout.FORM) {
      return p.container_form_tpl
    } else if (t == E_Layout.HORIZONTAL) {
      return p.container_hlayout_tpl
    } else if (t == E_Layout.VERTICAL) {
      return p.container_vlayout_tpl
    } else if (t == E_Layout.BORDER) {
      return p.container_border_layout_tpl
    } else if (t == E_Layout.STACK) {
      return p.container_stack_layout_tpl
    } else if (t == E_Layout.TAB) {
      return p.container_tab_layout_tpl
    } else if (t == E_Layout.WRAPPER) {
      return p.container_wrapper_layout_tpl
    } else if (t == E_Layout.ACCORDION) {
      return p.container_accordion_layout_tpl
    }
  }

  def private container_form_tpl(Panel v) {
    ''', layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}'''
  }

  def private container_hlayout_tpl(Panel v) {
    ''', layout: {type:"hbox", align:"begin", pack:"start"}'''
  }

  def private container_vlayout_tpl(Panel v) {
    ''', layout: {type:"vbox", align:"begin", pack:"start"}'''
  }

  def private container_accordion_layout_tpl(Panel v) {
    ''', layout:{ type: "accordion"}, activeItem:0'''
  }

  def private container_border_layout_tpl(Panel v) {
    ''', layout:{ type: "border"}, defaults:{split:true}'''
  }

  def private container_stack_layout_tpl(Panel v) {
    ''', layout:{ type: "card"}, activeItem:0'''
  }

  def private container_tab_layout_tpl(Panel v) {
    ''', xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false'''
  }

  def private container_wrapper_layout_tpl(Panel v) {
    ''', layout: { type: "fit"}'''
  }

}
