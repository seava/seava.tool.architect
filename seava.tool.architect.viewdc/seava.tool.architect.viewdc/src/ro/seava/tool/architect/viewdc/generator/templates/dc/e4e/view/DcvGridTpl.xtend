package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.view

import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component.GridColumnTpl

class DcvGridTpl extends AbstractDcGridViewTpl {

  private GridColumnTpl fieldTpl = new GridColumnTpl;

  override protected getExtendedClassFqn() {
    return "seava.lib.e4e.js.dc.view.AbstractDcvGrid";
  }

  override protected getFieldTpl() {
    return this.fieldTpl
  }
}
