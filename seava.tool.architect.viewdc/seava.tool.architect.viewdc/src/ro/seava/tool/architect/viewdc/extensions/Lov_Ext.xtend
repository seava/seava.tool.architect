package ro.seava.tool.architect.viewdc.extensions

import ro.seava.tool.architect.viewdc.viewDc.Lov
import java.util.List
import ro.seava.tool.architect.viewdc.viewDc.LovField
import java.util.ArrayList

class Lov_Ext extends Lov_Cfg {

  def static String getReturnFieldName(Lov lov) {
    if (lov.defaultReturnField != null) {
      return lov.defaultReturnField.source.name;
    } else {
      if (lov.superType != null) {
        return getReturnFieldName(lov.superType);
      }
    }
    return null;
  }

  def static List<LovField> getAllDisplayedFields(Lov lov) {
    var List<LovField> result = new ArrayList<LovField>();

    if (lov.superType != null) {
      result.addAll(lov.superType.fields.filter(e|!e.notVisible).toList);
    }
    result.addAll(lov.fields.filter(e|!e.notVisible).toList);
    return result;
  }

}
