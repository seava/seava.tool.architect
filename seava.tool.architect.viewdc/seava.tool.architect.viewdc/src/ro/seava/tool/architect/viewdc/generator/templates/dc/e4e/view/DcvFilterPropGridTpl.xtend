package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.view

import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component.FilterPropGridFieldTpl

class DcvFilterPropGridTpl extends AbstractDcPropGridViewTpl {

  private FilterPropGridFieldTpl fieldTpl = new FilterPropGridFieldTpl;

  override protected getExtendedClassFqn() {
    return "seava.lib.e4e.js.dc.view.AbstractDcvFilterPropGrid";
  }

  override protected getFieldTpl() {
    return this.fieldTpl
  }

}
