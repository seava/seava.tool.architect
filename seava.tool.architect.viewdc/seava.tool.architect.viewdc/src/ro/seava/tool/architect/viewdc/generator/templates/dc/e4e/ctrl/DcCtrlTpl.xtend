package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.ctrl

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import static extension ro.seava.tool.architect.viewdc.extensions.Dc_Ext.*

import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.Abstract_Dc_Tpl
import ro.seava.tool.architect.viewdc.viewDc.Dc
import ro.seava.tool.architect.viewdc.viewDc.Function

class DcCtrlTpl extends Abstract_Dc_Tpl {

  def doTrlFile(Dc dc) {
    '''
      «copyright»
      Ext.define("«dc.canonicalNameGeneratedDcTrl»",{
      });
    '''
  }

  def doDependencyFile(Dc dc) {
    '''
      [
        «FOR i : dc.collect_dependencies SEPARATOR ', '»
          "«i»"
        «ENDFOR»
      ]
    '''
  }

  def doJsClass(Dc dc) {
    var functions = dc.functions.filter(typeof(Function));
    '''
      «copyright»
      Ext.define("«dc.canonicalNameDc»", {
        extend: "seava.lib.e4e.js.dc.AbstractDcController",
        «FOR f : functions»
          «f.define_function_tpl»
        «ENDFOR»
        
        «IF dc.source.hasFilter»
          filterModel: "«dc.source.canonicalNameFilterUiExtjs»",
        «ENDIF»
        «IF dc.source.hasParam»
          paramModel: "«dc.source.canonicalNameParamUiExtjs»",
        «ENDIF»
        recordModel: "«dc.source.canonicalNameModelUiExtjs»"
      });
    '''
  }
}
