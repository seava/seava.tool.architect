package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.view

import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component.EditableGridColumnTpl

class DcvEditableGridTpl extends AbstractDcGridViewTpl {
  
   private EditableGridColumnTpl fieldTpl = new EditableGridColumnTpl;

  override protected getExtendedClassFqn() {
    return "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid";
  }

  override protected getFieldTpl() {
    return this.fieldTpl
  }
  
}