package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*

import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.viewdc.extensions.Lov_Cfg.*

import ro.seava.tool.architect.viewdc.viewDc.Field
import ro.seava.tool.architect.viewdc.viewDc.Button
import ro.seava.tool.architect.presenter.presenter.VmParam
import ro.seava.tool.architect.viewdc.viewDc.Dc
import ro.seava.tool.architect.base.base.E_DataType
import ro.seava.tool.architect.viewdc.viewDc.E_Field
import ro.seava.tool.architect.viewdc.viewDc.IViewItem
import ro.seava.tool.architect.base.base.DataDomain
import ro.seava.tool.architect.viewdc.viewDc.Image
import ro.seava.tool.architect.viewdc.viewDc.FieldConfig
import ro.seava.tool.architect.viewdc.viewDc.EventHandler

abstract class AbstractFormControlTpl extends AbstractControlTpl {

	override def public item_tpl(IViewItem i) {
		if (i instanceof Field) {
			i.field_tpl;
		} else if (i instanceof Button) {
			(i as Button).button_tpl
		} else if (i instanceof Image) {
			i.image_tpl
		}
	}

	def protected field_tpl(IViewItem i) {
		var f = i as Field;
		var t = f.type;
		var cfg = f.config;

		if (cfg.lov != null) {
			f.lov_field_tpl
		} else if (t == E_Field.TEXT) {
			f.text_field_tpl
		} else if (t == E_Field.DATE) {
			if (cfg.isRange) {
				f.date_range_field_tpl
			} else {
				f.date_field_tpl
			}
		} else if (t == E_Field.NUMBER) {
			if (cfg.isRange) {
				f.number_range_field_tpl
			} else {
				f.number_field_tpl
			}
		} else if (t == E_Field.BOOLEAN) {
			f.boolean_field_tpl
		}
	}

	def protected button_tpl(Button btn) {
		var cfg = btn.config;
		'''.addButton({name:"«btn.name»"«cfg._config_tpl», text: "«btn.config.title»"«IF cfg.width > 0», width:«cfg.
			width»«ENDIF»})'''
	}

	def protected image_tpl(IViewItem i) {
		'''.addImage()'''
	}

	def protected text_field_tpl(Field f) {
		var DataDomain dd = f.source.dataDomain
		if (dd.staticValues.size > 0 && !f.config.isDisabled) {
			'''.addCombo({ «f._commons_tpl»«f._data_type_attributes», store:[«FOR v : dd.staticValues SEPARATOR ','»"«v»"«ENDFOR»]})'''
		} else if (f.config.multiLine) {
			'''.addTextArea({ «f._commons_tpl»«f._data_type_attributes»})'''
		} else {
			'''.addTextField({ «f._commons_tpl»«f._data_type_attributes»«IF dd.dataType.string && dd.maxLength > 0», maxLength: «dd.maxLength»«ENDIF»})'''
		}
	}

	def protected date_field_tpl(Field f) {
		'''.addDateField({«f._commons_tpl»})'''
	}

	def protected date_range_field_tpl(Field f) {
		var name = f.name
		'''
			.addDateField({name:"«name»_From", dataIndex:"«f.source.name»_From", emptyText:"From" })
			.addDateField({name:"«name»_To", dataIndex:"«f.source.name»_To", emptyText:"To" })
			.addFieldContainer({name: "«name»"«IF f.config.title != null», fieldLabel:"«f.config.title»"«ENDIF»})
			  .addChildrenTo("«f.name»",["«name»_From", "«name»_To"])
		'''
	}

	def protected number_field_tpl(Field f) {
		'''.addNumberField({«f._commons_tpl»«f._data_type_attributes»})'''
	}

	def protected number_range_field_tpl(Field f) {
		var name = f.name
		'''
			.addNumberField({name:"«name»_From", dataIndex:"«f.source.name»_From", emptyText:"From" })
			.addNumberField({name:"«name»_To", dataIndex:"«f.source.name»_To", emptyText:"To" })
			.addFieldContainer({name: "«name»"«IF f.config.title != null», fieldLabel:"«f.config.title»"«ENDIF»})
			  .addChildrenTo("«f.name»",["«name»_From", "«name»_To"])
		'''
	}

	def protected boolean_field_tpl(Field f) {
		'''.addBooleanField({ «f._commons_tpl»})'''
	}

	def protected lov_field_tpl(Field f) {
		'''
			.addLov({«f._commons_tpl»«f._data_type_attributes», xtype:"«f.config.lov.alias»"«_lov_mapping(
				f.config.filterFields, f.config.returnFields)»})
		'''
	}

	def public _commons_tpl(Field f) {
		var FieldConfig cfg = f.config
		'''name:"«f.name»"«f.bound_field_tpl»«IF cfg.isRequired», allowBlank:false«ENDIF»«IF cfg.width > 0», width:«cfg.
			width»«ENDIF»«IF cfg.height > 0», height:«cfg.height»«ENDIF»«IF cfg.noLabel», noLabel: true«ENDIF»«cfg.
			_config_tpl»'''
	}

	def protected bound_field_tpl(Field f) {
		if (f.source != null) {
			var isParam = false;
			var name = f.source.name;
			if (f.source instanceof VmParam) {
				isParam = true;
			}
			'''«IF isParam», paramIndex:"«name»", bind: "{p.«name»}"«ELSE», bind: "{d.«name»}"«ENDIF»'''
		}
	}

	def protected _data_type_attributes(Field f) {
	}

	def protected _config_tpl(FieldConfig cfg) {
		'''«cfg._config_attributes»«cfg._config_state_control_tpl»«cfg._config_listeners»'''
	}

	def protected _config_state_control_tpl(FieldConfig cfg) {
		'''«IF cfg.isDisabled», noEdit:true «ENDIF»«IF cfg.isDisabledInsert», noInsert:true«ENDIF»«IF cfg.
			isDisabledUpdate», noUpdate:true«ENDIF»«cfg._config_state_control_tpl_enabled»«cfg.
			_config_state_control_tpl_visible»'''
	}

	def protected _config_state_control_tpl_enabled(FieldConfig cfg) {
		if (cfg != null) {
			if (cfg.enableFn != null) {
				return ''', _enableFn_: this.«IF cfg.enableFn.eContainer instanceof Dc»_controller_.«ENDIF»«cfg.enableFn.
					name»'''
			}
			if (cfg.enableRule != null) {
				return ''', _enableFn_: function(dc, rec) { return «cfg.enableRule»; } '''
			}
		}
	}

	def protected _config_state_control_tpl_visible(FieldConfig cfg) {
		if (cfg != null) {
			if (cfg.visibleFn != null) {
				return ''', _visibleFn_:this.«cfg.visibleFn.name»'''
			}
			if (cfg.visibleRule != null) {
				return ''', _visibleFn_: function(dc, rec) { return «cfg.visibleRule»; } '''
			}
		}
	}

	def protected _config_attributes(FieldConfig cfg) {
		if (cfg != null && cfg.attrMap != null) {
			'''«FOR e : cfg.attrMap.entries», «e.attr.attr_print_name»:«IF e.attr.valueType == E_DataType.STRING»"«e.
				value»"«ELSE»«e.value»«ENDIF»«ENDFOR»'''
		}
	}

	def protected _config_listeners(FieldConfig cfg) {
		if (cfg != null && cfg.events.size > 0) {
			'''
			,listeners:{
			  «FOR e : cfg.events SEPARATOR ','»
			  	«e._config_listener»
			  «ENDFOR»
			}'''
		}
	}

	def protected _config_listener(EventHandler h) {
		'''«h.event.extjsName»:{scope:this, fn:«IF h.fnBody != null»function() {«h.fnBody»}«ELSE»this.«h.fnRef.name»«ENDIF»}'''
	}

}
