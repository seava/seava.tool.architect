package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.view

import ro.seava.tool.architect.viewdc.viewDc.DcView
import ro.seava.tool.architect.viewdc.viewDc.Panel
import ro.seava.tool.architect.viewdc.viewDc.Button
import ro.seava.tool.architect.viewdc.viewDc.Field

abstract class AbstractDcFormViewTpl extends AbstractDcViewTpl {

  /**
   * 
   */
  override _define_elements_tpl(DcView v) {
    ''' 
      
      /**
       * Components definition
       */
      _defineElements_: function() {
        this._getBuilder_()
          «FOR cmp : v.items.filter(typeof(Button))»
            «getFieldTpl().item_tpl(cmp)»
          «ENDFOR»
          «FOR cmp : v.items.filter(typeof(Field))»
            «getFieldTpl().item_tpl(cmp)»
          «ENDFOR»
          
          /* =========== containers =========== */
          
          «FOR cmp : v.items.filter(typeof(Panel))»
            «getPanelTpl().item_tpl(cmp)»
          «ENDFOR»      
      },
    '''
  }

  /**
   * 
   */
  override _link_elements_tpl(DcView v) {
    '''   
      
      /**
        * Combine the components
        */     
      _linkElements_: function() {
         this._getBuilder_()
           «FOR item : v.items.filter(typeof(Panel))»
             «panelTpl.children_tpl(item)»
           «ENDFOR»
      },
    '''
  }
}
