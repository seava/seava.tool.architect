package ro.seava.tool.architect.viewdc.extensions

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.viewdc.viewDc.DC_Package
import ro.seava.tool.architect.project.project.Module
import ro.seava.tool.architect.project.project.Unit
import ro.seava.tool.architect.viewdc.viewDc.Dc
import ro.seava.tool.architect.viewdc.viewDc.DcView

class Dc_Cfg {

  /* ====================== DC OWNERS ====================== */
  /**
   * Get containing package
   */
  def static DC_Package getPackage(Dc dc) {
    return dc.eContainer as DC_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(Dc dc) {
    return dc.package.module;
  }

  /**
   * Get containing unit
   */
  def static Unit getUnit(Dc dc) {
    return dc.package.unit;
  }

  /**
   * Get containing data-control
   */
  def static Dc getDc(DcView v) {
    return v.eContainer as Dc;
  }

  /* ====================== SIMPLE NAMES ====================== */
  def static String simpleNameDc(Dc dc) {
    return dc.name + "_Dc";
  }

  def static String simpleNameDcView(DcView v) {
    return v.dc.simpleNameDc + "$" + v.name;
  }

  /* ====================== ROOT PACKAGES ====================== */
  def static String getRootPackageUi(Dc dc) {
    dc.module.config.ui.rootPackage + ".ui.extjs"
  }

  def static String getRootPackageTrl(Dc dc) {
    dc.module.config.trl.rootPackage + ".i18n.extjs"
  }

  /* ====================== PACKAGES ====================== */
  //dc
  def static String getPackageGeneratedDc(Dc dc) {
    var unit = dc.unit;
    if (unit != null) {
      dc.rootPackageUi + ".generated." + unit.name + ".dc";
    } else {
      dc.rootPackageUi + ".generated.dc";
    }
  }

  def static String getPackageCustomDc(Dc dc) {
    var unit = dc.unit;
    if (unit != null) {
      dc.rootPackageUi + ".extensions." + unit.name + ".dc";
    } else {
      dc.rootPackageUi + ".extensions.dc";
    }
  }

  def static String packageGeneratedDcTrl(Dc dc) {
    var unit = dc.unit;
    if (unit != null) {
      dc.rootPackageTrl + "." + dc.module.config.refLanguage + ".generated." + unit.name + ".dc";
    } else {
      dc.rootPackageTrl + "." + dc.module.config.refLanguage + ".generated.dc";
    }
  }

  //dc-view
  def static String getPackageGeneratedDcView(DcView v) {
    var unit = v.dc.unit;
    if (unit != null) {
      v.dc.rootPackageUi + ".generated." + unit.name + ".dc";
    } else {
      v.dc.rootPackageUi + ".generated.dc";
    }
  }

  def static String getPackageCustomDcView(DcView v) {
    var unit = v.dc.unit;
    if (unit != null) {
      v.dc.rootPackageUi + ".extensions." + unit.name + ".dc";
    } else {
      v.dc.rootPackageUi + ".extensions.dc";
    }
  }

  def static String getPackageGeneratedDcViewTrl(DcView v) {
    var unit = v.dc.unit;
    if (unit != null) {
      v.dc.rootPackageTrl + "." + v.dc.module.config.refLanguage + ".generated." + unit.name + ".dc";
    } else {
      v.dc.rootPackageTrl + "." + v.dc.module.config.refLanguage + ".generated.dc";
    }
  }

  /* ====================== GENERATED CONTENT ====================== */
  def static boolean hasGeneratedDc(Dc dc) {
    return true
  }

  def static boolean hasCustomDc(Dc dc) {
    return false
  }

  def static boolean hasDc(Dc dc) {
    return dc.hasGeneratedDc || dc.hasCustomDc
  }

  def static boolean hasGeneratedDcView(DcView v) {
    return true
  }

  def static boolean hasCustomDcView(DcView v) {
    return false
  }

  def static boolean hasDcView(DcView v) {
    return v.hasGeneratedDcView || v.hasCustomDcView
  }

  /* ====================== CANONICAL NAMES ====================== */
  //dc 
  def static String canonicalNameGeneratedDc(Dc dc) {
    return dc.packageGeneratedDc + '.' + dc.simpleNameDc;
  }

  def static String canonicalNameCustomDc(Dc dc) {
    return dc.packageCustomDc + '.' + dc.simpleNameDc;
  }

  def static String canonicalNameDc(Dc dc) {
    if (dc.hasCustomDc) {
      return dc.canonicalNameCustomDc
    } else {
      return dc.canonicalNameGeneratedDc
    }
  }

  def static String canonicalNameGeneratedDcTrl(Dc dc) {
    return dc.packageGeneratedDcTrl + '.' + dc.simpleNameDc;
  }

  //dc-view
  def static String canonicalNameGeneratedDcView(DcView v) {
    return v.packageGeneratedDcView + '.' + v.simpleNameDcView;
  }

  def static String canonicalNameCustomDcView(DcView v) {
    return v.packageCustomDcView + '.' + v.simpleNameDcView;
  }

  def static String canonicalNameDcView(DcView v) {
    if (v.hasCustomDcView) {
      return v.canonicalNameCustomDcView
    } else {
      return v.canonicalNameGeneratedDcView
    }
  }

  def static String canonicalNameGeneratedDcViewTrl(DcView v) {
    return v.packageGeneratedDcViewTrl + '.' + v.simpleNameDcView;
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  // dc
  def static String fileLocationGeneratedDc(Dc dc) {
    return dc.module.config.ui.path + "/src/main/resources/public/" + dc.canonicalNameGeneratedDc.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedDcDpd(Dc dc) {
    return dc.module.config.ui.path + "/src/main/resources/public/" + dc.canonicalNameGeneratedDc.pathFromFQN +
      ".jsdp"
  }

  def static String fileLocationGeneratedDcTrl(Dc dc) {
    return dc.module.config.trl.path + "/src/main/resources/public/" +
      dc.canonicalNameGeneratedDcTrl.pathFromFQN + ".js"
  }

  def static String fileLocationCustomDc(Dc dc) {
    return dc.module.config.ui.path + "/src/main/resources/public/" + dc.canonicalNameCustomDc.pathFromFQN + ".js"
  }

  def static String fileLocationCustomDcDpd(Dc dc) {
    return dc.module.config.ui.path + "/src/main/resources/public/" + dc.canonicalNameCustomDc.pathFromFQN + ".jsdp"
  }

  // dc-view
  def static String fileLocationGeneratedDcView(DcView v) {
    return v.dc.module.config.ui.path + "/src/main/resources/public/" + v.canonicalNameGeneratedDcView.pathFromFQN +
      ".js"
  }

  def static String fileLocationGeneratedDcViewDpd(DcView v) {
    return v.dc.module.config.ui.path + "/src/main/resources/public/" + v.canonicalNameGeneratedDcView.pathFromFQN +
      ".jsdp"
  }

  def static String fileLocationCustomDcView(DcView v) {
    return v.dc.module.config.ui.path + "/src/main/resources/public/" + v.canonicalNameCustomDcView.pathFromFQN +
      ".js"
  }

  def static String fileLocationCustomDcViewDpd(DcView v) {
    return v.dc.module.config.ui.path + "/src/main/resources/public/" + v.canonicalNameCustomDcView.pathFromFQN +
      ".jsdp"
  }

  def static String fileLocationGeneratedDcViewTrl(DcView v) {
    return v.dc.module.config.trl.path + "/src/main/resources/public/" +
      v.canonicalNameGeneratedDcViewTrl.pathFromFQN + ".js"
  }

}
