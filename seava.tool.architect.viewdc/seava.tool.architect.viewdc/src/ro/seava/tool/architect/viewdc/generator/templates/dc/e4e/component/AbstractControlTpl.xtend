package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component

import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*

import ro.seava.tool.architect.viewdc.viewDc.LovFieldFilterMapping
import java.util.List
import ro.seava.tool.architect.viewdc.viewDc.LovFieldFieldMapping

abstract class AbstractControlTpl extends AbstractComponentTpl {

  def protected _lov_mapping(List<LovFieldFilterMapping> filterFields, List<LovFieldFieldMapping> returnFields) {
    '''
    «IF returnFields != null && returnFields.size > 0»,
      retFieldMapping: [«FOR rf : returnFields SEPARATOR ','»«rf._lov_field_mapping»«ENDFOR»]«ENDIF»«IF filterFields !=
      null && filterFields.size > 0»,
      filterFieldMapping: [«FOR ff : filterFields SEPARATOR ','»«ff._lov_field_mapping» «ENDFOR»]«ENDIF»'''
  }

  /**
   * for the filter form is overriden in Filter_Form_FieldTpl.xtend
   */
  def protected _lov_field_mapping(LovFieldFieldMapping rf) {
    '''{lovField:"«rf.lovField.name»", «IF rf.valueField.isParam()»dsParam«ELSE»dsField«ENDIF»: "«rf.valueField.name»"} '''
  }

  def protected _lov_field_mapping(LovFieldFilterMapping rf) {
    var dsKey = "dsField";
    if (rf.valueField.isParam) {
      dsKey = "dsParam";
    }
    var lovKey = "lovField";
    if (rf.lovField.isParam) {
      lovKey = "lovParam";
    }

    '''{«lovKey»:"«rf.lovField.name»", «IF rf.staticValue != null»value: "«rf.staticValue»"«ELSE»«dsKey»: "«rf.
      valueField.name»"«ENDIF»«IF rf.isRequired», notNull: true«ENDIF»}'''
  }
}
