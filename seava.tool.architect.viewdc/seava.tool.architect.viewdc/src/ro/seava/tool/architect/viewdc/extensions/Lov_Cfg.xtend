package ro.seava.tool.architect.viewdc.extensions

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.viewdc.viewDc.DC_Package
import ro.seava.tool.architect.viewdc.viewDc.Lov
import ro.seava.tool.architect.project.project.Module
import ro.seava.tool.architect.project.project.Unit

class Lov_Cfg {

  /* ====================== DC OWNERS ====================== */
  /**
   * Get containing package
   */
  def static DC_Package getPackage(Lov lov) {
    return lov.eContainer as DC_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(Lov lov) {
    return lov.package.module;
  }

  /**
   * Get containing unit
   */
  def static Unit getUnit(Lov lov) {
    return lov.package.unit;
  }

  /* ====================== SIMPLE NAMES ====================== */
  def static String simpleNameLov(Lov lov) {
    return lov.name + "_Lov";
  }

  /* ====================== ROOT PACKAGES ====================== */
  def static String getRootPackageUi(Lov lov) {
    lov.module.config.ui.rootPackage + ".ui.extjs"
  }

  def static String getRootPackageTrl(Lov lov) {
    lov.module.config.trl.rootPackage + ".i18n.extjs"
  }

  /* ====================== PACKAGES ====================== */
  def static String getPackageGeneratedLov(Lov lov) {
    var unit = lov.unit;
    if (unit != null) {
      lov.rootPackageUi + ".generated." + unit.name + ".lov";
    } else {
      lov.rootPackageUi + ".generated.lov";
    }
  }

  def static String getPackageCustomLov(Lov lov) {
    var unit = lov.unit;
    if (unit != null) {
      lov.rootPackageUi + ".extensions." + unit.name + ".lov";
    } else {
      lov.rootPackageUi + ".extensions.lov";
    }
  }

  def static String packageGeneratedLovTrl(Lov lov) {
    var unit = lov.unit;
    if (unit != null) {
      lov.rootPackageTrl + "." + lov.module.config.refLanguage + ".generated." + unit.name + ".lov";
    } else {
      lov.rootPackageTrl + "." + lov.module.config.refLanguage + ".generated.lov";
    }
  }

  /* ====================== GENERATED CONTENT ====================== */
  def static boolean hasGeneratedLov(Lov lov) {
    return true
  }

  def static boolean hasCustomLov(Lov lov) {
    return false
  }

  def static boolean hasLov(Lov lov) {
    return lov.hasGeneratedLov || lov.hasCustomLov
  }

  /* ====================== CANONICAL NAMES ====================== */
  def static String alias(Lov lov) {
    return lov.canonicalNameLov
  }

  def static String canonicalNameGeneratedLov(Lov lov) {
    return lov.packageGeneratedLov + '.' + lov.simpleNameLov;
  }

  def static String canonicalNameCustomLov(Lov lov) {
    return lov.packageCustomLov + '.' + lov.simpleNameLov;
  }

  def static String canonicalNameLov(Lov lov) {
    if (lov.hasCustomLov) {
      return lov.canonicalNameCustomLov
    } else {
      return lov.canonicalNameGeneratedLov
    }
  }

  def static String canonicalNameGeneratedLovTrl(Lov lov) {
    return lov.packageGeneratedLovTrl + '.' + lov.simpleNameLov;
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  def static String fileLocationGeneratedLov(Lov lov) {
    return lov.module.config.ui.path + "/src/main/resources/public/" + lov.canonicalNameLov.pathFromFQN + ".js"
  }

  def static String fileLocationGeneratedLovDpd(Lov lov) {
    return lov.module.config.ui.path + "/src/main/resources/public/" + lov.canonicalNameLov.pathFromFQN + ".jsdp"
  }

  def static String fileLocationGeneratedLovTrl(Lov lov) {
    return lov.module.config.trl.path + "/src/main/resources/public/" +
      lov.canonicalNameGeneratedLovTrl.pathFromFQN + ".js"
  }

}
