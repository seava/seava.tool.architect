package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.view

import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component.EditFormFieldTpl

class DcvEditFormTpl extends AbstractDcFormViewTpl {

  private EditFormFieldTpl fieldTpl = new EditFormFieldTpl;

  override protected getExtendedClassFqn() {
    return "seava.lib.e4e.js.dc.view.AbstractDcvEditForm";
  }

  override protected getFieldTpl() {
    return this.fieldTpl
  }

}
