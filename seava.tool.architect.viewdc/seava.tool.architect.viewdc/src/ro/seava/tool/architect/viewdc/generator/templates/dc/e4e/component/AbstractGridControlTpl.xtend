package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.viewdc.extensions.Dc_Ext.*
import static extension ro.seava.tool.architect.viewdc.extensions.Lov_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*

import ro.seava.tool.architect.viewdc.viewDc.IViewItem
import ro.seava.tool.architect.viewdc.viewDc.Field
import ro.seava.tool.architect.viewdc.viewDc.E_Field
import ro.seava.tool.architect.base.base.E_DataType
import ro.seava.tool.architect.viewdc.viewDc.Button
import ro.seava.tool.architect.viewdc.viewDc.Image
import ro.seava.tool.architect.base.base.DataDomain

class AbstractGridControlTpl extends AbstractControlTpl {

  override def public item_tpl(IViewItem i) {
    if (i instanceof Field) {
      i.field_tpl;
    } else if (i instanceof Button) {
      i.button_tpl
    } else if (i instanceof Image) {
      i.image_tpl
    }
  }

  def protected field_tpl(IViewItem i) {
    var f = i as Field;
    var t = f.type;

    if (t == E_Field.TEXT) {
      f.text_column_tpl
    } else if (t == E_Field.DATE) {
      f.date_column_tpl
    } else if (t == E_Field.NUMBER) {
      f.number_column_tpl
    } else if (t == E_Field.BOOLEAN) {
      f.boolean_column_tpl
    }
  }

  def protected button_tpl(IViewItem i) {
    ''''''
  }

  def protected image_tpl(IViewItem i) {
    ''''''
  }

  /**
   * 
   */
  def protected text_column_tpl(Field f) {
    var DataDomain dd = f.source.dataDomain
    var editable = f.editable
    if (editable && dd.staticValues.size > 0) {
      '''.addComboColumn({ «f._commons_tpl»«f.text_column_editor_tpl»})'''
    } else if (editable && f.config.lov != null) {
      '''.addLov({ «f._commons_tpl»«f.text_column_editor_tpl»})'''
    } else {
      '''.addTextColumn({ «f._commons_tpl»«f.text_column_editor_tpl»})'''
    }
  }

  /**
   * 
   */
  def protected text_column_editor_tpl(Field f) {
    if (f.editable) {
      ''', editor: {«f._commons_editor_tpl»}'''
    }
  }

  /**
   * 
   */
  def protected date_column_tpl(Field f) {
    '''.addDateColumn({«f._commons_tpl»«f.date_column_editor_tpl»})'''
  }

  /**
   * 
   */
  def protected date_column_editor_tpl(Field f) {
    if (f.editable) {
      ''', editor: {«f._commons_editor_tpl»}'''
    }
  }

  /**
   * 
   */
  def protected number_column_tpl(Field f) {
    '''.addNumberColumn({«f._commons_tpl»«f.number_column_editor_tpl»})'''
  }

  /**
   * 
   */
  def protected number_column_editor_tpl(Field f) {
    if (f.editable) {
      ''', editor: {«f._commons_editor_tpl»}'''
    }
  }

  /**
   * 
   */
  def protected boolean_column_tpl(Field f) {
    '''.addBooleanColumn({ «f._commons_tpl»«f.boolean_column_editor_tpl»})'''
  }

  /**
   * 
   */
  def protected boolean_column_editor_tpl(Field f) {
    ''''''
  }

  /**
   * 
   */
  def protected _commons_editor_tpl(Field f) {
    '''«f._editor_base_tpl»«IF f.config.isRequired», allowBlank:false«ENDIF»«IF f.config.isDisabledInsert», noInsert:true«ENDIF»«IF f.
      config.isDisabledUpdate», noUpdate:true«ENDIF»«f._format_masks_tpl»'''
  }

  /**
   * 
   */
  def protected _editor_base_tpl(Field f) {
    if (f.config.lov != null) {
      '''xtype: "«f.config.lov.alias»"«_lov_mapping(f.config.filterFields, f.config.returnFields)»'''
    } else {
      if (f.type == E_Field.TEXT) {
        var DataDomain dd = f.source.dataDomain
        if (dd.staticValues.size > 0) {
          '''xtype: "combo", store:[«FOR v : dd.staticValues SEPARATOR ','»"«v»"«ENDFOR»]'''
        } else {
          '''xtype: "textfield"'''
        }
      } else if (f.type == E_Field.DATE) {
        '''xtype: "datefield"'''
      } else if (f.type == E_Field.NUMBER) {
        '''xtype: "numberfield"'''
      }
    }
  }

  /**
   * 
   */
  def protected _commons_tpl(Field f) {
    var CharSequence _tpl = ''''''
    if (f.source != null) {
      _tpl = ''', dataIndex:"«f.source.name»"'''
    } else {
      _tpl = ''', sortable:false'''
    }
    var cfg = f.config;

    return '''name:"«f.name»"«_tpl»«IF cfg.width > 0», width:«cfg.width»«ENDIF»«IF cfg.notVisible», hidden:true«ENDIF»«IF cfg.
      isDisabled», noEdit:true «ENDIF»«IF cfg.isDisabledInsert», noInsert:true«ENDIF»«IF cfg.isDisabledUpdate», noUpdate:true«ENDIF»«f.
      _format_masks_tpl»«f._commons_attributes»'''
  }

  /**
   * 
   */
  def protected _format_masks_tpl(Field f) {
    var dt = f.source.dataType
    if (dt.date) {
      var dtName = dt.name
      if (dtName.matches("Date")) {
        ''', _mask_: Masks.DATE'''
      } else if (dtName.matches("Time")) {
        ''', _mask_: Masks.TIME'''
      } else {
        ''', _mask_: Masks.DATETIME'''
      }
    }
  }

  /**
   * 
   */
  def protected _commons_attributes(Field f) {
    if (f.config != null && f.config.attrMap != null) {
      '''«FOR e : f.config.attrMap.entries», «e.attr.attr_print_name»:«IF e.attr.valueType == E_DataType.STRING»"«e.
        value»"«ELSE»«e.value»«ENDIF»«ENDFOR»'''
    }
  }

}
