package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component

import ro.seava.tool.architect.viewdc.viewDc.Panel
import ro.seava.tool.architect.viewdc.viewDc.IContainer
import ro.seava.tool.architect.viewdc.viewDc.Window
import ro.seava.tool.architect.viewdc.viewDc.PanelConfig

abstract class AbstractContainerTpl extends AbstractComponentTpl {

  /**
   * 
   */
  def CharSequence children_tpl(IContainer c) {
    if (c instanceof Window) {
      '''.addChildrenTo("«c.name»", ["«(c as Window).item.name»"])'''
    } else if (c instanceof Panel) {
      '''.addChildrenTo("«c.name»", [«FOR item : (c as Panel).items SEPARATOR ", "»"«item.name»"«ENDFOR» ])'''
    }
  }

  /**
   * 
   */
  def protected _width_tpl(IContainer c) {
    if (c.config.width > 0) {
      if (c.config.isWidthPercent) {
        ''', width:"«c.config.width»%"'''
      } else {
        ''', width:«c.config.width»'''
      }
    } else {
      c._default_width_tpl
    }
  }

  /**
   * 
   */
  def protected _default_width_tpl(IContainer c) {
    //    if (!c.name.equalsIgnoreCase("main")) {
    //      if ((p instanceof FormPanel) && ( (p as FormPanel).layout == FormPanelLayout.VERTICAL)) {
    //        if (c.eContainer instanceof DcFilterView) {
    //          ''', width:210'''
    //        } else if (c.eContainer instanceof DcEditView) {
    //          ''', width:250'''
    //        }
    //      }
    //    }
  }

  /**
   * 
   */
  def protected _height_tpl(IContainer c) {
    if (c.config.height > 0) {
      if (c.config.isHeightPercent) {
        ''', height: "«c.config.height»%"'''
      } else {
        ''', height: «c.config.height»'''
      }
    } else {
      c._default_height_tpl
    }
  }

  /**
   * 
   */
  def protected _default_height_tpl(IContainer c) {
  }

  def protected _buttons(PanelConfig pc) {
    var btns = pc.buttons;
    if (btns.size > 0) {
      ''', buttons: [«FOR btn : btns SEPARATOR ','»this._getConfig_("«btn.name»")«ENDFOR»]'''
    }
  }

}
