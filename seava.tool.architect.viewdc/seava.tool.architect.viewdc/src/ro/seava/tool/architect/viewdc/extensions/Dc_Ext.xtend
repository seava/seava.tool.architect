package ro.seava.tool.architect.viewdc.extensions

import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import ro.seava.tool.architect.viewdc.viewDc.DcView
import ro.seava.tool.architect.viewdc.viewDc.E_DcView
import ro.seava.tool.architect.viewdc.viewDc.Dc
import java.util.List
import java.util.ArrayList
import ro.seava.tool.architect.viewdc.viewDc.Field

class Dc_Ext extends Dc_Cfg {

  def static boolean isEditable(Field f) {
    return ((!(f.eContainer as DcView).readOnly ) && !f.config.isDisabled);
  }

  def static String xtype(DcView v) {
    return v.dc.module.name + "_" + v.simpleNameDcView;
  }

  def static boolean isGrid(DcView v) {
    return v.type == E_DcView.GRID;
  }

  def static boolean isForm(DcView v) {
    return v.type == E_DcView.FORM;
  }

  def static boolean isPropGrid(DcView v) {
    return v.type == E_DcView.PROPGRID;
  }

  def static List<String> collect_dependencies(Dc dc) {
    var List<String> list = new ArrayList<String>();

    var ds = dc.source;

    // Add DS
    list.add(ds.canonicalNameModelUiExtjs);
    if (ds.hasParam) {
      list.add(ds.canonicalNameParamUiExtjs);
    }

    //    // collect LOVs used in edit-forms
    //    for (v : dc.views.filter(typeof(DcEditView))) {
    //      for (lf : v.items.filter(typeof(F_LovField))) {
    //        var key = (lf.lov.eContainer as DC_Package).module.genConfig.ui.parent.artifactId + "/lov/" +
    //          lf.lov.canonicalName;
    //        if (!list.contains(key)) {
    //          list.add(key);
    //        }
    //      }
    //    }
    //
    //    // collect LOVs used in filter-forms
    //    for (v : dc.views.filter(typeof(DcFilterView))) {
    //      for (lf : v.items.filter(typeof(F_LovField))) {
    //        var key = (lf.lov.eContainer as DC_Package).module.genConfig.ui.parent.artifactId + "/lov/" +
    //          lf.lov.canonicalName;
    //        if (!list.contains(key)) {
    //          list.add(key);
    //        }
    //      }
    //    }
    //
    //    // collect LOVs used in edit-grids
    //    for (v : dc.views.filter(typeof(DcEditGridView))) {
    //      for (lf : v.items.filter(typeof(EGC_Lov))) {
    //        var key = (lf.lov.eContainer as DC_Package).module.genConfig.ui.parent.artifactId + "/lov/" +
    //          lf.lov.canonicalName;
    //        if (!list.contains(key)) {
    //          list.add(key);
    //        }
    //      }
    //    }
    return list
  }

}
