package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*

import ro.seava.tool.architect.abstracts.templates.Abstract_Tpl
import ro.seava.tool.architect.viewdc.viewDc.Function
import java.util.List
import ro.seava.tool.architect.viewdc.viewDc.AttrMap
import ro.seava.tool.architect.base.base.E_DataType

class Abstract_Dc_Tpl extends Abstract_Tpl {

  def protected define_functions_tpl(List<Function> functions) {
    '''      
      «FOR f : functions»
        «f.define_function_tpl»
      «ENDFOR»
    '''
  }

  def protected define_function_tpl(Function f) {
    '''
      
      «f.name»: function(«FOR a : f.args SEPARATOR ','»«a»«ENDFOR») {
        «f.body»
      },
    '''
  }

  /**
   * 
   */
  def protected attribute_set_tpl(AttrMap attrMap, boolean initialSeparator) {
    if (attrMap != null) {
      '''«IF initialSeparator», «ENDIF»«FOR at : attrMap.entries SEPARATOR ','» «at.attr.attr_print_name»:«IF at.
        attr.valueType == E_DataType.STRING»"«at.value»"«ELSE»«at.value»«ENDIF»«ENDFOR»'''
    }
  }
}
