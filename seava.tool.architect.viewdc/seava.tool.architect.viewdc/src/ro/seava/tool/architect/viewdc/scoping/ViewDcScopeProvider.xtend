/*
 * generated by Xtext
 */
package ro.seava.tool.architect.viewdc.scoping

import static extension ro.seava.tool.architect.base.scoping.BaseScopeUtils.*
import static extension ro.seava.tool.architect.presenter.scoping.PresenterScopeUtils.*

import ro.seava.tool.architect.viewdc.viewDc.Field
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.scoping.IScope
import ro.seava.tool.architect.viewdc.viewDc.Dc
import org.eclipse.xtext.scoping.Scopes
import ro.seava.tool.architect.viewdc.viewDc.LovField
import ro.seava.tool.architect.viewdc.viewDc.Lov
import ro.seava.tool.architect.presenter.presenter.IVmField
import ro.seava.tool.architect.viewdc.viewDc.FieldConfig
import ro.seava.tool.architect.viewdc.viewDc.Panel
import ro.seava.tool.architect.viewdc.viewDc.PanelConfig
import ro.seava.tool.architect.presenter.presenter.IVmMember
import ro.seava.tool.architect.viewdc.viewDc.DcView
import ro.seava.tool.architect.viewdc.viewDc.E_DcView
import ro.seava.tool.architect.base.base.E_AttrTarget

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 *
 */
class ViewDcScopeProvider extends org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider {

  def IScope scope_Field_source(Field ctx, EReference reference) {
    return Scopes.scopeFor((ctx.eContainer.eContainer as Dc).source.collect_members(IVmMember));
  }

  // lov
  def IScope scope_LovField_source(LovField ctx, EReference reference) {
    return Scopes.scopeFor((ctx.eContainer as Lov).source.collect_members(IVmField));
  }

  def IScope scope_LovFieldFieldMapping_lovField(FieldConfig ctx, EReference ref) {
    return Scopes.scopeFor((ctx.lov as Lov).source.collect_members(IVmField));
  }

  def IScope scope_LovFieldFilterMapping_lovField(FieldConfig ctx, EReference ref) {
    return Scopes.scopeFor((ctx.lov as Lov).source.collect_members(IVmMember));
  }

  def IScope scope_LovFieldFieldMapping_valueField(FieldConfig ctx, EReference ref) {
    return Scopes.scopeFor((ctx.eContainer.eContainer.eContainer as Dc).source.collect_members(IVmMember));
  }

  def IScope scope_LovFieldFilterMapping_valueField(FieldConfig ctx, EReference ref) {
    return Scopes.scopeFor((ctx.eContainer.eContainer.eContainer as Dc).source.collect_members(IVmMember));
  }

  // attributes
  def IScope scope_Attr(Field ctx, EReference ref) {
    return delegate.getScope(ctx, ref).filtered_Attr_scope(ctx.type.literal)
  }

  def IScope scope_Attr(PanelConfig ctx, EReference ref) {
    return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.PANEL.literal)
  }

  def IScope scope_Attr(Panel ctx, EReference ref) {
    return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.ABSTRACT_ITEM.literal)
  }

  def IScope scope_Attr(Dc ctx, EReference ref) {
    return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.DC.literal);
  }

  def IScope scope_Attr(DcView ctx, EReference ref) {
    if (ctx.type == E_DcView.FORM) {
      if (ctx.forFilter) {
        return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.FILTER_FORM_VIEW.literal)
      } else {
        return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.EDIT_FORM_VIEW.literal)
      }
    } else if (ctx.type == E_DcView.PROPGRID) {
      if (ctx.forFilter) {
        return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.FILTER_PROP_VIEW.literal)
      } else {
        return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.EDIT_PROP_VIEW.literal)
      }
    } else if (ctx.type == E_DcView.GRID) {
      if (ctx.readOnly) {
        return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.GRID_VIEW.literal)
      } else {
        return delegate.getScope(ctx, ref).filtered_Attr_scope(E_AttrTarget.EDITABLE_GRID_VIEW.literal)
      }
    }

  }

}
