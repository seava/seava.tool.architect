package ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.component

import ro.seava.tool.architect.viewdc.viewDc.IViewItem
import ro.seava.tool.architect.viewdc.generator.templates.dc.e4e.Abstract_Dc_Tpl

abstract class AbstractComponentTpl extends Abstract_Dc_Tpl {

  abstract def CharSequence item_tpl(IViewItem i);

}
