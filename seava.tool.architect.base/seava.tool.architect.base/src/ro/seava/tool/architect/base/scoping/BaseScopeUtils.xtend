package ro.seava.tool.architect.base.scoping

import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.impl.FilteringScope
import org.eclipse.emf.ecore.EObject
import ro.seava.tool.architect.base.base.ApiMethod

class BaseScopeUtils {

  def static IScope filtered_Attr_scope(IScope source, String type) {

    return new FilteringScope(source,
      [ iod |
        {
          var String target = iod.getUserData("target");

          //System.out.println("target=" + target);
          if (target != null && target.matches(type)) {
            return true;
          }

          //          if (false) {
          //            var EObject e = iod.getEObjectOrProxy();
          //            var boolean isProxy = e.eIsProxy
          //            if (e instanceof Attr) {
          //              var String t = (e as Attr).target.literal;
          //              if (t != null) {
          //                System.out.println(t.matches(type));
          //                return t.matches(type);
          //              }
          //            }
          //          }
          return false;
        }
      ])
  }

  def static IScope filtered_ApiMethod_scope(IScope source, String type) {
    return new FilteringScope(source,
      [ iod |
        {
          var EObject e = iod.getEObjectOrProxy();
          if (e instanceof ApiMethod) {
            var String t = (e as ApiMethod).type;
            if (t != null) {
              return t.matches(type);
            }
          }

          var String t = iod.getUserData("type");
          if (t != null && t.matches(type)) {
            return true;
          }
          return false;
        }
      ])
  }

}
