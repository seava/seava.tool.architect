package ro.seava.tool.architect.base.my;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.util.IAcceptor;

import ro.seava.tool.architect.base.base.ApiMethod;
import ro.seava.tool.architect.base.base.Attr;

public class MyResourceDescriptionStrategy extends
		DefaultResourceDescriptionStrategy {

	private final static Logger LOG = Logger
			.getLogger(MyResourceDescriptionStrategy.class);

	/**
	 * Create custom object descriptors
	 */
	public boolean createEObjectDescriptions(EObject eObject,
			IAcceptor<IEObjectDescription> acceptor) {
		if (eObject instanceof ApiMethod) {
			return this.descriptionForApiMethod((ApiMethod) eObject, acceptor);
		} else if (eObject instanceof Attr) {
			return this.descriptionForAttr((Attr) eObject, acceptor);
		}
		return super.createEObjectDescriptions(eObject, acceptor);
	}

	/**
	 * Export object description for Attr instances
	 * 
	 * @param t
	 * @param acceptor
	 * @return
	 */
	private boolean descriptionForAttr(Attr t,
			IAcceptor<IEObjectDescription> acceptor) {
		if (getQualifiedNameProvider() == null)
			return false;
		try {
			QualifiedName qualifiedName = getQualifiedNameProvider()
					.getFullyQualifiedName(t);
			if (qualifiedName != null) {
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("target", t.getTarget().getLiteral());
				acceptor.accept(EObjectDescription.create(qualifiedName, t,
						userData));
			}
		} catch (Exception exc) {
			LOG.error(exc.getMessage());
		}
		return true;
	}

	/**
	 * Export object description for ApiMethod instances
	 * 
	 * @param t
	 * @param acceptor
	 * @return
	 */
	private boolean descriptionForApiMethod(ApiMethod t,
			IAcceptor<IEObjectDescription> acceptor) {
		if (getQualifiedNameProvider() == null)
			return false;
		try {
			QualifiedName qualifiedName = getQualifiedNameProvider()
					.getFullyQualifiedName(t);
			if (qualifiedName != null) {
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("type", t.getType());
				acceptor.accept(EObjectDescription.create(qualifiedName, t,
						userData));
			}
		} catch (Exception exc) {
			LOG.error(exc.getMessage());
		}
		return true;
	}

}
