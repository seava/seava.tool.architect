/*
 * generated by Xtext
 */
package ro.seava.tool.architect.base.formatting

import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig

import com.google.inject.Inject;
import ro.seava.tool.architect.base.services.BaseGrammarAccess

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
class BaseFormatter extends AbstractDeclarativeFormatter {

  @Inject extension BaseGrammarAccess

  override protected void configureFormatting(FormattingConfig c) {
    c.setAutoLinewrap(100)

    // comments
    c.setLinewrap(0, 1, 2).before(SL_COMMENTRule)
    c.setLinewrap(0, 1, 2).after(SL_COMMENTRule)
    c.setLinewrap(1, 1, 2).before(ML_COMMENTRule)
    c.setLinewrap(1, 1, 2).after(ML_COMMENTRule)

    // elements
    c.format_package
    c.format_data_type
    c.format_data_domain
    c.format_built_in_var
    c.format_action

    c.setLinewrap(1, 1, 2).before(eventRule)
    c.setLinewrap(1, 1, 2).before(styleClassRule)
    c.setLinewrap(1, 1, 2).before(attrRule)
    c.setLinewrap(1, 1, 2).before(enableRuleRule)
    c.setLinewrap(1, 1, 2).before(apiMethodRule)
  }

  /**
   * Format action related elements
   */
  def private format_action(FormattingConfig c) {

    //action-group
    c.setLinewrap(1, 1, 2).before(actionGroupRule)

    //action definition
    c.setLinewrap(1, 1, 2).before(actionDefRule)
    c.setIndentationIncrement.after(actionDefAccess.actionDefinitionKeyword_0)
    c.setIndentationDecrement.before(actionDefAccess.semicolonKeyword_9)
    c.setLinewrap(1).after(actionDefAccess.leftCurlyBracketKeyword_8_0)
  }

  /**
   * Format built-in-variable definitions
   */
  def private format_built_in_var(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(builtInVarRule)
    c.setIndentationIncrement.after(builtInVarAccess.builtInVarKeyword_0)
    c.setIndentationDecrement.before(builtInVarAccess.semicolonKeyword_6)

    c.setLinewrap(1, 1, 2).before(builtInVarAccess.javaExpressionKeyword_2)
    c.setLinewrap(1, 1, 2).before(builtInVarAccess.extjsExpressionKeyword_4)
  }

  /**
   * Format data domains
   */
  def private format_data_domain(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(dataDomainRule)
    c.setIndentationIncrement.after(dataDomainAccess.dataDomainKeyword_0)
    c.setIndentationDecrement.before(dataDomainAccess.semicolonKeyword_9)

    c.setLinewrap(1).before(dataDomainAccess.staticValuesKeyword_7_0)
  }

  /**
   * Format data types
   */
  def private format_data_type(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).before(dataTypeRule)
    c.setIndentationIncrement.after(dataTypeAccess.dataTypeKeyword_0)
    c.setIndentationDecrement.before(dataTypeAccess.semicolonKeyword_14)

    c.setLinewrap(1, 1, 2).before(dataTypeAccess.javaTypeKeyword_4)
    c.setLinewrap(1, 1, 2).before(dataTypeAccess.extjsTypeKeyword_6)
    c.setLinewrap(1, 1, 2).before(dataTypeAccess.mysqlTypeKeyword_11)
    c.setLinewrap(1, 1, 2).before(dataTypeAccess.oracleTypeKeyword_8)
  }

  /**
   * Format package declaration
   */
  def private format_package(FormattingConfig c) {
    c.setLinewrap(1, 1, 2).after(importRule)

    c.setLinewrap(2).after(t_PackageAccess.leftCurlyBracketKeyword_2)
    c.setIndentationIncrement.after(t_PackageAccess.leftCurlyBracketKeyword_2)
    c.setIndentationDecrement.before(t_PackageAccess.rightCurlyBracketKeyword_5)
    c.setLinewrap(2).before(t_PackageAccess.rightCurlyBracketKeyword_5)
  }
}
