package ro.seava.tool.architect.base.extensions

import ro.seava.tool.architect.base.base.Attr
import ro.seava.tool.architect.base.base.DataType
import ro.seava.tool.architect.base.base.E_DataType

class Base_Ext {

  def static boolean isString(DataType d) {
    return d.type == E_DataType.STRING;
  }

  def static boolean isNumber(DataType d) {
    return d.type == E_DataType.NUMBER;
  }

  def static boolean isDate(DataType d) {
    return d.type == E_DataType.DATE;
  }

  def static boolean isBoolean(DataType d) {
    return d.type == E_DataType.BOOLEAN;
  }

  def static boolean isLob(DataType d) {
    return d.type == E_DataType.LOB;
  }

  def static attr_print_name(Attr a) {
    if (a.propertyName != null) {
      return a.propertyName
    } else {
      return a.name
    }
  }
}
