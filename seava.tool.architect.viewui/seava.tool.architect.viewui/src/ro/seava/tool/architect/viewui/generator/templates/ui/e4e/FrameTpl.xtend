package ro.seava.tool.architect.viewui.generator.templates.ui.e4e

import static extension ro.seava.tool.architect.viewui.extensions.Frame_Ext.*
import static extension ro.seava.tool.architect.viewdc.extensions.Dc_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*

import ro.seava.tool.architect.viewui.viewUi.Ui
import ro.seava.tool.architect.abstracts.templates.Abstract_Tpl
import ro.seava.tool.architect.viewui.viewUi.UiDc
import ro.seava.tool.architect.viewui.viewUi.UiDcLink
import ro.seava.tool.architect.viewui.viewUi.AttrMap
import ro.seava.tool.architect.base.base.E_DataType
import ro.seava.tool.architect.viewui.viewUi.UiButton
import ro.seava.tool.architect.viewui.viewUi.IUiView
import ro.seava.tool.architect.viewui.viewUi.Panel
import ro.seava.tool.architect.viewui.viewUi.UiDcView
import ro.seava.tool.architect.viewui.viewUi.IStatement
import ro.seava.tool.architect.viewui.viewUi.JsStatement
import ro.seava.tool.architect.viewui.viewUi.CallFnStatement
import ro.seava.tool.architect.viewui.viewUi.CallRpcStatement
import ro.seava.tool.architect.viewui.viewUi.InvokeMethodStatement
import ro.seava.tool.architect.viewui.viewUi.OpenUiStatement
import ro.seava.tool.architect.presenter.presenter.IVmField
import ro.seava.tool.architect.viewui.viewUi.UiWindow
import ro.seava.tool.architect.presenter.presenter.Rpc
import ro.seava.tool.architect.viewui.viewUi.CodeBlock
import ro.seava.tool.architect.viewui.viewUi.E_Layout
import ro.seava.tool.architect.viewui.viewUi.UiMenu
import ro.seava.tool.architect.viewui.viewUi.Function
import ro.seava.tool.architect.presenter.presenter.E_Rpc
import ro.seava.tool.architect.viewui.viewUi.OpenAsgnStatement
import ro.seava.tool.architect.presenter.presenter.AsgnField
import ro.seava.tool.architect.presenter.presenter.AsgnParam
import ro.seava.tool.architect.viewui.viewUi.EventHandler
import ro.seava.tool.architect.viewui.viewUi.Action

class FrameTpl extends Abstract_Tpl {

  def CharSequence doDependencyFile(Ui ui) {
    '''
      [
        «FOR i : ui.collect_dependencies.sort SEPARATOR ', '»
          "«i»"
        «ENDFOR»
      ]
    '''
  }

  def CharSequence doJsClass(Ui ui) {
    '''
      «copyright»
      Ext.define("«ui.canonicalNameGeneratedUi»", {
        
        /**
         * Data-controls definition
         */
        _defineDcs_: function() {
          this._getBuilder_()
            «ui.define_dcs_tpl»;
        },
        
        /**
         * Components definition
         */
        _defineElements_: function() {
          this._getBuilder_()
            «ui.define_elements_tpl»;
            «IF ui.withToc»
              this._mainViewName_  = "_main_with_toc_";
            «ENDIF»
        },
        
        /**
         * Combine the components
         */
        _linkElements_: function() {
          this._getBuilder_()
            «ui.link_elements_tpl»;
        },
        
        /**
         * Create toolbars
         */
        _defineToolbars_: function() {
          this._getBuilder_()
            «ui.define_toolbars_tpl»;
        },
        «FOR b : ui.buttons.filter(e|e.handlerCodeBlock != null)»
          «b.button_handler_tpl»
        «ENDFOR»
        «FOR f : ui.functions»
          «f.function_tpl»
        «ENDFOR»
        «FOR ev : ui.events»
          «ev.event_handler_tpl»
        «ENDFOR»
        
        extend: "seava.lib.e4e.js.frame.AbstractFrame",
        alias: "widget.«ui.simpleNameUi»"
      });
    '''
  }

  /* ================================   DEFINE TOOLBARS ================================  */
  def protected define_toolbars_tpl(Ui ui) {
    '''«FOR menu : ui.menus»«menu.toolbar_tpl»«ENDFOR»'''
  }

  def protected toolbar_tpl(UiMenu m) {
    '''    
    .beginToolbar("«m.name»", {dc: "«m.uiDc.name»"})
      «m.toolbar_title_tpl»
      «m.toolbar_items_tpl»
      «IF m.uiDc.parentUiDc != null && !m.noAuto».addSeparator().addAutoLoad()«ENDIF»
      «m.toolbar_buttons_tpl».addReports()
    .end()'''
  }

  def protected toolbar_title_tpl(UiMenu m) {
    '''
      «IF (m.title != null && !m.title.equals(""))».addTitle().addSeparator().addSeparator()«ENDIF»
    '''
  }

  def protected toolbar_items_tpl(UiMenu m) {
    '''
      «IF m.template != null»
        «FOR item : m.template.items».add«item.actionDef.cmd»(«m.toolbar_template_item_cfg_tpl(item)»)«ENDFOR»
      «ENDIF»
      «FOR item : m.items».add«item.actionDef.cmd»(«item.toolbar_item_param_tpl»)«ENDFOR»
    '''
  }

  def protected toolbar_template_item_cfg_tpl(UiMenu m, ro.seava.tool.architect.base.base.Action item) {
    if (m.templateConfig != null) {
      '''
        «FOR c : m.templateConfig.actionConfig.filter(e|e.actionDef == item.actionDef)»
          «c.toolbar_item_param_tpl»
        «ENDFOR»
      '''
    }
  }

  def protected toolbar_item_param_tpl(Action a) {
    '''«IF a.params.size > 0»{«FOR p : a.params SEPARATOR ','»«p.param.name»:"«p.value»"«ENDFOR»}«ENDIF»'''
  }

  def protected toolbar_buttons_tpl(UiMenu m) {
    if (m.buttons.size > 0) {
      '''
        .addSeparator().addSeparator()
        .addButtons([«FOR btn : m.buttons SEPARATOR ','»this._elems_.get("«btn.name»") «ENDFOR»])
      '''
    }
  }

  /* ================================   LINK ELEMENTS ================================  */
  def private link_elements_tpl(Ui ui) {
    '''
      «ui.link_elements_views_tpl»
      «ui.link_elements_toc_tpl»
      «ui.link_elements_toolbars_tpl»
    '''
  }

  def private link_elements_views_tpl(Ui ui) {
    '''
      «FOR c : ui.views.filter(typeof(Panel))»
        «c.container_children_tpl»
      «ENDFOR»
    '''
  }

  def private link_elements_toc_tpl(Ui ui) {
    '''
      «IF ui.withToc»
        .addChildrenTo("_main_with_toc_",["main","_toc_"]).change("main",{region: "center"})
      «ENDIF»
    '''
  }

  def private link_elements_toolbars_tpl(Ui ui) {
    '''
      «FOR m : ui.menus»
        .addToolbarTo("«m.uiView.name»", "«m.name»")
      «ENDFOR»
    '''
  }

  def protected container_children_tpl(Panel c) {
    '''.addChildrenTo("«c.name»", [«FOR item : c.items SEPARATOR ', '»"«item.name»"«ENDFOR»]«c.
      container_children_details_tpl»)'''
  }

  def protected container_children_details_tpl(Panel c) {
    if (c.layout == E_Layout.BORDER) {
      ''',[«FOR lr : c.layoutConfig.regions SEPARATOR ', '»"«lr.region.getName().toLowerCase()»"«ENDFOR»]'''
    }
  }

  /* ================================   DEFINE DC ================================  */
  /**
   * 
   */
  def private define_dcs_tpl(Ui ui) {
    '''
      «FOR uidc : ui.dcs»
        «uidc.define_dcs_dc_tpl»
      «ENDFOR»
      «FOR uidc : ui.dcs.filter(e|e.parentUiDc != null)»
        «uidc.define_dcs_link_tpl»
      «ENDFOR»
    '''
  }

  /**
   * 
   */
  def private define_dcs_dc_tpl(UiDc uidc) {
    var StringBuffer _props = new StringBuffer();
    var b = false;
    if (uidc.multiEdit) {
      _props.append("multiEdit: true")
    }
    if (uidc.trackEditMode) {
      if (_props.length > 0) {
        _props.append(", ")
      }
      _props.append("trackEditMode: true")
    }
    if (_props.length > 0) {
      b = true;
    }

    '''
    .addDc("«uidc.name»", Ext.create(«uidc.dc.canonicalNameDc»,{«_props.toString»«uidc.attrMap._attribute_set_tpl(b)»}))'''
  }

  /**
   * 
   */
  def private define_dcs_link_tpl(UiDc uidc) {
    '''
    .linkDc("«uidc.name»", "«uidc.parentUiDc.name»",{«IF uidc.autoLoad»fetchMode:"auto",«ENDIF»
      fields:[«FOR f : uidc.fields SEPARATOR ', '»«f.dc_relation_field_tpl»«ENDFOR»]})'''
  }

  /**
   * 
   */
  def private dc_relation_field_tpl(UiDcLink f) {
    var childKey = "childField";
    if (f.childField.isParam) {
      childKey = "childParam";
    }
    var String parentKey = "";
    var String parentValue = "";

    if (f.parentField != null) {

      if (f.parentField.isParam) {
        parentKey = "parentParam";
      } else {
        parentKey = "parentField";
      }
      parentValue = f.parentField.name;

    } else if (f.staticValue != null) {
      parentKey = "value";
      parentValue = f.staticValue;
    }

    '''{«childKey»:"«f.childField.name»", «parentKey»:"«parentValue»"«IF f.noFilter», noFilter:true«ENDIF»}'''
  }

  /**
   * 
   */
  def private _attribute_set_tpl(AttrMap attrMap, boolean initialSeparator) {
    if (attrMap != null) {
      '''«IF initialSeparator», «ENDIF»«FOR at : attrMap.entries SEPARATOR ','» «at.attr.attr_print_name»:«IF at.
        attr.valueType == E_DataType.STRING»"«at.value»"«ELSE»«at.value»«ENDIF»«ENDFOR»'''
    }
  }

  /* ================================   DEFINE ELEMENTS ================================  */
  def private define_elements_tpl(Ui ui) {
    '''
      «ui.define_elements_buttons_tpl»
      «ui.define_elements_views_tpl»
      «IF ui.withToc»«ui.define_elements_toc_tpl»«ENDIF»
    '''
  }

  def private define_elements_toc_tpl(Ui ui) {
    '''
      .addPanel({name:"_main_with_toc_", layout:"border", id:Ext.id(), defaults:{split:true}, header:false,
            listeners:{ activate:{scope:this,fn:function(p){p.doLayout(false,true); this.fireEvent('canvaschange', p);  } }}
        })
      .addToc([«FOR tocItem : ui.toc SEPARATOR ','»"«tocItem.uiView.name»:«tocItem.uiDc.name»"«ENDFOR»])
    '''
  }

  def private define_elements_buttons_tpl(Ui ui) {
    '''
      «FOR b : ui.buttons»
        «b.button_tpl»
      «ENDFOR»
    '''
  }

  def private define_elements_views_tpl(Ui ui) {
    '''
      «FOR v : ui.views»
        «v.view_tpl»
      «ENDFOR»
    '''
  }

  // ========================= VIEWS =========================
  def private view_tpl(IUiView v) {
    if (v instanceof UiDcView) {
      (v as UiDcView).view_tpl
    } else if (v instanceof Panel) {
      (v as Panel).view_tpl
    } else if (v instanceof UiWindow) {
      (v as UiWindow).view_tpl
    }
  }

  def protected view_tpl(UiWindow w) {
    '''    
    .addWindow({«w.container_commons_tpl», closeAction:'hide', resizable:true, layout:"fit"«IF w.notModal», modal:false«ELSE», modal:true«ENDIF»,
      items:[this._elems_.get("«w.item.name»")]})'''
  }

  def private view_tpl(UiDcView v) {
    '''
    .addDcView("«v.uiDc.name»", {«v.container_commons_tpl», xtype:"«v.dcView.xtype»"«IF v.noInsert», noInsert:true«ENDIF»«IF v.noUpdate», noUpdate:true«ENDIF»})'''

  //    '''
  //    .addDc«v.dcView._dcViewNameToken»View("«v.uiDc.name»", {«v._containerCommonsTpl», xtype:"«v.dcView.xtype»"«IF v.
  //      dcView.isEditableGrid()», frame:true«ENDIF»«v.config.attributeSet._attribute_set_tpl(true)»«v._buttonsTpl»«IF this.
  //      _grid_print_titles.containsKey(v.name)», _printTitle: "«this._grid_print_titles.get(v.name)»"«ENDIF»«IF v.
  //      noFocusOnInsert», _acquireFocusInsert_: false«ENDIF»«IF v.noFocusOnUpdate», _acquireFocusUpdate_: false«ENDIF»})'''
  }

  def private view_tpl(Panel v) {
    '''.addPanel({«v.container_commons_tpl»«v.container_layout_tpl»})'''
  }

  def private container_commons_tpl(IUiView v) {
    '''name:"«v.name»"«IF v.config.title != null», _hasTitle_:true«ENDIF»«IF v.name.toLowerCase().
      startsWith("canvas")», preventHeader:true, isCanvas:true«ENDIF»«IF v.config.width > 0 &&
      !v.config.isWidthPercent», width:«v.config.width»«ENDIF»«IF v.config.width > 0 && v.config.isWidthPercent», width:"«v.
      config.width»%"«ENDIF»«IF v.config.height > 0 && !v.config.isHeightPercent», height:«v.config.height»«ENDIF»«IF v.
      config.height > 0 && v.config.isHeightPercent», height:"«v.config.height»%"«ENDIF»«v.config.attrMap.
      _attribute_set_tpl(true)»«v.container_buttons_tpl»'''
  }

  def private container_buttons_tpl(IUiView c) {
    if (c.config.buttons != null && c.config.buttons.size > 0) {

      ''', 
    dockedItems:[{xtype:"toolbar", ui:"footer", dock:'«c.config.side.getName().toLowerCase()»', weight:-1,
      items:[«FOR button : c.config.buttons SEPARATOR ','» this._elems_.get("«button.name»")«ENDFOR»]}]'''
    }
  }

  def private container_layout_tpl(Panel v) {
    var t = v.layout
    if (t == E_Layout.HORIZONTAL) {
      return v.container_hlayout_tpl
    } else if (t == E_Layout.VERTICAL) {
      return v.container_vlayout_tpl
    } else if (t == E_Layout.BORDER) {
      return v.container_border_layout_tpl
    } else if (t == E_Layout.STACK) {
      return v.container_stack_layout_tpl
    } else if (t == E_Layout.TAB) {
      return v.container_tab_layout_tpl
    } else if (t == E_Layout.WRAPPER) {
      return v.container_wrapper_layout_tpl
    } else if (t == E_Layout.ACCORDION) {
      return v.container_accordion_layout_tpl
    }
  }

  def private container_hlayout_tpl(Panel v) {
    ''''''
  }

  def private container_vlayout_tpl(Panel v) {
    ''', layout:"hbox", layoutConfig: {padding:"2", align: "stretchmax"}'''
  }

  def private container_accordion_layout_tpl(Panel v) {
    ''', layout:{ type: "accordion" }, activeItem:0'''
  }

  def private container_border_layout_tpl(Panel v) {
    ''', layout:{ type: "border"«v.container_border_layout_tpl_weight»}, defaults:{split:true}'''
  }

  def private container_border_layout_tpl_weight(Panel v) {
    if (v.layoutConfig != null) {
      ''', regionWeights: {«FOR r : v.layoutConfig.regions SEPARATOR ','»«r.region.literal.toLowerCase»:«r.weight»«ENDFOR»}'''
    }
  }

  def private container_stack_layout_tpl(Panel v) {
    ''', layout:{ type: "card" }, activeItem:0'''
  }

  def private container_tab_layout_tpl(Panel v) {
    ''', xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false'''
  }

  def private container_wrapper_layout_tpl(Panel v) {
    ''''''
  }

  // ========================= BUTTONS =========================
  def private button_tpl(UiButton b) {
    '''    
    .addButton({name:"«b.name»"«b.button_commons_tpl», handler: «b.button_clickHandler_tpl»,«b.button_state_manager_tpl» scope:this})'''
  }

  def private button_commons_tpl(UiButton b) {
    ''', disabled:«IF b.enableRule != null && b.enableRule.defaultDisabled»true«ELSE»false«ENDIF»«IF b.styleClass !=
      null», iconCls:"«b.styleClass.value»"«ENDIF»'''
  }

  def private button_clickHandler_tpl(UiButton b) {
    if (b.handlerFn != null) {
      '''this.«b.handlerFn.name»'''
    } else {
      '''this.on«b.name.toFirstUpper()»'''
    }
  }

  def private button_state_manager_tpl(UiButton b) {
    if (b.enableRule != null) {
      ''' 
      
          stateManager:{ name:"«b.enableRule.name»", dc:"«b.enableRuleContext.name»" «IF b.jsExpression != null», and: function(dc) {return («b.
        jsExpression»);}«ENDIF»},'''
    }
  }

  def private button_handler_tpl(UiButton b) {
    '''
      «IF b.handlerCodeBlock != null»
        
        /**
         * On-Click handler for button «b.name»
         */
        on«b.name.toFirstUpper()»: function() {
          «b.handlerCodeBlock.statements_tpl»
        },
      «ENDIF»
    '''
  }

  // ================== FUNCTIONS ==================
  def private function_tpl(Function f) {
    '''
      
      «f.name»: function(«FOR a : f.args SEPARATOR ','»«a»«ENDFOR») {
        «IF f.body != null»
          «f.body»
        «ELSE»
          «f.codeBlock.statements_tpl»
        «ENDIF»
      },
    '''
  }

  def private statements_tpl(CodeBlock codeBlock) {
    '''
      «FOR st : codeBlock.statements»
        «st.statement_tpl»
      «ENDFOR»
    '''
  }

  def protected CharSequence statement_tpl(IStatement s) {
    if (s instanceof JsStatement) {
      (s as JsStatement).statement_JsStatement_tpl
    } else if (s instanceof CallFnStatement) {
      (s as CallFnStatement).statement_CallFnStatement_tpl
    } else if (s instanceof CallRpcStatement) {
      (s as CallRpcStatement).statement_CallRpcStatement_tpl
    } else if (s instanceof InvokeMethodStatement) {
      (s as InvokeMethodStatement).statement_InvokeMethodStatement_tpl
    } else if (s instanceof OpenUiStatement) {
      (s as OpenUiStatement).statement_OpenUiStatement_tpl
    } else if (s instanceof OpenAsgnStatement) {
      (s as OpenAsgnStatement).statement_OpenAsgnStatement_tpl
    }
  }

  def protected statement_JsStatement_tpl(JsStatement s) {
    '''
      «s.code»
    '''
  }

  def protected statement_CallFnStatement_tpl(CallFnStatement s) {
    '''this.«s.fn.name»();'''
  }

  /**
   * 
   */
  def protected statement_InvokeMethodStatement_tpl(InvokeMethodStatement s) {
    '''
      «IF s.target instanceof UiDc»
      this._getDc_("«s.target.name»")«ELSEIF s.target instanceof UiWindow»
      this._getWindow_("«s.target.name»")«ELSE»
      this._getElement_("«s.target.name»")«ENDIF».«s.fn.name»();
    '''
  }

  // Open frame statement 
  /**
   * 
   */
  def protected statement_OpenUiStatement_tpl(OpenUiStatement s) {
    '''      
      «IF s.params.size > 0»    
        getApplication().showFrame("«s.ui.canonicalNameUi»",{
          params: {
            «FOR p : s.params SEPARATOR ','»
              «p.paramName»: «IF p.uiDcMember instanceof IVmField»this._getDc_("«p.uiDc.name»").getRecord().get("«p.
        uiDcMember.name»")«ELSE»this._getDc_("«p.uiDc.name»").getParamValue("«p.uiDcMember.name»")«ENDIF»
            «ENDFOR»
          },
          callback: function (params) {
            this.«s.callbackFn.name»(params);
          }
        });
      «ELSE»
        getApplication().showFrame("«s.ui.canonicalNameUi»");
      «ENDIF»
    '''
  }

  // Call RPC statement 
  /**
   * 
   */
  def protected statement_CallRpcStatement_tpl(CallRpcStatement s) {
    '''
      «IF s.successCodeBlock != null»
        var successFn = function(dc,response,serviceName,specs) {
          «s.successCodeBlock.statements_tpl»
        };
      «ENDIF»
      «IF s.failureCodeBlock != null»
        var failureFn = function(dc,response,serviceName,specs) {
          «s.failureCodeBlock.statements_tpl»
        }; 
      «ENDIF»
      var o={
        name:"«IF s.rpc instanceof Rpc»«(s.rpc as Rpc).name»«ENDIF»",
        «IF s.successFn != null || s.failureFn != null || s.successCodeBlock != null || s.failureCodeBlock != null»
          callbacks:{
            «IF s.successFn != null || s.successCodeBlock != null»
              successFn: «IF s.successCodeBlock != null»successFn«ELSE»«s.successFn.name»«ENDIF»,
              successScope: this«IF s.failureFn != null || s.failureCodeBlock != null»,«ENDIF»
            «ENDIF»
            «IF s.failureFn != null || s.failureCodeBlock != null»
              failureFn: «IF s.failureCodeBlock != null»failureFn«ELSE»«s.failureFn.name»«ENDIF»,
              failureScope: this
            «ENDIF»
          },
        «ENDIF»
        modal:true
      };
      this._getDc_("«s.uiDc.name»").«(s.rpc as Rpc).rpc_command_name»(o);
    '''
  }

  def private rpc_command_name(Rpc rpc) {
    if (rpc.type == E_Rpc.RPC_FILTER) {
      "doRpcFilter"
    } else if (rpc.type == E_Rpc.RPC_DATA) {
      "doRpcData"
    } else if (rpc.type == E_Rpc.RPC_LIST_DATA) {
      "doRpcDataList"
    } else if (rpc.type == E_Rpc.RPC_LIST_ID) {
      "doRpcIdList"
    }
  }

  // Open assignment statement 
  /**
   * 
   */
  def protected statement_OpenAsgnStatement_tpl(OpenAsgnStatement s) {
    '''this._showAsgnWindow_("«s.asgn.canonicalNameWindowUiExtjs»" ,{dc: "«s.contextUiDc.name»", objectIdField: "«s.
      contextDsField.name»"«s.statement_OpenAsgnStatement_context»«IF s.onCloseCodeBlock != null»
    ,listeners: {close: {scope: this, fn: function() { «s.onCloseCodeBlock.statements_tpl»}} }«ENDIF»});'''
  }

  def private statement_OpenAsgnStatement_context(OpenAsgnStatement s) {
    '''«s.statement_OpenAsgnStatement_filter("left")»«s.statement_OpenAsgnStatement_filter("right")»«s.
      statement_OpenAsgnStatement_params("left")»«s.statement_OpenAsgnStatement_params("right")»'''
  }

  def private statement_OpenAsgnStatement_filter(OpenAsgnStatement s, String side) {
    var l = s.args.filter(t|t.lovMember instanceof AsgnField);
    if (l.size > 0) {
      ''',«side»Filter: {«FOR a : l SEPARATOR ','»«a.lovMember.name»:«IF a.staticValue != null»"«a.staticValue»"«ELSE»this._getDc_("«IF a.
        uiDc == null»«s.contextUiDc.name»«ELSE»«a.uiDc.name»«ENDIF»").getRecord().get("«a.uiDcMember.name»")«ENDIF»«ENDFOR»}'''
    }
  }

  def private statement_OpenAsgnStatement_params(OpenAsgnStatement s, String side) {
    var l = s.args.filter(t|t.lovMember instanceof AsgnParam);
    if (l.size > 0) {
      ''',«side»Params: {«FOR a : l SEPARATOR ','»«a.lovMember.name»:«IF a.staticValue != null»"«a.staticValue»"«ELSE»this._getDc_("«IF a.
        uiDc == null»«s.contextUiDc.name»«ELSE»«a.uiDc.name»«ENDIF»").getRecord().get("«a.uiDcMember.name»")«ENDIF»«ENDFOR»}'''
    }
  }

  // Event handlers
  def private event_handler_tpl(EventHandler evh) {
    '''
      
      «evh.event.extjsName»: function() {
        this.«evh.fnRef.name»();
      },
    '''
  }
}
