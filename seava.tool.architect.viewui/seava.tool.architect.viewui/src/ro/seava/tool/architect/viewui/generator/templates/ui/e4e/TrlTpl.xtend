package ro.seava.tool.architect.viewui.generator.templates.ui.e4e

import static extension ro.seava.tool.architect.viewui.extensions.Frame_Cfg.*
import ro.seava.tool.architect.viewui.viewUi.Ui

/**
 * Translation file for a frame.  
 */
class TrlTpl {

  def doFile(Ui ui) {
    '''
      Ext.define("«ui.canonicalNameGeneratedUiTrl»", {
        /* view */
        «FOR elem : ui.views.filter(e|e.config.title != null).sortBy(e|e.name)»
          «elem.name»__ttl: "«elem.config.title»",
        «ENDFOR»
        /* menu */
        «FOR elem : ui.menus.filter(e|e.title != null).sortBy(e|e.name)»
          «elem.name»__ttl: "«elem.title»",
        «ENDFOR»
        /* button */
        «FOR elem : ui.buttons.filter(e|e.title != null).sortBy(e|e.name)»
          «elem.name»__lbl: "«elem.title»",
          «elem.name»__tlp: "«elem.description»",
        «ENDFOR»
        
        title: "«ui.title»"
      });
    '''
  }
}
