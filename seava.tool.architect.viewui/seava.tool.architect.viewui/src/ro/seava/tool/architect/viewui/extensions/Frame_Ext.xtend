package ro.seava.tool.architect.viewui.extensions

//import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*
import static extension ro.seava.tool.architect.viewdc.extensions.Dc_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.Asgn_Cfg.*

import java.util.Set
import ro.seava.tool.architect.viewui.viewUi.Ui
import java.util.HashSet
import ro.seava.tool.architect.viewui.viewUi.UiDcView
import ro.seava.tool.architect.viewui.viewUi.OpenAsgnStatement
import ro.seava.tool.architect.viewui.viewUi.Function

class Frame_Ext extends Frame_Cfg {

  def static Set<String> collect_dependencies(Ui ui) {
    var Set<String> list = new HashSet<String>();

    // collect data-controls
    for (uidc : ui.dcs) {
      list.add(uidc.dc.canonicalNameDc);
    }

    // collect data-control views 
    for (uiDcView : ui.views.filter(typeof(UiDcView))) {
      list.add(uiDcView.dcView.canonicalNameDcView);
    }

    // collect assignment windows from button handlers
    for (btn : ui.buttons.filter(e|e.handlerCodeBlock != null)) {
      for (asgnCall : btn.handlerCodeBlock.statements.filter(typeof(OpenAsgnStatement))) {
        list.add(asgnCall.asgn.canonicalNameWindowUiExtjs);
      }
    }

    // collect assignment windows from functions
    for (fn : ui.functions.filter(typeof(Function)).filter(e|e.codeBlock != null)) {
      for (asgnCall : fn.codeBlock.statements.filter(typeof(OpenAsgnStatement))) {
        list.add(asgnCall.asgn.canonicalNameWindowUiExtjs);
      }
    }

    for (dpd : ui.externalDependencies) {
      list.add(dpd)
    }

    return list
  }

}
