package ro.seava.tool.architect.viewui.generator.templates.lovlink.e4e

import static extension ro.seava.tool.architect.viewui.extensions.Frame_Cfg.*
import static extension ro.seava.tool.architect.viewdc.extensions.Lov_Ext.*
import static extension ro.seava.tool.architect.presenter.extensions.Vm_Cfg.*
import static extension ro.seava.tool.architect.presenter.extensions.VmMember_Ext.*
import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*

import ro.seava.tool.architect.abstracts.templates.Abstract_Tpl
import ro.seava.tool.architect.viewui.viewUi.LovLink
import ro.seava.tool.architect.viewdc.viewDc.LovField

class LovLinkTpl extends Abstract_Tpl {

  def doDependencyFile(LovLink link) {
    var lov = link.lov;

    '''
      [
        «IF lov.source.hasParam»
          "«lov.source.canonicalNameParamUiExtjs»",
        «ENDIF»
        "«lov.source.canonicalNameModelUiExtjs»"
      ]
    '''
  }

  def doTrlFile(LovLink link) {
    '''
      «copyright»
      Ext.define("«link.lov.canonicalNameGeneratedLovTrl»", {
      });
    '''
  }

  def doJsClass(LovLink link) {
    var lov = link.lov;

    '''
      «copyright»
      Ext.define("«lov.canonicalNameGeneratedLov»", {
        extend: "seava.lib.e4e.js.lov.AbstractCombo",
        alias: "widget.«lov.alias»",
        displayField: "«lov.getReturnFieldName»",
        listConfig: {
          getInnerTpl: function() {
            return '<span>«FOR f : lov.getAllDisplayedFields SEPARATOR ', '»«f.lov_field_tpl»«ENDFOR»</span>';
          }
          //width:
        },
        «IF link.ui != null»
          _editFrame_: {
            name: "«link.ui.canonicalNameUi»"«IF link.canvas != null»,
            tocElement: "«link.canvas.name»"«ENDIF»
          },
          triggers : {
            picker : {
              handler : 'onTriggerClick',
              scope : 'this'
            },
            showFrame : {
              handler : 'onTrigger2Click',
              scope : 'this',
              cls : Ext.baseCSSPrefix + 'form-search-trigger'
            }
          },
        «ENDIF»
        «IF lov.source.hasParam»
          paramModel: "«lov.source.canonicalNameParamUiExtjs»",
        «ENDIF»
        recordModel: "«lov.source.canonicalNameModelUiExtjs»"
      });
    '''

  //width:«IF lov.width > 0»«lov.width»«ELSE»250«ENDIF», maxHeight:«IF lov.height > 0»«lov.height»«ELSE»350«ENDIF»
  }

  def private lov_field_tpl(LovField f) {
    '''«IF f.source.dataType.date»{[ Ext.util.Format.date(values.«f.source.name»,Dnet.DATE_FORMAT)]}«ELSE»{«f.source.
      name»}«ENDIF»'''
  }

}
