package ro.seava.tool.architect.viewui.extensions

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.viewui.viewUi.UI_Package
import ro.seava.tool.architect.viewui.viewUi.Ui
import ro.seava.tool.architect.project.project.Module
import ro.seava.tool.architect.project.project.Unit

class Frame_Cfg {

  /* ====================== DC OWNERS ====================== */
  /**
   * Get containing package
   */
  def static UI_Package getPackage(Ui ui) {
    return ui.eContainer as UI_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(Ui ui) {
    return ui.package.module;
  }

  /**
   * Get containing unit
   */
  def static Unit getUnit(Ui ui) {
    return ui.package.unit;
  }

  /* ====================== SIMPLE NAMES ====================== */
  def static String simpleNameUi(Ui ui) {
    return ui.name + "_Ui";
  }

  /* ====================== ROOT PACKAGES ====================== */
  def static String getRootPackageUi(Ui ui) {
    ui.module.config.ui.rootPackage + ".ui.extjs"
  }

  def static String getRootPackageTrl(Ui ui) {
    ui.module.config.trl.rootPackage + ".i18n.extjs"
  }

  /* ====================== PACKAGES ====================== */
  def static String getPackageGeneratedUiTrl(Ui ui) {
    var unit = ui.unit;
    if (unit != null) {
      ui.rootPackageTrl + "." + ui.module.config.refLanguage + ".generated." + unit.name + ".frame";
    } else {
      ui.rootPackageTrl + "." + ui.module.config.refLanguage + ".generated.frame";
    }
  }

  def static String getPackageGeneratedUi(Ui ui) {
    var unit = ui.unit;
    if (unit != null) {
      ui.rootPackageUi + ".generated." + unit.name + ".frame";
    } else {
      ui.rootPackageUi + ".generated.frame";
    }
  }

  def static String getPackageCustomUi(Ui ui) {
    var unit = ui.unit;
    if (unit != null) {
      ui.rootPackageUi + ".extensions." + unit.name + ".frame";
    } else {
      ui.rootPackageUi + ".extensions.frame";
    }
  }

  /* ====================== GENERATED CONTENT ====================== */
  def static boolean hasGeneratedUi(Ui ui) {
    return true
  }

  def static boolean hasCustomUi(Ui ui) {
    return false
  }

  /* ====================== CANONICAL NAMES ====================== */
  def static String canonicalNameGeneratedUi(Ui ui) {
    return ui.packageGeneratedUi + '.' + ui.simpleNameUi;
  }

  def static String canonicalNameCustomUi(Ui ui) {
    return ui.packageCustomUi + '.' + ui.simpleNameUi;
  }

  def static String canonicalNameUi(Ui ui) {
    if (ui.hasCustomUi) {
      return ui.canonicalNameCustomUi
    } else {
      return ui.canonicalNameGeneratedUi
    }
  }

  def static String canonicalNameGeneratedUiTrl(Ui ui) {
    return ui.packageGeneratedUiTrl + '.' + ui.simpleNameUi;
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  def static String fileLocationGeneratedUi(Ui ui) {
    return ui.module.config.ui.path + "/src/main/resources/public/" + ui.canonicalNameGeneratedUi.pathFromFQN +
      ".js"
  }

  def static String fileLocationCustomUi(Ui ui) {
    return ui.module.config.ui.path + "/src/main/resources/public/" + ui.canonicalNameCustomUi.pathFromFQN + ".js"
  }

  def static String fileLocationGeneratedUiDpd(Ui ui) {
    return ui.module.config.ui.path + "/src/main/resources/public/" + ui.canonicalNameGeneratedUi.pathFromFQN +
      ".jsdp"
  }

  def static String fileLocationCustomUiDpd(Ui ui) {
    return ui.module.config.ui.path + "/src/main/resources/public/" + ui.canonicalNameCustomUi.pathFromFQN + ".jsdp"
  }

  def static String fileLocationGeneratedUiTrl(Ui ui) {
    return ui.module.config.trl.path + "/src/main/resources/public/" + ui.canonicalNameGeneratedUiTrl.pathFromFQN +
      ".js"
  }

}
