package ro.seava.tool.architect.business.generator.controllers.db

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.business.extensions.Bm_Ext.*
import static extension ro.seava.tool.architect.business.extensions.BmMember_Ext.*
import static extension ro.seava.tool.architect.business.scoping.BusinessScopeUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*
import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.business.IBmField
import ro.seava.tool.architect.business.business.BmAttrField
import ro.seava.tool.architect.business.business.BmRefField
import ro.seava.tool.architect.business.generator.models.db.Table
import ro.seava.tool.architect.business.generator.models.db.Column
import ro.seava.tool.architect.business.generator.models.db.PrimaryKey
import ro.seava.tool.architect.business.business.BmUk
import ro.seava.tool.architect.business.generator.models.db.UniqueKey
import ro.seava.tool.architect.business.generator.models.db.ForeignKey
import java.util.ArrayList
import java.util.List
import ro.seava.tool.architect.business.business.E_CascadeType

/**
 * Abstract controller class to create template model for database table from a certain business-model 
 */
abstract class AbstractDbController {

  /**
   * Database engine name.
   */
  def abstract String getDbEngine() ;

  /**
   * Resolve the column data-type for an attribute field. 
   * It is database specific so an implementation is required for each supported engine.
   */
  def protected abstract void resolveDataType(BmAttrField f, Column c) ;

  /**
   * Resolve the column data-type for a reference field. 
   * It is database specific so an implementation is required for each supported engine.
   */
  def protected abstract void resolveDataType(BmRefField f, Column c) ;

  /**
   * Build the template model for a database table
   */
  def public List<Table> build_db_table_model(Bm bm) {
    var List<Table> result = new ArrayList<Table>();
    result.add(bm.buildMainTable);
    for (ref : bm.collect_members_as_type(BmRefField).filter(r|r.manyToMany && r.joinSource != null)) {
      result.add(ref.buildManyToManyTable);
    }
    return result;
  }

  /**
   * 
   */
  def protected Table buildMainTable(Bm bm) {
    var Table t = new Table();
    t.name = bm.tableName
    t.abstract = bm.isAbstract

    if (!t.abstract && !bm.sourceProvided) {
      var path = bm.module.config.db.path + "/src/main/resources/private/" + bm.rootPackageDb.pathFromFQN + "/" +
        getDbEngine + "/install"

      t.fileLocationTable = path + "/table/" + t.name + ".sql"
      t.fileLocationFk = path + "/fk/" + t.name + ".sql"
    }

    bm.build_columns(t)
    bm.build_primary_key(t)
    bm.build_unique_keys(t)
    bm.build_foreign_keys(t)
    return t;
  }

  def protected buildManyToManyTable(BmRefField ref) {
    var Table t = new Table();
    var bm = ref.eContainer as Bm;

    t.name = ref.joinSource
    t.abstract = false

    var path = bm.module.config.db.path + "/src/main/resources/private/" + bm.rootPackageDb.pathFromFQN + "/" +
      getDbEngine + "/install"
    t.fileLocationTable = path + "/table/" + t.name + ".sql"
    t.fileLocationFk = path + "/fk/" + t.name + ".sql"

    var c0 = new Column;
    var opposite = ref.bm.idField
    c0.bmFieldName = ref.name
    c0.name = ref.multiJoinTableOppositeColumnName
    c0.required = true
    opposite.resolveDataType(c0)
    t.columns.add(c0)

    var fk0 = new ForeignKey
    fk0.setName("FK_" + t.name + "_1")
    fk0.referencedTableName = bm.tableName
    fk0.columnNames.add(c0.name)
    fk0.referencedColumnNames.add("ID")
    t.foreignKeys.add(fk0)

    var Column c1 = new Column;
    c1.bmFieldName = ref.name
    c1.name = ref.multiJoinTableColumnName
    c1.required = true
    ref.resolveDataType(c1)
    t.columns.add(c1)

    var fk1 = new ForeignKey
    fk1.setName("FK_" + t.name + "_2")
    fk1.referencedTableName = ref.bm.tableName
    fk1.columnNames.add(c1.name)
    fk1.referencedColumnNames.add("ID")
    t.foreignKeys.add(fk1)

    var pk = new PrimaryKey
    pk.columnNames = new ArrayList
    pk.columnNames.add(c0.name)
    pk.columnNames.add(c1.name)
    pk.name = "PK_" + t.name
    t.primaryKey = pk

    return t;
  }

  /**
   *  Create column definitions
   */
  def protected build_columns(Bm bm, Table t) {
    for (f : bm.collect_members_as_type(IBmField)) {
      if (!f.config.transient) {
        var c = (f as IBmField).build_column
        if (c != null) {
          t.columns.add(c)
        }
      }
    }
  }

  /**
   * Create column definition
   */
  def protected Column build_column(IBmField f) {
    var Column c = new Column;
    c.bmFieldName = f.name
    c.name = f.columnName
    c.required = f.config.isRequired

    if (f.attrField) {
      (f as BmAttrField).build_column(c)
    } else if (f.refField) {
      var rf = f as BmRefField;
      if (!rf.collection) {
        rf.build_column(c)
      } else {
        return null;
      }
    }
    return c
  }

  /**
   * Create column definition for an attribute business-model field
   */
  def protected build_column(BmAttrField f, Column c) {
    f.resolveDataType(c)
  }

  /**
   * Create column definition for a reference business-model field 
   */
  def protected build_column(BmRefField f, Column c) {
    f.resolveDataType(c)
  }

  /**
   * Create primary key definition
   */
  def protected build_primary_key(Bm bm, Table t) {
    var idPk = bm.idPk
    if (idPk != null) {
      var target = new PrimaryKey
      target.columnNames.add(idPk.field.columnName)
      t.primaryKey = target
    } else {
      var _pk = bm.pk
      if (_pk != null) {
        var target = new PrimaryKey
        for (f : _pk.fields) {
          target.columnNames.add(f.columnName)
        }
        t.primaryKey = target
      }
    }
  }

  /**
   * Create unique key definitions
   */
  def protected build_unique_keys(Bm bm, Table t) {
    var uks = bm.collect_members_as_type(BmUk)
    var List<String> processed = new ArrayList<String>();
    for (source : uks) {
      if (!processed.contains(source.name)) {
        var target = new UniqueKey
        target.setName("UK_" + t.name + "_" + source.ukNo)
        if (bm.hasClientIdField) {
        	target.columnNames.add("CLIENTID")
        }
        for (f : source.fields) {
          target.columnNames.add(f.columnName)
        }
        t.uniqueKeys.add(target)
        processed.add(source.name)
      }
    }
  }

  /**
   * Create foreign key definitions
   */
  def protected build_foreign_keys(Bm bm, Table t) {
    var refs = bm.collect_members_as_type(BmRefField).filter(e|!e.collection)
    for (source : refs) {
      var target = new ForeignKey
      target.setName("FK_" + t.name + "_" + source.fkNo)
      target.referencedTableName = source.bm.tableName
      target.columnNames.add(source.columnName)
      target.referencedColumnNames.add("ID")

      if (source.bm.members.filter(BmRefField).exists(
        fkf|
          fkf.bm == bm && fkf.collection &&
            (fkf.cascade.contains(E_CascadeType.ALL) || fkf.cascade.contains(E_CascadeType.REMOVE)))) {
        target.cascadeDelete = true;
      }
      t.foreignKeys.add(target)
    }
  }

}
