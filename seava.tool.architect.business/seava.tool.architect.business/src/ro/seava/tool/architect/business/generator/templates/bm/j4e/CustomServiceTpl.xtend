package ro.seava.tool.architect.business.generator.templates.bm.j4e

import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*

import ro.seava.tool.architect.abstracts.templates.Abstract_Java_Tpl
import ro.seava.tool.architect.business.business.Bm

class CustomServiceTpl extends Abstract_Java_Tpl {

  /**
   * 
   */
  def doJavaClass(Bm bm) {
    var CharSequence body = bm.body_tpl;

    '''
      «copyright»
      package «bm.packageCustomService»;
      
      «imports»
      «body»
    '''
  }

  def body_tpl(Bm bm) {
    var serviceName = bm.simpleNameCustomService
    '''
      
      /**
       * Service class implementation for {@link «bm.simpleNameModel»} domain
       * entity. <br>
       * Contains repository functionality with finder methods as well as specific business functionality
       */
      public class «serviceName»«bm.extends_tpl»«bm.implements_tpl» {
        //TODO: Implement me...
      }
    '''
  }

  /**
   * Extended classes template
   */
  def private extends_tpl(Bm bm) {
    if (bm.hasGeneratedService) {
      ''' extends «bm.canonicalNameGeneratedService»'''
    } else {
      ''' extends «simpleNameFromFQN("seava.lib.j4e.business.service.entity.AbstractEntityService")»<«bm.
        canonicalNameModel.simpleNameFromFQN»>'''
    }
  }

  /**
   * Implemented interfaces template
   */
  def private implements_tpl(Bm bm) {
    if (bm.hasCustomServiceInterface) {
      ''' implements «bm.canonicalNameCustomServiceInterface.simpleNameFromFQN»'''
    } else if (bm.hasGeneratedServiceInterface) {
      ''' implements «bm.canonicalNameGeneratedServiceInterface.simpleNameFromFQN»'''
    }
  }

}
