package ro.seava.tool.architect.business.extensions

import static extension ro.seava.tool.architect.abstracts.extensions.StringUtils.*
import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.business.B_Package
import ro.seava.tool.architect.project.project.Unit
import ro.seava.tool.architect.project.project.Module

class Bm_Cfg {

  /* ====================== BM OWNERS ====================== */
  /**
   * Get containing package
   */
  def static B_Package getPackage(Bm bm) {
    return bm.eContainer as B_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(Bm bm) {
    return bm.package.module;
  }

  /**
   * Get containing unit
   */
  def static Unit getUnit(Bm bm) {
    return bm.package.unit;
  }

  /* ====================== SIMPLE NAMES ====================== */
  def static String simpleNameModel(Bm bm) {
    return bm.name
  }

  def static String simpleNameCustomModel(Bm bm) {
    return bm.name + "_EventHandler"
  }

  def static String simpleNameService(Bm bm) {
    return bm.name + "_Service";
  }

  def static String simpleNameCustomService(Bm bm) {
    return bm.name + "_Service";
  }

  def static String simpleNameServiceInterface(Bm bm) {
    return 'I' + bm.name + "Service";
  }

  def static String simpleNameCustomServiceInterface(Bm bm) {
    return 'I' + bm.name + "Service";
  }

  /* ====================== ROOT PACKAGES ====================== */
  def static String getRootPackageDb(Bm bm) {
    bm.module.config.db.rootPackage + ".db"
  }

  def static String getRootPackageDomain(Bm bm) {
    bm.module.config.domain.rootPackage + ".domain"
  }

  def static String getRootPackageBusiness(Bm bm) {
    bm.module.config.business.rootPackage + ".business_impl"
  }

  def static String getRootPackageBusinessApi(Bm bm) {
    bm.module.config.businessApi.rootPackage + ".business_api"
  }

  /* ====================== PACKAGES ====================== */
  def static String getPackageGeneratedModel(Bm bm) {
    var unit = bm.unit;
    if (unit != null) {
      bm.rootPackageDomain + ".generated." + unit.name + ".model";
    } else {
      bm.rootPackageDomain + ".generated.model"
    }
  }

  def static String getPackageCustomModel(Bm bm) {
    var unit = bm.unit;
    if (unit != null) {
      bm.rootPackageDomain + ".extensions." + unit.name + ".model";
    } else {
      bm.rootPackageDomain + ".extensions.model"
    }
  }

  def static String getPackageGeneratedService(Bm bm) {
    var unit = bm.unit;
    if (unit != null) {
      bm.rootPackageBusiness + ".generated." + unit.name + ".service";
    } else {
      bm.rootPackageBusiness + ".generated.service"
    }
  }

  def static String getPackageCustomService(Bm bm) {
    var unit = bm.unit;
    if (unit != null) {
      bm.rootPackageBusiness + ".extensions." + unit.name + ".service";
    } else {
      bm.rootPackageBusiness + ".extensions.service";
    }
  }

  def static String getPackageGeneratedServiceInterface(Bm bm) {
    var unit = bm.unit;
    if (unit != null) {
      bm.rootPackageBusinessApi + ".generated." + unit.name + ".spi";
    } else {
      bm.rootPackageBusinessApi + ".generated.spi"
    }
  }

  def static String getPackageCustomServiceInterface(Bm bm) {
    var unit = bm.unit;
    if (unit != null) {
      bm.rootPackageBusinessApi + ".extensions." + unit.name + ".spi";
    } else {
      bm.rootPackageBusinessApi + ".extensions.spi"
    }
  }

  /* ====================== CANONICAL NAMES ====================== */
  def static String alias(Bm bm) {
    return bm.module.config.aliasPrefixBusiness + bm.name
  }

  def static String canonicalNameGeneratedModel(Bm bm) {
    return bm.packageGeneratedModel + '.' + bm.simpleNameModel;
  }

  def static String canonicalNameCustomModel(Bm bm) {
    return bm.packageCustomModel + '.' + bm.simpleNameModel;
  }

  def static String canonicalNameModel(Bm bm) {
    if (bm.hasCustomModel) {
      return bm.canonicalNameCustomModel;
    } else {
      return bm.canonicalNameGeneratedModel;
    }
  }

  def static String canonicalNameGeneratedService(Bm bm) {
    return bm.packageGeneratedService + '.' + bm.simpleNameService;
  }

  def static String canonicalNameCustomService(Bm bm) {
    return bm.packageCustomService + '.' + bm.simpleNameCustomService;
  }

  def static String canonicalNameGeneratedServiceInterface(Bm bm) {
    return bm.packageGeneratedServiceInterface + '.' + bm.simpleNameServiceInterface;
  }

  def static String canonicalNameCustomServiceInterface(Bm bm) {
    return bm.packageCustomServiceInterface + '.' + bm.simpleNameCustomServiceInterface;
  }

  /* ====================== GENERATED CONTENT ====================== */
  def static boolean hasGeneratedModel(Bm bm) {
    return true
  }

  def static boolean hasCustomModel(Bm bm) {
    return bm.withCustomModel
  }

  def static boolean hasGeneratedService(Bm bm) {
    return bm.withCustomService || (bm.serviceMethods.size > 0 || bm.findByMethods.size > 0)
  }

  def static boolean hasCustomService(Bm bm) {
    return bm.withCustomService || bm.serviceMethods.size > 0
  }

  def static boolean hasGeneratedServiceInterface(Bm bm) {
    return !bm.isAbstract && (bm.serviceMethods.size > 0 || bm.findByMethods.size > 0)
  }

  def static boolean hasCustomServiceInterface(Bm bm) {
    return bm.withCustomServiceInterface
  }

  /* ====================== GENERATED CONTENT ====================== */
  // database
  def static boolean generate_table(Bm bm) {
    return !bm.isAbstract && bm.source != null && !bm.sourceProvided
  }

  /**
   * Generate database objects definition as SQL files for MySql database? 
   */
  def static boolean generate_mysql_sql(Bm bm) {
    return true
  }

  /**
   * Generate database objects definition as Liquibase files for MySql database? 
   */
  def static boolean generate_mysql_liquibase(Bm bm) {
    return true
  }

  // java 
  /**
   * Generate JPA content ? 
   */
  def static boolean generate_jpa(Bm bm) {
    return true
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  def static String fileLocationGeneratedModel(Bm bm) {
    return bm.module.config.domain.path + "/src/main/java/" + bm.canonicalNameGeneratedModel.pathFromFQN + ".java"
  }

  def static String fileLocationCustomModel(Bm bm) {
    return bm.module.config.domain.path + "/src/main/java/" + bm.canonicalNameCustomModel.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedService(Bm bm) {
    return bm.module.config.business.path + "/src/main/java/" + bm.canonicalNameGeneratedService.pathFromFQN +
      ".java"
  }

  def static String fileLocationCustomService(Bm bm) {
    return bm.module.config.business.path + "/src/main/java/" + bm.canonicalNameCustomService.pathFromFQN + ".java"
  }

  def static String fileLocationGeneratedServiceInterface(Bm bm) {
    return bm.module.config.businessApi.path + "/src/main/java/" +
      bm.canonicalNameGeneratedServiceInterface.pathFromFQN + ".java"
  }

  def static String fileLocationCustomServiceInterface(Bm bm) {
    return bm.module.config.businessApi.path + "/src/main/java/" +
      bm.canonicalNameCustomServiceInterface.pathFromFQN + ".java"
  }
}
