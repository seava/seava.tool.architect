package ro.seava.tool.architect.business.generator.controllers.db

import static extension ro.seava.tool.architect.business.extensions.Bm_Ext.*

import ro.seava.tool.architect.business.business.BmAttrField
import ro.seava.tool.architect.business.business.BmRefField
import ro.seava.tool.architect.business.generator.models.db.Column

class MysqlController extends AbstractDbController {

  override String getDbEngine() {
    return "mysql"
  }

  override protected resolveDataType(BmAttrField f, Column c) {
    var dd = f.dataDomain
    var dt = dd.dataType
    c.dateType = dt.mysqlType

    if (dt.mysqlTypeWithLength) {
      c.length = dd.maxLength
      if (dd.precision != 0)
        c.precision = dd.precision
    }
  }

  override protected resolveDataType(BmRefField f, Column c) {
    var idfield = f.bm.idField
    var dd = idfield.dataDomain
    var dt = dd.dataType
    c.dateType = dt.mysqlType

    if (dt.mysqlTypeWithLength) {
      c.length = dd.maxLength
      if (dd.precision != 0)
        c.precision = dd.precision
    }
  }

}
