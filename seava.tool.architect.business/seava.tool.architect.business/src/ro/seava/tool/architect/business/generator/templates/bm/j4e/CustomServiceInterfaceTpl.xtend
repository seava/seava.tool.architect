package ro.seava.tool.architect.business.generator.templates.bm.j4e

import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*

import ro.seava.tool.architect.business.generator.templates.AbstractBusinessTemplate
import ro.seava.tool.architect.business.business.Bm

class CustomServiceInterfaceTpl extends AbstractBusinessTemplate {

  /**
   * 
   */
  def doJavaClass(Bm bm) {
    var CharSequence body = bm.body_tpl;

    '''
      «copyright»
      package «bm.packageCustomServiceInterface»;
      
      «imports»
      «body»
    '''
  }

  def body_tpl(Bm bm) {
    '''
      
      /**
       * Interface to expose business functions specific for {@link «bm.simpleNameModel»} domain
       * entity.
       */
      public interface «bm.simpleNameServiceInterface»«bm.extends_tpl» {
      }
    '''
  }

  /**
   * Extended interface template
   */
  def private extends_tpl(Bm bm) {
    if (bm.hasGeneratedServiceInterface) {
      ''' extends «bm.canonicalNameGeneratedServiceInterface.simpleNameFromFQN»'''
    } else {
      ''' extends «"seava.lib.j4e.api.service.business.IEntityService".simpleNameFromFQN»<«bm.canonicalNameModel.
        simpleNameFromFQN»>'''
    }
  }

}
