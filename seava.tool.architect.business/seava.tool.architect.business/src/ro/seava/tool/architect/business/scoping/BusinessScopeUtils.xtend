package ro.seava.tool.architect.business.scoping

import java.util.List
import ro.seava.tool.architect.business.business.Bm
import java.util.ArrayList
import ro.seava.tool.architect.business.business.IBmMember

/**
 * Helper methods to collect business model related elements
 */
class BusinessScopeUtils {

  /**
   * Collect all members of the given type
   */
  def static <T> List<T> collect_members_as_type(Bm bm, Class<T> clazz) {
    return collect_members_as_type(bm, clazz, false);
  }

  /**
     * Collect declared/all members of the given type
     */
  def static <T> List<T> collect_members_as_type(Bm bm, Class<T> clazz, boolean declaredMembersOnly) {
    var List<T> collector = new ArrayList<T>();
    collect_members_as_type(bm, collector, clazz, declaredMembersOnly);
    return collector;
  }

  /**
   * 
   */
  def static private <T> void collect_members_as_type(Bm bm, List<T> collector, Class<T> clazz,
    boolean declaredMembersOnly) {

    for (e : bm.members.filter(typeof(IBmMember)).filter(
      e|clazz.isAssignableFrom(e.eClass.instanceClass)
    )) {
      if (!collector.exists[element | (element as IBmMember).name.matches(e.name) ]) {
        collector.add(e as T);
      }      
    }

    if (!declaredMembersOnly && bm.superType != null) {
      collect_members_as_type(bm.superType, collector, clazz, declaredMembersOnly)
    }
  }

  // ============================
  /**
   * Collect all members of the given type
   */
  def static List<IBmMember> collect_members(Bm bm, Class<?> clazz) {
    return collect_members(bm, clazz, false);
  }

  /**
   * Collect declared/all members of the given type
   */
  def static List<IBmMember> collect_members(Bm bm, Class<?> clazz, boolean declaredMembersOnly) {
    var List<IBmMember> collector = new ArrayList<IBmMember>();
    collect_members(bm, collector, clazz, declaredMembersOnly);
    return collector;
  }

  /**
   * 
   */
  def static private void collect_members(Bm bm, List<IBmMember> collector, Class<?> clazz,
    boolean declaredMembersOnly) {

    collector.addAll(bm.members.filter(typeof(IBmMember)).filter(e|clazz.isAssignableFrom(e.eClass.instanceClass)))
    if (!declaredMembersOnly && bm.superType != null) {
      collect_members(bm.superType, collector, clazz, declaredMembersOnly)
    }
  }
}
