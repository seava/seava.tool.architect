package ro.seava.tool.architect.business.extensions

import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.business.business.B_Package

class Package_Ext {

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  /**
   * File location of spring beans configuration file for entity services
   * 
   * @param e
   * @return
   */
  def static String fileLocationSpringBeansXml(B_Package pck) {
    var s = pck.module.name;
    if (pck.unit != null) {
      s = s + "-" + pck.unit.name;
      if (pck.subUnit != null) {
        s = s + "-" + pck.subUnit;
      }
    }
    s = s + "-business"
    return pck.module.config.business.path + "/src/main/resources/META-INF/spring/beans/" + s + ".xml";
  }

}
