package ro.seava.tool.architect.business.generator.models.db;

import java.util.ArrayList;
import java.util.List;

public class PrimaryKey {

	/**
	 * Constraint name
	 */
	private String name;

	/**
	 * Constraint column names
	 */
	private List<String> columnNames = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

}
