package ro.seava.tool.architect.business.generator.templates.bm.j4e

import static extension ro.seava.tool.architect.business.extensions.Bm_Ext.*

import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.generator.templates.AbstractBusinessTemplate
import ro.seava.tool.architect.business.business.BmRefField
import ro.seava.tool.architect.business.business.BmFindByMethod

class GeneratedServiceTpl extends AbstractBusinessTemplate {

  /**
   * 
   */
  def doJavaClass(Bm bm) {
    var CharSequence body = bm.body_tpl;

    '''
      «copyright»
      package «bm.packageGeneratedService»;
      
      «imports»
      «body»
    '''
  }

  def body_tpl(Bm bm) {
    var serviceName = bm.simpleNameService
    '''
      
      /**
       * Service class implementation for {@link «bm.simpleNameModel»} domain
       * entity. <br>
       * Contains repository functionality with finder methods as well as specific business functionality
       */
      public class «serviceName»«bm.extends_tpl»«bm.implements_tpl» {
        
        public «serviceName»() {
          super();
        }
        
        public «serviceName»(«simpleNameFromFQN("javax.persistence.EntityManager")» em) {
          super();
          this.setEntityManager(em);
        }
        
        @Override
        public Class<«bm.simpleNameModel»> getEntityClass() {
          return «bm.simpleNameModel».class;
        }
        «bm.findby_methods_tpl»
      }
    '''
  }

  def private findby_methods_tpl(Bm bm) {
    if (bm.findByMethods.size > 0) {
      '''
        «FOR m : bm.findByMethods»
          «m.findby_method_tpl»
        «ENDFOR»
      '''
    }
  }

  def private findby_method_tpl(BmFindByMethod m) {
    '''
      «m.findby_method_declaration_tpl(false)» {
        «m.findby_method_body_tpl(false)»
      }
      «IF m.fields.exists[t|t instanceof BmRefField]»
        «m.findby_method_declaration_tpl(true)» {
          «m.findby_method_body_tpl(true)»
        }
      «ENDIF»
    '''
  }

  def private findby_method_body_tpl(BmFindByMethod m, boolean refAsId) {
    if (refAsId) {
      var bm = m.eContainer as Bm;
      var boolean hasClient = bm.hasClientIdField;
      '''
        return (List<«bm.canonicalNameModel.simpleNameFromFQN»>) this
              .getEntityManager()
              .createQuery(
                  "select e from «bm.alias» e where«IF hasClient» e.clientId = :clientId and«ENDIF» «m.
          findby_method_where_tpl»" ,
                  «bm.name».class)
              «FOR f : m.fields»
                «var _n = f.name»
                «IF f instanceof BmRefField»
                  .setParameter("«_n»Id", «_n»Id)
                «ELSE»
                  .setParameter("«_n»", «_n»)  
                «ENDIF»
              «ENDFOR»
              «IF hasClient»
                .setParameter("clientId",
                  «"seava.lib.j4e.api.base.session.Session".simpleNameFromFQN».user.get().getClient().getId())
              «ENDIF»
              .getResultList();
      '''
    } else {
      return '''return this.«m.findby_method_name_tpl»(«FOR f : m.fields»«f.name»«IF f instanceof BmRefField».getId()«ENDIF»«ENDFOR»);'''
    }
  }

  /**
   * 
   */
  def private findby_method_where_tpl(BmFindByMethod m) {
    var CharSequence result = '''''';
    for (f : m.fields) {
      var _n = f.name

      if (f instanceof BmRefField) {
        _n = _n + "Id"
      }
      result = result + '''e.«f.name»«IF f instanceof BmRefField».id«ENDIF» = :«_n»'''
    }
    return result
  }

  /**
   * Extended classes template
   */
  def private extends_tpl(Bm bm) {
    ''' extends «simpleNameFromFQN("seava.lib.j4e.business.service.entity.AbstractEntityService")»<«bm.
      canonicalNameModel().simpleNameFromFQN»>'''
  }

  /**
   * Implemented interfaces template
   */
  def private implements_tpl(Bm bm) {
    '''«IF !bm.hasCustomService» implements «bm.canonicalNameGeneratedServiceInterface.simpleNameFromFQN»«ENDIF»'''
  }

}
