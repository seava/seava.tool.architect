package ro.seava.tool.architect.business.extensions

import static extension ro.seava.tool.architect.business.scoping.BusinessScopeUtils.*

import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.business.BmPk
import ro.seava.tool.architect.business.business.BmAttrField
import java.util.List
import ro.seava.tool.architect.business.generator.models.JavaClass
import java.util.ArrayList
import ro.seava.tool.architect.business.business.BmIdPk
import ro.seava.tool.architect.business.business.IBmField

class Bm_Ext extends Bm_Cfg {

  // ================= Storage / DB =================
  /**
   * 
   */
  def static String tableName(Bm bm) {
    if (bm.source != null) {
      return bm.source.toUpperCase;
    } else {
      return ""
    }
  }

  /**
   * 
   */
  def static boolean hasOwnTable(Bm bm) {
    if (bm.isIsAbstract()) {
      return false;
    }
    return true;

  //    return (bm.getSuperType() == null)
  //        || (bm.getSuperType().isIsAbstract())
  //        || (bm.getSuperType().getInheritance() != null && !e
  //            .getSuperType().getInheritance().getType().getLiteral()
  //            .matches("SINGLE_TABLE"));
  }

  // =========================== ID  ==================================
  /**
   * 
   */
  def static boolean hasIdPk(Bm bm) {
    return bm.getIdPk != null;
  }

  /**
   * 
   */
  def static boolean hasIdField(Bm bm) {
    return (bm.getIdPk != null)
  }

  /**
   * 
   */
  def static boolean hasDeclaredIdField(Bm bm) {
    var list = bm.members.filter(typeof(BmIdPk))
    if (list.size > 0) {
      return true
    }
    return false
  }

  /**
   * 
   */
  def static BmAttrField getIdField(Bm bm) {
    var idPk = bm.getIdPk;
    if (idPk != null) {
      return (idPk.field as BmAttrField);
    }
  }

  /**
   * 
   */
  def static boolean hasClientIdField(Bm bm) {
    return bm.collect_members_as_type(BmAttrField).findFirst([t|t.name.matches("clientId")]) != null;
  }

  /**
   * 
   */
  def static boolean hasDeclaredClientIdField(Bm bm) {
    return bm.members.filter(typeof(BmAttrField)).findFirst([t|t.name.matches("clientId")]) != null;
  }

  /**
   * 
   */
  def static BmPk getPk(Bm bm) {
    var list = bm.members.filter(typeof(BmPk))
    if (list.size > 0) {
      return list.get(0)
    } else {
      var _e = bm;
      while (_e.superType != null) {
        _e = _e.superType
        list = _e.members.filter(typeof(BmPk))
        if (list.size > 0) {
          return list.get(0)
        }
      }
    }
    return null
  }

  /**
   * 
   */
  def static boolean containsField(BmPk pk, IBmField field) {
    if (pk != null) {
      for (f : pk.fields) {
        if (f == field) {
          return true
        }
      }
    }
    return false
  }

  /**
   * 
   */
  def static BmIdPk getIdPk(Bm bm) {
    var list = bm.members.filter(typeof(BmIdPk))

    if (list.size > 0) {
      return list.get(0)
    } else {
      var _e = bm;
      while (_e.superType != null) {
        _e = _e.superType
        list = _e.members.filter(typeof(BmIdPk))
        if (list.size > 0) {
          return list.get(0)
        }
      }
    }
    return null
  }

  /**
   * Return a list of implemented interfaces as a JavaClass list
   */
  def static List<JavaClass> getImplementedInterfaces(Bm bm) {
    var List<JavaClass> interfaces = new ArrayList<JavaClass>();
    if (bm.hasDeclaredIdField) {
      var i = new JavaClass("seava.lib.j4e.api.base.descriptor.IModelWithId")
      i.types.add(new JavaClass(bm.idField.dataDomain.dataType.javaType))
      interfaces.add(i)
    }
    if (bm.hasDeclaredClientIdField) {
      interfaces.add(new JavaClass("seava.lib.j4e.api.base.descriptor.IModelWithClientId"))
    }
    return interfaces
  }

}
