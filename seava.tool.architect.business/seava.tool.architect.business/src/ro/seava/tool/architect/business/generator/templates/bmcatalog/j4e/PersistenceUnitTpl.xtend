package ro.seava.tool.architect.business.generator.templates.bmcatalog.j4e

import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*
import static extension ro.seava.tool.architect.business.extensions.Pu_Ext.*

import ro.seava.tool.architect.business.business.Pu
import java.util.Set
import java.util.HashSet

class Pu_Tpl {

  /**
   * Generate persistence unit file content
   */
  def doPersistenceXml(Pu pu) {
    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <persistence xmlns="http://java.sun.com/xml/ns/persistence"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_1_0.xsd"
        version="1.0">
        
        <persistence-unit name="«pu.name»" transaction-type="RESOURCE_LOCAL">
          <provider>org.eclipse.persistence.jpa.PersistenceProvider</provider>
          
          <mapping-file>META-INF/«pu.fileNameMappingXml»</mapping-file>
          «FOR rpu : pu.requiredUnits»
            <mapping-file>META-INF/«rpu.fileNameMappingXml»</mapping-file>
          «ENDFOR»
          <exclude-unlisted-classes>true</exclude-unlisted-classes>
          <properties>
            <property name="eclipselink.weaving" value="static" />
          </properties>
        </persistence-unit>
        
      </persistence>
    '''
  }

  /**
   * Generate entity mapping file content
   */
  def doMappingXml(Pu pu) {

    // prepare a list of entity class canonical names 
    var Set<String> entities = new HashSet<String>();
    for (bm : pu.items) {
      entities.add(bm.canonicalNameModel)
    }

    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <entity-mappings
        xsi:schemaLocation="http://www.eclipse.org/eclipselink/xsds/persistence/orm xsd/eclipselink_orm_1_0.xsd"
        xmlns="http://www.eclipse.org/eclipselink/xsds/persistence/orm"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0">
        
        «FOR n : entities.sort»
          <entity class="«n»" />
        «ENDFOR»
        
      </entity-mappings>
    '''
  }

}
