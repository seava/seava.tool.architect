package ro.seava.tool.architect.business.generator.models.db;

import java.util.ArrayList;
import java.util.List;

public class ForeignKey {

	/**
	 * Constraint name
	 */
	private String name;

	/**
	 * Constraint column names
	 */
	private List<String> columnNames = new ArrayList<String>();

	/**
	 * Referenced table name
	 */
	private String referencedTableName;

	/**
	 * Referenced column names
	 */
	private List<String> referencedColumnNames = new ArrayList<String>();

	private boolean cascadeDelete;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

	public String getReferencedTableName() {
		return referencedTableName;
	}

	public void setReferencedTableName(String referencedTableName) {
		this.referencedTableName = referencedTableName;
	}

	public List<String> getReferencedColumnNames() {
		return referencedColumnNames;
	}

	public void setReferencedColumnNames(List<String> referencedColumnNames) {
		this.referencedColumnNames = referencedColumnNames;
	}

	public boolean isCascadeDelete() {
		return cascadeDelete;
	}

	public void setCascadeDelete(boolean cascadeDelete) {
		this.cascadeDelete = cascadeDelete;
	}

}
