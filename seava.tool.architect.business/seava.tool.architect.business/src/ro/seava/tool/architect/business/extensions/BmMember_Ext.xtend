package ro.seava.tool.architect.business.extensions

import static extension ro.seava.tool.architect.business.scoping.BusinessScopeUtils.*
import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*

import ro.seava.tool.architect.business.business.BmUk
import ro.seava.tool.architect.business.business.BmRefField
import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.business.IBmField
import ro.seava.tool.architect.business.business.BmAttrField

class BmMember_Ext {

  /**
   * Check if an UK rule contains a reference field
   */
  def static boolean containsRefField(BmUk uk) {
    return uk.fields.filter(typeof(BmRefField)).size > 0;
  }

  def static boolean hasCascadeDelete(BmRefField f) {
    f.cascade.exists(t|t.getName().matches("ALL") || t.getName().matches("REMOVE"));
  }

  /**
   * Check if a member is part of a many to many relation 
   */
  def static boolean isManyToMany(BmRefField elem) {
    if (elem.collection) {
      var ownerBm = elem.eContainer as Bm;
      var members = elem.bm.collect_members_as_type(BmRefField)
      for (m : members) {
        if (m.bm == ownerBm && m.collection) {
          if (elem.childField == null || elem.childField == m) {
            return true
          }
        }
      }
    }
    return false;
  }

  /**
   * Check if a member is part of a many to one relation 
   */
  def static boolean isManyToOne(BmRefField elem) {
    if (elem.collection) {
      var ownerBm = elem.eContainer as Bm;
      var members = elem.bm.collect_members_as_type(BmRefField)
      for (m : members) {
        if (m.bm == ownerBm && !m.collection) {
          if (elem.childField == null || elem.childField == m) {
            return true
          }
        }
      }
    }
    return false;
  }

  def static BmRefField resolveMappedChildField(BmRefField elem) {
    if (elem.childField != null) {
      return elem.childField;
    } else {
      var ownerBm = elem.eContainer as Bm;
      var members = elem.bm.collect_members_as_type(BmRefField)
      for (m : members) {
        if (m.bm == ownerBm) {
          return m
        }
      }
      return null;
    }
  }

  def static String refOppositeFieldName(BmRefField e) {
    if (e.getChildField() != null) {
      return e.getChildField().getName();
    } else {
      return e.resolveMappedChildField.name;
    }
  }

  def static String multiJoinTableColumnName(BmRefField e) {
    if (e.config.source != null) {
      return e.config.source;
    } else {
      return e.name.toUpperCase + "_ID";
    }
  }

  def static String multiJoinTableOppositeColumnName(BmRefField f) {
    if (f.childField != null) {
      return f.childField.multiJoinTableColumnName
    } else {
      return f.resolveMappedChildField.multiJoinTableColumnName
    }
  }

  def static String columnName(IBmField f) {
    if (f instanceof BmAttrField) {
      return (f as BmAttrField).columnNameAttr;
    } else {
      return (f as BmRefField).columnNameRef;
    }
  }

  def static String columnNameAttr(BmAttrField f) {
    if (f.config.source != null) {
      return f.config.source.toUpperCase();
    } else {
      return f.name.toUpperCase();
    }
  }

  def static String columnNameRef(BmRefField f) {
    if (f.config.source != null) {
      return f.config.source.toUpperCase;
    } else {
      return f.name.toUpperCase + "_ID";
    }
  }

  def static boolean isAttrField(IBmField f) {
    return f instanceof BmAttrField;
  }

  def static boolean isRefField(IBmField f) {
    return f instanceof BmRefField;
  }

  def static String getDataTypeFqn(IBmField f) {
    if (f instanceof BmAttrField) {
      return (f as BmAttrField).dataDomain.dataType.javaType
    } else if (f instanceof BmRefField) {
      return (f as BmRefField).bm.canonicalNameModel
    }
  }

}
