package ro.seava.tool.architect.business.generator.models.db;

import java.util.ArrayList;
import java.util.List;

public class Table {

	/**
	 * File full name where the generated table code is written
	 */
	private String fileLocationTable;

	/**
	 * File full name where the generated foreign keys code is written
	 */
	private String fileLocationFk;

	/**
	 * Is abstract table? It is not generated as table, only columns information
	 * is used
	 */
	private boolean isAbstract;

	/**
	 * Constraint name
	 */
	private String name;

	/**
	 * Constraint column names
	 */
	private List<Column> columns = new ArrayList<Column>();

	/**
	 * Temporary table ?
	 */
	private boolean temporary;

	/**
	 * Table primary key
	 */
	private PrimaryKey primaryKey;

	/**
	 * List of unique keys
	 */
	private List<UniqueKey> uniqueKeys = new ArrayList<UniqueKey>();

	/**
	 * List of foreign keys
	 */
	private List<ForeignKey> foreignKeys = new ArrayList<ForeignKey>();

	/**
	 * List of indexes
	 */
	private List<Index> indexes = new ArrayList<Index>();

	/* ====================== HELPER FUNCTIONS ====================== */

	/**
	 * 
	 * @param bmFieldName
	 * @return
	 */
	public Column getColumnByBmFieldName(String bmFieldName) {
		for (Column c : this.columns) {
			if (c.getBmFieldName().matches(bmFieldName)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasUniqueKeys() {
		return this.uniqueKeys.size() > 0;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasForeignKeys() {
		return this.foreignKeys.size() > 0;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasIndexKeys() {
		return this.indexes.size() > 0;
	}

	/* ====================== GETTERS-SETTERS ====================== */

	public String getFileLocationTable() {
		return fileLocationTable;
	}

	public void setFileLocationTable(String fileLocationTable) {
		this.fileLocationTable = fileLocationTable;
	}

	public String getFileLocationFk() {
		return fileLocationFk;
	}

	public void setFileLocationFk(String fileLocationFk) {
		this.fileLocationFk = fileLocationFk;
	}

	public boolean isAbstract() {
		return isAbstract;
	}

	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public boolean isTemporary() {
		return temporary;
	}

	public void setTemporary(boolean temporary) {
		this.temporary = temporary;
	}

	public PrimaryKey getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(PrimaryKey primaryKey) {
		this.primaryKey = primaryKey;
	}

	public List<UniqueKey> getUniqueKeys() {
		return uniqueKeys;
	}

	public void setUniqueKeys(List<UniqueKey> uniqueKeys) {
		this.uniqueKeys = uniqueKeys;
	}

	public List<ForeignKey> getForeignKeys() {
		return foreignKeys;
	}

	public void setForeignKeys(List<ForeignKey> foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	public List<Index> getIndexes() {
		return indexes;
	}

	public void setIndexes(List<Index> indexes) {
		this.indexes = indexes;
	}

}
