package ro.seava.tool.architect.business.generator.templates.bm.j4e

import static extension ro.seava.tool.architect.base.extensions.Base_Ext.*
import static extension ro.seava.tool.architect.business.extensions.Bm_Ext.*
import static extension ro.seava.tool.architect.business.extensions.BmMember_Ext.*
import static extension ro.seava.tool.architect.business.scoping.BusinessScopeUtils.*

import ro.seava.tool.architect.business.generator.templates.AbstractBusinessTemplate
import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.generator.models.JavaClass
import ro.seava.tool.architect.business.business.IBmField
import ro.seava.tool.architect.business.business.BmAttrField
import ro.seava.tool.architect.base.base.DataType
import ro.seava.tool.architect.business.business.BmIdPk
import ro.seava.tool.architect.business.business.E_IdType
import ro.seava.tool.architect.business.business.BmRefField
import ro.seava.tool.architect.business.business.BmUk
import java.util.List
import ro.seava.tool.architect.base.base.E_DataType

class GeneratedModelTpl extends AbstractBusinessTemplate {

  /**
   * 
   */
  def doJavaClass(Bm bm) {
    var CharSequence body = bm.body_tpl;

    '''
      «copyright»
      package «bm.packageGeneratedModel»;
      
      «imports»
      «IF bm.imports.size > 0»
        «FOR i : bm.imports»
          import «i»;
        «ENDFOR»
      «ENDIF»
      «body»
    '''
  }

  /**
   * Build the class body. In the process the imports list is constructed 
   */
  def protected body_tpl(Bm bm) {
    '''   
      
      «bm.description»
      «bm.class_annotations_tpl»
      «bm.class_named_queries_tpl»
      public«IF bm.isAbstract» abstract«ENDIF» class «bm.name»«bm.class_extends_tpl»«bm.class_implements_tpl» {
        «bm.static_fields_tpl»
        «bm.fields_tpl»
        «bm.accessors_tpl»
        «bm.functions_tpl»
        «bm.default_values_tpl»
      }
    '''
  }

  /**
   * 
   */
  def protected CharSequence class_reference_tpl(JavaClass c) {
    '''«c.fqn.simpleNameFromFQN»«IF c.parameterized»<«FOR t : c.types SEPARATOR ', '»«t.class_reference_tpl»«ENDFOR»>«ENDIF»'''
  }

  /**
   * Extended classes template
   */
  def protected class_extends_tpl(Bm bm) {
    if (bm.superType != null) {
      ''' extends «bm.superType.canonicalNameModel.simpleNameFromFQN»'''
    }
  }

  /**
   * Implemented interfaces template
   */
  def protected class_implements_tpl(Bm bm) {
    var interfaces = bm.implementedInterfaces
    var superType = bm.superType;

    if (superType == null || interfaces.size > 0) {
      ''' implements«IF superType == null» «"java.io.Serializable".simpleNameFromFQN»«IF interfaces.size > 0»,«ENDIF»«ENDIF»«FOR fqn : interfaces SEPARATOR ','» «fqn.
        class_reference_tpl»«ENDFOR»'''
    }
  }

  /**
   * Static fields template
   */
  def protected static_fields_tpl(Bm bm) {
    '''
      
      private static final long serialVersionUID = -8865917134914502125L;
      «IF !bm.isAbstract && bm.source != null»
        
        public static final String ALIAS = "«bm.alias»";
        
        public static final String TABLE_NAME = "«bm.source»";
        «FOR uk : bm.collect_members_as_type(BmUk)»
          /**
           * Named query find by unique key: «uk.name.replaceFirst("uk_", "").toFirstUpper».
           */
          public static final String NQ_FIND_BY_«uk.name.replaceFirst("uk_", "").toUpperCase()» = "«bm.name».findBy«uk.
        name.replaceFirst("uk_", "").toFirstUpper()»";
          «IF uk.containsRefField»
            /**
             * Named query find by unique key: «uk.name.replaceFirst("uk_", "").toFirstUpper()» using the ID field for references.
             */
            public static final String NQ_FIND_BY_«uk.name.replaceFirst("uk_", "").toUpperCase()»_PRIMITIVE = "«bm.name».findBy«uk.
        name.replaceFirst("uk_", "").toFirstUpper()»_PRIMITIVE";
          «ENDIF»
        «ENDFOR»  
      «ENDIF»
          
    '''
  }

  /**
   * Fields template
   */
  def protected fields_tpl(Bm bm) {
    '''
      «FOR f : bm.members.filter(typeof(IBmField))»
        «f.field_tpl»
      «ENDFOR»
    '''
  }

  /**
   * Field template
   */
  def protected field_tpl(IBmField f) {
    if (f instanceof BmAttrField) {
      return (f as BmAttrField).field_attr_field_tpl
    } else {
      var rf = f as BmRefField;
      if (rf.collection) {
        return rf.field_ref_collection_field_tpl
      } else {
        return rf.field_ref_field_tpl
      }
    }
  }

  /**
   * 
   */
  def protected field_ref_field_tpl(BmRefField f) {
    '''  
      
      @«simpleNameFromFQN("javax.persistence.ManyToOne")»(fetch=«simpleNameFromFQN("javax.persistence.FetchType")».LAZY, targetEntity=«f.
        bm.name».class«IF f.childField != null», mappedBy="«f.childField.name»"«ENDIF»)
      @«simpleNameFromFQN("javax.persistence.JoinColumn")»(name="«f.columnName»", referencedColumnName="«f.bm.idField.
        columnName»")
      private «f.bm.canonicalNameModel.simpleNameFromFQN» «f.name»;
    '''
  }

  /**
   * 
   */
  def protected field_ref_collection_field_tpl(BmRefField f) {
    '''
      
      «IF f.isManyToMany»
        @«simpleNameFromFQN("javax.persistence.ManyToMany")»«IF !f.isOwnerSide && f.resolveMappedChildField() != null»(mappedBy="«f.
        refOppositeFieldName()»")«ELSE»
        @«simpleNameFromFQN("javax.persistence.JoinTable")»(name="«f.joinSource»", joinColumns={@«simpleNameFromFQN(
        "javax.persistence.JoinColumn")»(name="«f.multiJoinTableOppositeColumnName()»")}, inverseJoinColumns={@«simpleNameFromFQN(
        "javax.persistence.JoinColumn")»(name="«f.multiJoinTableColumnName()»")})«ENDIF»
      «ELSE»
        @«simpleNameFromFQN("javax.persistence.OneToMany")»(fetch=«simpleNameFromFQN("javax.persistence.FetchType")».LAZY, targetEntity=«f.
        bm.name».class«IF f.resolveMappedChildField() != null», mappedBy="«f.refOppositeFieldName()»"
        «ENDIF»
        «IF f.cascade.size == 1»
        ,cascade=«simpleNameFromFQN("javax.persistence.CascadeType")».«f.cascade.get(0)»«ENDIF»)
      «ENDIF»
      «IF f.hasCascadeDelete()»
        @«simpleNameFromFQN("org.eclipse.persistence.annotations.CascadeOnDelete")»
      «ENDIF»    
      private «"java.util.Collection".simpleNameFromFQN»<«f.bm.canonicalNameModel.simpleNameFromFQN»> «f.name»;
    '''
  }

  /**
   * 
   */
  def protected field_attr_field_tpl(BmAttrField f) {
    '''
      
      «IF !f.config.transient»
        @«simpleNameFromFQN("javax.persistence.Column")»(name="«f.columnName»"«f.field_column_annotation_tpl»)
      «ELSE»
        @«simpleNameFromFQN("javax.persistence.Transient")»
      «ENDIF»
      «IF f instanceof BmAttrField»«(f as BmAttrField).field_attr_field_annotations_tpl»«ENDIF»
      private «f.dataTypeFqn.simpleNameFromFQN» «f.name»;
    '''
  }

  /**
   * 
   */
  def protected field_attr_field_annotations_tpl(BmAttrField f) {
    var DataType dt = f.dataDomain.dataType

    '''      
      «IF f.name.matches("version")»
        @«"javax.persistence.Version".simpleNameFromFQN»
      «ELSEIF f.name.matches("id")»        
        «var idPk = (f.eContainer as Bm).idPk»
        @«"javax.persistence.Id".simpleNameFromFQN»
        «IF idPk != null»
          @«"javax.persistence.GeneratedValue".simpleNameFromFQN»(«idPk.id_strategy_tpl»)
        «ENDIF»
      «ELSE»
        «var pk = (f.eContainer as Bm).pk»
        «IF pk.containsField(f)»
          @«"javax.persistence.Id".simpleNameFromFQN»
        «ENDIF»
      «ENDIF»
      «IF dt.date»
        «IF f.dataDomain.dataType.name.matches("Date")»
          @«simpleNameFromFQN("javax.persistence.Temporal")»(«simpleNameFromFQN("javax.persistence.TemporalType")».DATE)
        «ENDIF»
        «IF f.dataDomain.dataType.name.matches("Time")»
          @«simpleNameFromFQN("javax.persistence.Temporal")»(«simpleNameFromFQN("javax.persistence.TemporalType")».TIME)
        «ENDIF»
        «IF f.dataDomain.dataType.name.matches("DateTime") || f.dataDomain.dataType.name.matches("Timestamp")»
          @«simpleNameFromFQN("javax.persistence.Temporal")»(«simpleNameFromFQN("javax.persistence.TemporalType")».TIMESTAMP)
        «ENDIF»
      «ENDIF»
    '''
  }

  /**
   * 
   */
  def protected id_strategy_tpl(BmIdPk idPk) {
    if (idPk.type == E_IdType.UUID) {
      return '''generator = «simpleNameFromFQN("seava.lib.j4e.api.Constants")».UUID_GENERATOR_NAME'''
    } else {
      return '''strategy = «simpleNameFromFQN("javax.persistence.GenerationType")».«idPk.type.getName()»«IF idPk.
        generator != null»,generator = «idPk.generator»«ENDIF»'''
    }
  }

  /**
   * Field column annotations template
   */
  def protected field_column_annotation_tpl(IBmField f) {
    '''«IF f.config.noInsert», insertable=false«ENDIF»«IF f.config.noUpdate», updatable=false«ENDIF»«IF f.config.
      isRequired», nullable=false«ENDIF»'''
  }

  /**
   * Getters-setters template
   */
  def protected accessors_tpl(Bm bm) {
    '''
      «FOR f : bm.members.filter(typeof(IBmField))»
        «f.accessor_tpl»
      «ENDFOR»
    '''
  }

  /**
   * Getters-setter template
   */
  def protected accessor_tpl(IBmField f) {
    if (f instanceof BmRefField) {
      var rf = f as BmRefField;
      if (rf.isCollection) {
        rf.accessor_ref_collection_field_tpl;
      } else {
        f.accessor_field_tpl;
      }
    } else if (f instanceof BmAttrField) {
      f.accessor_field_tpl;
    }

  }

  /**
   * Getters setters template
   */
  def protected accessor_field_tpl(IBmField f) {
    var type = f.dataTypeFqn.simpleNameFromFQN;
    var name = f.name;
    '''
      
      «IF f.config.transient»@Transient«ENDIF»
      public «type» get«name.toFirstUpper»() {
        «IF f.config.getterFn != null»
          «f.config.getterFn»
        «ELSE»
          return this.«name»;
        «ENDIF»
      }
      
      public void set«f.name.toFirstUpper»(«type» «name») {
        «IF f.config.setterFn != null»
          «f.config.setterFn»
        «ELSE»
          this.«name» = «name»;
        «ENDIF»
      }
    '''
  }

  /**
   * Getters setters template for collection type reference fields
   */
  def protected accessor_ref_collection_field_tpl(BmRefField f) {
    var type = f.bm.simpleNameModel;
    var name = f.name;
    var nameFirstUpper = name.toFirstUpper;
    '''
      
      public Collection<«type»> get«nameFirstUpper»() {
        return this.«name»;
      }
      
      public void set«nameFirstUpper»(Collection<«type»> «name») {
        this.«name» = «name»;
      }
      «IF !f.isManyToMany»
        
        public void addTo«nameFirstUpper»(«type» e) {
          if (this.«name» == null) {
            this.«name» = new «"java.util.ArrayList".simpleNameFromFQN»<«type»>();
          }
          e.set«f.refOppositeFieldName.toFirstUpper»(this);
          this.«name».add(e);
        }
      «ENDIF»
    '''
  }

  /**
   * Entity level annotations
   */
  def protected class_annotations_tpl(Bm bm) {
    var entityName = bm.simpleNameModel;
    '''
      «IF bm.isAbstract»
        @«"javax.persistence.MappedSuperclass".simpleNameFromFQN»
      «ELSE»
        @«"javax.persistence.Entity".simpleNameFromFQN»(name=«entityName».ALIAS)
        «IF bm.source != null»
          @«"javax.persistence.Table".simpleNameFromFQN»(name=«entityName».TABLE_NAME)
        «ENDIF» 
      «ENDIF»
      «IF bm.readOnly»
        @«"org.eclipse.persistence.annotations.ReadOnly".simpleNameFromFQN»
      «ENDIF»
      «IF bm.noCache»
        @«"org.eclipse.persistence.annotations.Cache".simpleNameFromFQN»(type=«"org.eclipse.persistence.annotations.CacheType".
        simpleNameFromFQN».NONE)
      «ENDIF»
    '''

  //          «IF e.table.hasUniqueKeys»
  //              ,uniqueConstraints={
  //                «FOR uk : e.table.uniqueKeys SEPARATOR ','»
  //                  @«simpleNameFromFQN("javax.persistence.UniqueConstraint")»( 
  //                    name=«e.name».TABLE_NAME+"«uk.name»"
  //                    ,columnNames={«FOR c : uk.columnNames SEPARATOR ','»"«c»"«ENDFOR»}
  //                  )
  //              «ENDFOR»          
  //              }
  //          «ENDIF»       
  }

  /**
   * Named queries declaration 
   */
  def protected class_named_queries_tpl(Bm bm) {
    if (!bm.isAbstract) {
      var List<BmUk> allUkRules = bm.collect_members_as_type(BmUk);
      var bmName = bm.name
      var hasClientId = bm.hasClientIdField
      if (allUkRules.size > 0) {
        '''
          @«simpleNameFromFQN("javax.persistence.NamedQueries")»({
            «FOR uk : allUkRules SEPARATOR ','»
              @«simpleNameFromFQN("javax.persistence.NamedQuery")»(
                name=«bmName».NQ_FIND_BY_«uk.name.replaceFirst("uk_", "").toUpperCase()»,
                query="SELECT e FROM "+«bmName».ALIAS+" e WHERE «IF hasClientId»e.clientId = :clientId and «ENDIF»«FOR f : uk.
            fields SEPARATOR " and "»e.«f.name» = :«f.name»«ENDFOR»",
                hints=@«simpleNameFromFQN("javax.persistence.QueryHint")»(name=«simpleNameFromFQN(
            "org.eclipse.persistence.config.QueryHints")».BIND_PARAMETERS, value=«simpleNameFromFQN(
            "org.eclipse.persistence.config.HintValues")».TRUE)
              )
              «IF uk.containsRefField()»
              ,@«simpleNameFromFQN("javax.persistence.NamedQuery")»(name=«bmName».NQ_FIND_BY_«uk.name.
            replaceFirst("uk_", "").toUpperCase()»_PRIMITIVE, query="SELECT e FROM "+«bmName».ALIAS+" e WHERE «IF hasClientId»e.clientId = :clientId and «ENDIF»«FOR f : uk.
            fields SEPARATOR " and "»«IF f instanceof BmRefField»e.«f.name».id = :«f.name»Id«ELSE»e.«f.name» = :«f.
            name»«ENDIF»«ENDFOR»", hints=@«simpleNameFromFQN("javax.persistence.QueryHint")»(name=«simpleNameFromFQN(
            "org.eclipse.persistence.config.QueryHints")».BIND_PARAMETERS, value=«simpleNameFromFQN(
            "org.eclipse.persistence.config.HintValues")».TRUE))
            «ENDIF»
            «ENDFOR»
          })
        '''
      }
    }
  }

  /**
   * Declaration of the entity methods
   */
  def protected functions_tpl(Bm bm) {
    '''
      «FOR fn : bm.functions»
        «var m = fn.method»
        
        «IF m.methodName.matches("preInsert")»
          @«"javax.persistence.PrePersist".simpleNameFromFQN»
        «ENDIF»
        «IF m.methodName.matches("preUpdate")»
          @«"javax.persistence.PreUpdate".simpleNameFromFQN»
        «ENDIF»
        public «m.service_method_return_type_tpl» «m.methodName»(«m.service_method_arguments_tpl») «IF m.
        throwsException»throws «simpleNameFromFQN("seava.lib.j4e.api.exceptions.ManagedException")»«ENDIF» {
           «fn.body»
        }
      «ENDFOR»
    '''
  }

  /**
   * Generate field default values based on the data-domain type
   */
  def protected CharSequence default_values_tpl(Bm bm) {
    '''
      
      protected void applyDefaultValues() {
        «IF bm.superType != null»
          super.applyDefaultValues();
        «ENDIF»
        «FOR f : bm.members.filter(typeof(BmAttrField))»
          «f.default_value_tpl»
        «ENDFOR»
      }
    '''
  }

  /**
   * 
   */
  def protected CharSequence default_value_tpl(BmAttrField f) {
    if (f.config.isRequired) {
      var dd = f.dataDomain;
      if (dd.defaultValue != null) {
        '''
          if (this.«f.name» == null«IF dd.dataType.type == E_DataType.STRING» || this.«f.name».equals("")«ENDIF») {
            this.«f.name» = «IF dd.dataType.type == E_DataType.STRING»"«dd.defaultValue»"«ELSE»«dd.defaultValue»«ENDIF»;
          }
        '''
      } else if (dd.defaultValueVar != null) {
        '''
          if (this.«f.name» == null«IF dd.dataType.type == E_DataType.STRING» || this.«f.name».equals("")«ENDIF») {
            this.«f.name» = «dd.defaultValueVar.javaExpression»;
          }
        '''
      }
    }

  }
}
