package ro.seava.tool.architect.business.generator.templates

import static extension ro.seava.tool.architect.business.extensions.Bm_Ext.*

import ro.seava.tool.architect.abstracts.templates.Abstract_Java_Tpl
import ro.seava.tool.architect.business.business.BmAttrField
import ro.seava.tool.architect.business.business.MethodArgument
import ro.seava.tool.architect.business.business.Method
import ro.seava.tool.architect.business.business.ApiTypeBm
import ro.seava.tool.architect.business.business.ApiTypeDataType
import ro.seava.tool.architect.business.business.ApiTypeString
import ro.seava.tool.architect.business.business.IApiType
import ro.seava.tool.architect.business.business.BmFindByMethod
import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.business.BmServiceMethod
import ro.seava.tool.architect.business.business.IBmField
import ro.seava.tool.architect.business.business.BmRefField

class AbstractBusinessTemplate extends Abstract_Java_Tpl {

  // ======================= findBy vmethods ==========================
  /**
   * 
   */
  def protected findby_method_declaration_tpl(BmFindByMethod m, boolean refAsId) {
    var bm = m.eContainer as Bm;
    if (refAsId) {
      '''
      
      public «"java.util.List".simpleNameFromFQN»<«bm.canonicalNameModel.simpleNameFromFQN»> «m.findby_method_name_tpl»(«m.
        findby_method_arguments_tpl(refAsId)») 
        throws «simpleNameFromFQN("seava.lib.j4e.api.base.exceptions.ManagedException")»'''
    } else {
      '''
      
      public «"java.util.List".simpleNameFromFQN»<«bm.canonicalNameModel.simpleNameFromFQN»> «m.findby_method_name_tpl»(«m.
        findby_method_arguments_tpl(refAsId)») 
        throws «simpleNameFromFQN("seava.lib.j4e.api.base.exceptions.ManagedException")»'''
    }
  }

  /**
   * 
   */
  def protected String findby_method_arguments_tpl(BmFindByMethod m, boolean refAsId) {
    '''«FOR f : m.fields SEPARATOR ","»«m.findby_method_argument_tpl(f, refAsId)»«ENDFOR»'''
  }

  /**
   * 
   */
  def protected String findby_method_argument_tpl(BmFindByMethod m, IBmField f, boolean refAsId) {
    '''«m.findby_method_argument_type_tpl(f, refAsId)» «f.name»«IF refAsId»Id«ENDIF»'''
  }

  /**
   * 
   */
  def protected String findby_method_argument_type_tpl(BmFindByMethod m, IBmField f, boolean refAsId) {
    if (f instanceof BmAttrField) {
      var _f = f as BmAttrField;
      return '''«_f.dataDomain.dataType.javaType.simpleNameFromFQN»'''
    } else if (f instanceof BmRefField) {
      var _f = f as BmRefField;
      if (refAsId) {
        return '''«_f.bm.idField.dataDomain.dataType.javaType.simpleNameFromFQN»'''
      } else {
        return '''«_f.bm.canonicalNameModel.simpleNameFromFQN»'''
      }
    }
  }

  /**
   * 
   */
  def protected String findby_method_name_tpl(BmFindByMethod m) {
    if (m.name != null) {
      return m.name
    } else {
      var StringBuffer sb = new StringBuffer("findBy")
      for (f : m.fields) {
        sb.append(f.name.toFirstUpper)
      }
      return sb.toString
    }
  }

  // ======================= service vmethods ==========================
  /**
   * 
   */
  def protected service_method_declaration_tpl(BmServiceMethod sm) {
    var m = sm.method;
    '''
    
    public «m.service_method_return_type_tpl» «m.methodName»(«m.service_method_arguments_tpl»)
      throws «simpleNameFromFQN("seava.lib.j4e.api.base.exceptions.ManagedException")»'''
  }

  /**
   * Method arguments
   */
  def protected service_method_arguments_tpl(Method m) {
    '''«FOR a : m.args SEPARATOR ', '»«a.service_method_argument_tpl»«ENDFOR»'''
  }

  /**
   * Template for one method argument
   */
  def protected service_method_argument_tpl(MethodArgument a) {
    return '''«a.type.api_type_tpl» «a.argName»'''
  }

  /**
 * Resolve method return type
 */
  def protected service_method_return_type_tpl(Method m) {
    if (m.returnType == null) {
      '''void'''
    } else {
      return m.returnType.api_type_tpl
    }
  }

  def protected api_type_tpl(IApiType t) {
    if (t instanceof ApiTypeBm) {
      return (t as ApiTypeBm).api_type_tpl
    } else if (t instanceof ApiTypeDataType) {
      return (t as ApiTypeDataType).api_type_tpl
    } else {
      return (t as ApiTypeString).api_type_tpl
    }
  }

  def protected api_type_tpl(ApiTypeBm t) {
    var type = "";
    var _type = t
    if (_type.field != null) {
      if (_type.field instanceof BmAttrField) {
        type = (_type.field as BmAttrField).dataDomain.dataType.javaType
      } else {
      }
    } else {
      type = _type.bm.canonicalNameModel
    }

    if (t.asCollection) {
      '''«simpleNameFromFQN("java.util.List")»<«type.simpleNameFromFQN»>'''
    } else {
      '''«type.simpleNameFromFQN»'''
    }
  }

  def protected api_type_tpl(ApiTypeDataType t) {
    if (t.asCollection) {
      '''«simpleNameFromFQN("java.util.List")»<«t.dataType.javaType.simpleNameFromFQN»>'''
    } else {
      '''«t.dataType.javaType.simpleNameFromFQN»'''
    }
  }

  def protected api_type_tpl(ApiTypeString t) {
    if (t.asCollection) {
      '''«simpleNameFromFQN("java.util.List")»<«t.fqn.simpleNameFromFQN»>'''
    } else {
      '''«t.fqn.simpleNameFromFQN»'''
    }
  }

}
