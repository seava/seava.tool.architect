package ro.seava.tool.architect.business.generator.models.db;

public class Column {

	/**
	 * Business-model field name
	 */
	private String bmFieldName;

	/**
	 * Column name
	 */
	private String name;

	/**
	 * 
	 */
	private String title;

	/**
	 * 
	 */
	private String description;

	/**
	 * Database engine specific data type
	 */
	private String dateType;

	/**
	 * 
	 */
	private Integer length;

	/**
	 * 
	 */
	private Integer precision;

	/**
	 * 
	 */
	private boolean required;

	/* ====================== HELPER FUNCTIONS ====================== */

	/**
	 * Returns how many length related information is available.<br>
	 * 0 - ex: date or boolean types <br>
	 * 1 - ex: string types<br>
	 * 2 - ex: numbers <br>
	 * 
	 * @return
	 */
	public int lengthDescriptorCount() {
		if (this.length == null) {
			return 0;
		} else {
			if (this.precision == null) {
				return 1;
			} else {
				return 2;
			}
		}
	}

	/* ====================== GETTERS-SETTERS ====================== */

	public String getBmFieldName() {
		return bmFieldName;
	}

	public void setBmFieldName(String bmFieldName) {
		this.bmFieldName = bmFieldName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getPrecision() {
		return precision;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

}
