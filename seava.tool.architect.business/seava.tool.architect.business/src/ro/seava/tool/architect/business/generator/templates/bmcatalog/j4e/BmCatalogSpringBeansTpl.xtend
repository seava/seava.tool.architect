package ro.seava.tool.architect.business.generator.templates.bmcatalog.j4e

import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*

import ro.seava.tool.architect.business.generator.templates.AbstractBusinessTemplate
import ro.seava.tool.architect.business.business.BmCatalog
import ro.seava.tool.architect.business.business.Bm

class BmCatalogSpringBeansTpl extends AbstractBusinessTemplate {

  def doBeansXml(BmCatalog c) {
    '''
      <?xml version="1.0" encoding="UTF-8"?>
      <beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
        xsi:schemaLocation="
            http://www.springframework.org/schema/beans
              http://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context 
              http://www.springframework.org/schema/context/spring-context.xsd">
        «FOR bm : c.items»
          «bm.bean_declaration_tpl»
        «ENDFOR»
      </beans>
    '''
  }

  def private bean_declaration_tpl(Bm bm) {
    if (bm.hasCustomService) {
      return bm.custom_bean_tpl
    } else if (bm.hasGeneratedService) {
      return bm.generated_bean_tpl
    } else {
      return bm.default_bean_tpl
    }
  }

  def private default_bean_tpl(Bm bm) {
    '''
        
      <bean id="«bm.alias»" scope="singleton" lazy-init="true"
        class="seava.lib.j4e.business.service.entity.DefaultEntityService">
          <constructor-arg name="entityClass" value="«bm.canonicalNameModel»"/>
      </bean>
    '''
  }

  def private generated_bean_tpl(Bm bm) {
    '''
       
      <bean id="«bm.alias»" scope="singleton" lazy-init="true"
        class="«bm.canonicalNameGeneratedService»"/>
    '''
  }

  def private custom_bean_tpl(Bm bm) {
    '''
      
      <bean id="«bm.alias»" scope="singleton" lazy-init="true"
        class="«bm.canonicalNameCustomService»"/>
    '''
  }

}
