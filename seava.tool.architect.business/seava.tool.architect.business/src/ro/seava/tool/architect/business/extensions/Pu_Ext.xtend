package ro.seava.tool.architect.business.extensions

import static extension ro.seava.tool.architect.project.extensions.Artifact_Ext.*

import ro.seava.tool.architect.business.business.Pu
import ro.seava.tool.architect.business.business.B_Package
import ro.seava.tool.architect.project.project.Module

class Pu_Ext {

  /* ====================== BM OWNERS ====================== */
  /**
   * Get containing package
   */
  def static B_Package getPackage(Pu pu) {
    return pu.eContainer as B_Package;
  }

  /**
   * Get containing module
   */
  def static Module getModule(Pu pu) {
    return pu.package.module;
  }

  /* ====================== GENERATED FILE LOCATIONS ====================== */
  /**
   * File location of persistence.xml
   * 
   * @param e
   * @return
   */
  def static String fileLocationPersistenceXml(Pu pu) {
    return pu.module.config.domain.path + "/src/main/resources/META-INF/" + pu.fileNamePersistenceXml
  }

  /**
   * File location of persistence.xml
   * 
   * @param e
   * @return
   */
  def static String fileLocationMappingXml(Pu pu) {
    return pu.module.config.domain.path + "/src/main/resources/META-INF/" + pu.fileNameMappingXml
  }

  /* ====================== SIMPLE FILE-NAMES ====================== */
  /**
   * File name of persistence.xml
   * 
   * @param e
   * @return
   */
  def static String fileNamePersistenceXml(Pu pu) {
    return '''persistence.xml'''
  }

  /**
   * File name of orm-mapping.xml
   * 
   * @param e
   * @return
   */
  def static String fileNameMappingXml(Pu pu) {
    var module = pu.module
    return '''«module.vendor.name»-«module.name»-domain-orm.xml'''
  }

}
