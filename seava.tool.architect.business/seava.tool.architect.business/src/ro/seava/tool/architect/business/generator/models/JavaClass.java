package ro.seava.tool.architect.business.generator.models;

import java.util.ArrayList;
import java.util.List;

import ro.seava.tool.architect.abstracts.extensions.StringUtils;

public class JavaClass {

	/**
	 * Simple class name
	 */
	private String name;

	/**
	 * Fully qualified (canonical) name
	 */
	private String fqn;

	/**
	 * Parameterized class types
	 */
	private List<JavaClass> types = new ArrayList<JavaClass>();

	/* ====================== CONSTRUCTORS ====================== */

	public JavaClass(String fqn) {
		super();
		this.fqn = fqn;
		this.name = StringUtils.lastTokenFromFQN(this.fqn);
	}

	/* ====================== HELPER FUNCTIONS ====================== */

	public boolean isParameterized() {
		return (this.types.size() > 0);
	}

	/* ====================== GETTERS-SETTERS ====================== */

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFqn() {
		return fqn;
	}

	public void setFqn(String fqn) {
		this.fqn = fqn;
	}

	public List<JavaClass> getTypes() {
		return types;
	}

	public void setTypes(List<JavaClass> types) {
		this.types = types;
	}

}
