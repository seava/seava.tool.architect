package ro.seava.tool.architect.business.generator.controllers.db

import ro.seava.tool.architect.business.business.BmAttrField
import ro.seava.tool.architect.business.generator.models.db.Column
import ro.seava.tool.architect.business.business.BmRefField

class OracleController extends AbstractDbController {

  override getDbEngine() {
    return "oracle"
  }

  override protected resolveDataType(BmAttrField f, Column c) {
    c.dateType = f.dataDomain.dataType.oracleType
  }

  override protected resolveDataType(BmRefField f, Column c) {
  }

}
