package ro.seava.tool.architect.business.generator.templates.bm.j4e

import static extension ro.seava.tool.architect.business.extensions.Bm_Cfg.*

import ro.seava.tool.architect.business.business.Bm
import ro.seava.tool.architect.business.generator.templates.AbstractBusinessTemplate
import ro.seava.tool.architect.business.business.BmRefField

class GeneratedServiceInterfaceTpl extends AbstractBusinessTemplate {

  /**
   * 
   */
  def doJavaClass(Bm bm) {
    var CharSequence body = bm.body_tpl;

    '''
      «copyright»
      package «bm.packageGeneratedServiceInterface»;
      
      «imports»
      «body»
    '''
  }

  def body_tpl(Bm bm) {
    '''
      
      /**
       * Interface to expose business functions specific for {@link «bm.simpleNameModel»} domain
       * entity.
       */
      public interface «bm.simpleNameServiceInterface»«bm.extends_tpl» {
        «bm.service_methods_tpl»
        «bm.findby_methods_tpl»
      }
    '''
  }

  /**
   * Extended interface template
   */
  def private extends_tpl(Bm bm) {
    ''' extends «"seava.lib.j4e.api.business.service.IEntityService".simpleNameFromFQN»<«bm.canonicalNameModel.
      simpleNameFromFQN»>'''
  }

  /**
   * Declaration of the service methods
   */
  def protected service_methods_tpl(Bm bm) {
    '''      
      «FOR sm : bm.serviceMethods»
        «sm.service_method_declaration_tpl»;
      «ENDFOR»
    '''
  }

  /**
   * Find-by methods
   */
  def protected findby_methods_tpl(Bm bm) {
    if (bm.findByMethods.size > 0) {
      '''
        «FOR m : bm.findByMethods»
          «m.findby_method_declaration_tpl(false)»;
          «IF m.fields.exists[t|t instanceof BmRefField]»
            «m.findby_method_declaration_tpl(true)»;
          «ENDIF»
        «ENDFOR»
      '''
    }
  }

}
