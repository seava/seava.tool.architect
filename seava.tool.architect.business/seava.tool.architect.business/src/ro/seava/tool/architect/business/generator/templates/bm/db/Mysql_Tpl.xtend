package ro.seava.tool.architect.business.generator.templates.bm.db

import ro.seava.tool.architect.business.generator.models.db.Column
import ro.seava.tool.architect.business.generator.models.db.Table
import ro.seava.tool.architect.business.generator.models.db.UniqueKey
import ro.seava.tool.architect.business.generator.models.db.ForeignKey

class Mysql_Tpl {

  /**
   * Table template
   */
  def doFileTable(Table t) {
    var tableName = t.name;
    '''
      
        /* ==================== «tableName» ==================== */
        
        /* Table */
        
        create table if not exists «tableName» (
          «t.columns_tpl»
        );
        
        /* Constraints */
        
        «t.primary_key_tpl»
        «t.unique_keys_tpl»
    '''
  }

  /**
   * Foreign keys template
   */
  def doFileFk(Table t) {
    '''
      «t.foreign_keys_tpl»
    '''
  }

  /**
   * Table columns template
   */
  def private columns_tpl(Table t) {
    '''
      «FOR c : t.columns SEPARATOR ','»
        «c.column_tpl»
      «ENDFOR»
    '''
  }

  /**
   * Column definition template
   */
  def private column_tpl(Column c) {
    '''«c.name» «c.column_datatype_tpl» «c.column_required_tpl»'''
  }

  /**
   * Data-type template for a column definition
   */
  def private column_datatype_tpl(Column c) {

    var int lengthDescriptorCount = c.lengthDescriptorCount

    if (lengthDescriptorCount == 0) {
      return '''«c.dateType»'''
    }

    if (lengthDescriptorCount == 1) {
      return '''«c.dateType»(«c.length»)'''
    }

    if (lengthDescriptorCount == 2) {
      return '''«c.dateType»(«c.length», «c.precision»)'''
    }
  }

  /**
   * Not-null attribute template
   */
  def private column_required_tpl(Column c) {
    if (c.required) {
      return '''not null'''
    }
  }

  /**
   *  Primary key template
   */
  def private primary_key_tpl(Table t) {
    var pk = t.primaryKey
    if (pk != null) {
      '''alter table «t.name» add primary key(«FOR c : pk.columnNames SEPARATOR ','»«c»«ENDFOR»);'''
    }
  }

  /**
   *  Unique keys template
   */
  def private unique_keys_tpl(Table t) {
    '''
      «FOR uk : t.uniqueKeys»
        «uk.unique_key_tpl(t)»
      «ENDFOR»
    '''
  }

  /**
   *  Unique key template
   */
  def private unique_key_tpl(UniqueKey uk, Table t) {
    '''alter table «t.name» add constraint «uk.name» unique («FOR c : uk.columnNames SEPARATOR ','»«c»«ENDFOR»);'''
  }

  /**
   *  Foreign keys template
   */
  def private foreign_keys_tpl(Table t) {
    '''
      «FOR fk : t.foreignKeys» 
        «fk.foreign_key_tpl(t)»
      «ENDFOR»
    '''
  }

  /**
   *  Foreign key template
   */
  def private foreign_key_tpl(ForeignKey fk, Table t) {
    '''
      alter table «t.name» add constraint «fk.name» foreign key («FOR c : fk.columnNames SEPARATOR ','»«c»«ENDFOR»)
      references «fk.referencedTableName»(«FOR c : fk.referencedColumnNames SEPARATOR ','»«c»«ENDFOR»)«IF fk.cascadeDelete» on delete cascade«ENDIF»;
    '''
  }

}
